import 'dart:async';
import 'dart:convert';

import 'package:flutter/widgets.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../common/constant.dart';
import '../common/tools.dart';
import '../helper/i_shared_context.dart';
import '../helper/location-helper.dart';
import '../helper/shared_variables.dart';
import '../models/predictions_model.dart';
import '../models/saved_location_model.dart';
import '../models/user.dart';
import 'auth_provider.dart';

class UserModel with ChangeNotifier, ISharedContext {
  User? user;
  String? _token;
  int? clientId;
  String? get token {
    if (_token != null) {
      return _token;
    }
    return null;
  }

  bool isAuth = false;
  int? loginStatusCode = -1;
  List<SavedLocationModel> homeLocation=[];
  List<SavedLocationModel> workLocation=[];
  List<SavedLocationModel> favouriteLocations = [];
  bool saveLocationLoading = false;

  var responseData;

  bool isUserSaved = false;
  bool userUpdate = false;
  bool coodAuth = false;
  bool needRegister = false;
  bool isLoading = false;
  bool isFirsTime = true;
  bool getAddressLoading = true;

  Future<bool> updateClient({
    required String clientName,
    required bool gender,
    required String birthdate,
  }) async {
    Map<String, dynamic> queryParam = {
      "clientId": "$userIdForGetLocations",
      'email': "exampel@gmail.com",
      "clientName": clientName,
      'gender': gender.toString(),
      'birthdate': birthdate
    };
    print(queryParam);
    var uri = Uri.http(API_URL, '/MobileClient/updateClient', queryParam);

    var response = await http.post(uri);
    print(response.statusCode);

    if (response.statusCode == 200) {
      final responseData = json.decode(response.body);
      print(responseData);
      final SharedPreferences sh = await SharedPreferences.getInstance();
      await sh.remove('user');
      user = User.fromJson(responseData['client']);
      print('got userr info from json succes');
      Map userJson = user!.toJson();
      String userString = jsonEncode(userJson);
      await sh.setString('user', userString);

      needRegister = false;
      coodAuth = false;
      isUserSaved = true;
      user!.clientName = clientName;
      user!.clientBirthday = birthdate;
      user!.address = "";

      Provider.of<AuthProvider>(sharedContext, listen: false)
          .updateToken(user!.token);
      await Provider.of<AuthProvider>(sharedContext, listen: false)
          .loadTokenFromPrefs();
      return true;
    }
    return false;
  }

  Future<bool> saveHomeLocation(int clientId, LatLng location) async {
    saveLocationLoading = true;
    notifyListeners();
    Map<String, String> para = {
      "clientId": "$clientId",
      "homelocation": "${location.latitude},${location.longitude}"
    };
    print("the parameters are : $para");

    final uri = Uri.http(API_URL, '/MobileClient/saveHome', para);

    try {
      final response = await http.get(uri);

      if (response.statusCode == 200) {
        saveLocationLoading = false;
        notifyListeners();
        return true;
      } else {
        saveLocationLoading = false;
        notifyListeners();
        return false;
      }
    } catch (e) {
      saveLocationLoading = false;
      notifyListeners();
      return false;
    }
  }

  Future<bool> saveWorkLocation(int clientId, LatLng location) async {
    saveLocationLoading = true;
    notifyListeners();
    Map<String, String> para = {
      "clientId": "$clientId",
      "homelocation": "${location.latitude},${location.longitude}"
    };

    final uri = Uri.http(API_URL, '/MobileClient/saveWork', para);

    try {
      final response = await http.post(uri);

      if (response.statusCode == 200) {
        saveLocationLoading = false;
        notifyListeners();
        return true;
      } else {
        saveLocationLoading = false;
        notifyListeners();
        return false;
      }
    } catch (e) {
      saveLocationLoading = false;
      notifyListeners();
      return false;
    }
  }

  Future<bool> saveFavoriteLocation(int clientId, LatLng locationf) async {
    saveLocationLoading = true;
    notifyListeners();
    Map<String, String> para = {
      "clientId": "$clientId",
      "favlocation":"${locationf.latitude},${locationf.longitude}",
    };
    print(para);
    final uri = Uri.https(API_URL, '/MobileClient/saveFavourite', para);
    try {
      final response = await http.post(uri);
      print(response.statusCode);
      if (response.statusCode == 200) {
        saveLocationLoading = false;
        notifyListeners();
        return true;
      } else {
        saveLocationLoading = false;
        notifyListeners();
        return false;
      }
    } catch (e) {
      saveLocationLoading = false;
      notifyListeners();
      return false;
    }
  }

  Future<List<SavedLocationModel>?> getAllFavouriteAddress() async {
    if (userIdForGetLocations == null) {
      return null;
    }
    Map<String, String> para = {
      "clientId": "$userIdForGetLocations",
    };

    final uri = Uri.http(API_URL, '/MobileClient/getAllFavourite', para);

    try {
      final response = await http.get(uri);

      if (response.statusCode == 200) {
        final jsonData = json.decode(response.body);
        if (jsonData["points"].isEmpty) {
          notifyListeners();
          return null;
        } else {
          favouriteLocations.clear();
          for (var location in jsonData['points']) {
            final tempLocation = location["location"].split(",");
            final tempAddress = SavedLocationModel(
              addressId: location["addressId"],
              clientId: location["clientId"],
              location: LatLng(
                  double.parse(tempLocation[0]), double.parse(tempLocation[1])),
              typeid: location["typeid"],
            );
            favouriteLocations.add(tempAddress);
          }
        }
      } else {
        return null;
      }
    } catch (e) {
      print(e);
    }
    return favouriteLocations;
  }

  Future<List<SavedLocationModel>?> getWorkLocation() async {
    if (userIdForGetLocations == null) {
      return null;
    }
    Map<String, String> para = {
      "clientId": "$userIdForGetLocations",
    };
    getAddressLoading = true;
    notifyListeners();
    final uri = Uri.http(API_URL, '/MobileClient/getWord', para);

    try {
      final response = await http.get(uri);
      if (response.statusCode == 200) {
        print("get work adrress done  ${response.body}");
        final jsonData = json.decode(response.body);
        if (jsonData["point"]["location"] == null) {
          workLocation = [];
          getAddressLoading = false;
          notifyListeners();
        } else {
          workLocation[0] = SavedLocationModel(
            addressId: jsonData["point"]["addressId"],
            clientId: jsonData["point"]["clientId"],
            location: jsonData["point"]["location"],
            typeid: jsonData["point"]["typeid"],
          );
          getAddressLoading = false;
          notifyListeners();
        }
      } else {
        getAddressLoading = false;
        notifyListeners();
      }
    } catch (e) {
      getAddressLoading = false;
      notifyListeners();
      print(e);
      return null;
    }
    return workLocation;
  }

  Future<List<SavedLocationModel>?> getHomeLocation() async {
    if (userIdForGetLocations == null) {
      return null;
    }
    Map<String, String> para = {
      "clientId": "$userIdForGetLocations",
    };
    getAddressLoading = true;
    notifyListeners();
    final uri = Uri.http(API_URL, '/MobileClient/getHome', para);

    try {
      final response = await http.get(uri);
      if (response.statusCode == 200) {
        print("get home adrress done  ${response.body}");
        final jsonData = json.decode(response.body);
        if (jsonData["point"]["location"] == null) {
          homeLocation = [];
          getAddressLoading = false;
          notifyListeners();
        } else {
          homeLocation[0] = SavedLocationModel(
            addressId: jsonData["point"]["addressId"],
            clientId: jsonData["point"]["clientId"],
            location: jsonData["point"]["location"],
            typeid: jsonData["point"]["typeid"],
          );
          getAddressLoading = false;
          notifyListeners();
        }
      } else {
        getAddressLoading = false;
        notifyListeners();
      }
    } catch (e) {
      getAddressLoading = false;
      notifyListeners();
      print(e);
      return null;
    }
    return homeLocation;
  }


}

