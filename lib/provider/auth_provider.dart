import 'dart:convert';

import 'package:flutter/cupertino.dart';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../common/constant.dart';
import '../helper/shared_variables.dart';

import '../models/user.dart';

class AuthProvider with ChangeNotifier {
  bool isLoading = false;
  User? user;

  TextEditingController verifyNumer1 = TextEditingController();
  TextEditingController verifyNumer2 = TextEditingController();
  TextEditingController verifyNumer3 = TextEditingController();
  TextEditingController verifyNumer4 = TextEditingController();
  TextEditingController verifyNumer5 = TextEditingController();
  TextEditingController verifyNumer6 = TextEditingController();

  TextEditingController phone = TextEditingController();
  // final TextEditingController _phone = TextEditingController();


  bool isAuth = false;
  int? loginStatusCode = -1;
  bool isUserSaved = false;
  bool userUpdate = false;
  bool coodAuth = false;
  bool? needRegister = false;
  bool isFirsTime = true;
  String? token;
  bool? firstTimeInApp;
  Future<void> checkIsFirstTimeInApp() async {
    final sh = await SharedPreferences.getInstance();
    firstTimeInApp = await sh.getBool("firstTimeInApp");
    notifyListeners();
  }

  Future<bool> login(String phoneNumber) async {
    Map<String, String> queryParam = {
      'Mobile': phoneNumber,
    };

    Map<String, String> queryParam2 = {
      'ClientMobile': phoneNumber,
    };
    final uri = Uri.http(API_URL, '/MobileClient/getByMobile', queryParam);
    final uri2 = Uri.http(API_URL, '/MobileClient/addNewClient', queryParam2);
    try {
      isLoading = true;
      isAuth = false;
      notifyListeners();

      final response = await http.post(
        uri,
      );
print(response.statusCode);
      if (response.statusCode == 200) {
        final responseData = json.decode(response.body);
        // print(response.body);
        activationCode = responseData['activationCodes']['\$values'][0]['code'];
        loginStatusCode = response.statusCode;
        user = User.fromJson(responseData);
        userIdForGetLocations = user!.clientId;
        isLoading = false;
        notifyListeners();
        return false;
      } else if (response.statusCode == 204) {
        // try {
        needRegister = true;
        isLoading = true;
        notifyListeners();

        final response = await http.post(
          uri2,
        );
        print('hellooo ${response.statusCode}');
        if (response.statusCode == 200) {
          final responseData = json.decode(response.body);
          // print(responseData);
          activationCode = responseData['client']['activationCodes']['\$values'][0]['code'];
          user = User.fromJson(responseData['client']);
          final Map<String, dynamic> userMap = user!.toJson();
          final stringUser = json.encode(userMap);
          SharedPreferences sp = await SharedPreferences.getInstance();
          sp.setString("user", stringUser);
          userIdForGetLocations = user!.clientId;
          loginStatusCode = response.statusCode;
          needRegister = true;
          isLoading = false;
          return false;
        } else {
          loginStatusCode = -1;
          isLoading = false;
          notifyListeners();
          print("failed to register new account ${response.statusCode}");
          return false;
        }
      } else {
        print("failed to login with exists account ${response.statusCode}");
        loginStatusCode = -1;
        isLoading = false;
        notifyListeners();
        return false;
      }
    } catch (e) {
      print("failed to login with exists account from catch  $e");
      loginStatusCode = -1;
      isLoading = false;
      notifyListeners();
      return false;
    }
  }

  void updateToken(String token) {
    this.token = token;
    notifyListeners();
  }

  bool? showStatrtsPage;

  Future<void> showStartPageDone() async {
    SharedPreferences sh = await SharedPreferences.getInstance();
    await sh.setBool('firstTimeInApp', false);
    firstTimeInApp = true;
    token = null;
    notifyListeners();
  }

  Future<void> setTokenToPrefs(String token) async {
    final Map<String, dynamic> userJson = user!.toJson();
    String encodeUser = json.encode(userJson);

    SharedPreferences sh = await SharedPreferences.getInstance();
    this.token = token;
    await sh.setString("token", token);
    await sh.setString('user', encodeUser);
  }

  Future<void> loadTokenFromPrefs() async {
    SharedPreferences sh = await SharedPreferences.getInstance();
    firstTimeInApp = sh.getBool("firstTimeInApp");
    token = sh.getString('token');
    if (token != null) {
      if (sh.getString('user') != null) {
        final String? encodeUser = sh.getString('user');

        Map<String, dynamic> userMap = jsonDecode(encodeUser!);
       print(userMap);
        user = User.fromJson(userMap);
        userIdForGetLocations = user!.clientId;
      }
    }

    notifyListeners();
  }

  Future<void> activationCodesCheack() async {
    if (this.needRegister == true) {
      this.token = user!.token;
      await setTokenToPrefs(this.token!);
      this.needRegister = true;
      notifyListeners();
    } else {
      await setTokenToPrefs(user!.token);
      this.needRegister = false;
      this.token = user!.token;

      notifyListeners();
    }
  }

  Future<void> updateClient({
    required String clientName,
    required String clientemail,
    required String gender,
    required String birthdate,
  }) async {
    Map<String, String> queryParam = {
      'clientId': "${user!.clientId}",
      'clientName': clientName,
      'clientemail': clientemail,
      'gender': gender,
      'birthdate': birthdate,
    };
    var uri = Uri.http(API_URL, '/MobileClient/updateClient', queryParam);
    var response = await http.post(uri);
    if (response.statusCode == 200) {
      print("the new user is : ${response.body}");
      var responseData = jsonDecode(response.body);
      user!.clientName = responseData['client']['clientName'];
      user!.clientBirthday = responseData['client']['clientBirthday'];
      user!.clientGender = responseData['client']['clientGender'];
      token = user!.token;
      await setTokenToPrefs(token!);

      notifyListeners();
    }
  }

  Future<void> logOut() async {
    SharedPreferences sh = await SharedPreferences.getInstance();
    sh.clear();
    token = null;
    verifyNumer6.clear();
    verifyNumer5.clear();
    verifyNumer4.clear();
    verifyNumer3.clear();
    verifyNumer2.clear();
    verifyNumer1.clear();
    phone.clear();
    needRegister = false;
    notifyListeners();
  }

  void notif() {
    notifyListeners();
  }

}
