// ignore_for_file: unnecessary_null_comparison

import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

import '../common/tools.dart';
import '../helper/i_shared_context.dart';
import '../helper/location-helper.dart';
import '../models/predictions_model.dart';
import '../models/saved_location_model.dart';
import 'user_provider.dart';

class MapHelper with ChangeNotifier , ISharedContext  {

//https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%D8%AD%D9%85%D8%B5&key=AIzaSyDCSt4ABayMg8O3n9Hvxb_vrs_1oUfWXuA&components=country:sy

  // void changeThePredictionsListContent(int type) {
  //   predictions.clear();
  //   final List tempList = allPoints.map((e) {
  //     if (e.typeid == type) ;
  //   }).toList();
  // }


  List<PredictionModel> predictions = [];

  List<SavedLocationModel> allUserAddress = [];
  List<PredictionModel> allUserAddressWithText = [];
  List<PredictionModel> allPoints = [];
  List<SavedLocationModel> myAddress=[];

  bool isFavSlected = false;
  bool isHouseSelected = false;
  bool isWorkSelected = false;

  bool loadingUntilHandelPointLocation = true;

  void cahngeSelectedValue(String type) {
    if (type == "fav") {
      isFavSlected = true;
      isHouseSelected = false;
      isWorkSelected = false;
      clearPredications();
      getPointAddressName(Provider.of<UserModel>(sharedContext,listen: false).favouriteLocations);
      notifyListeners();
    } else if (type == "work") {
      isFavSlected = false;
      isHouseSelected = false;
      isWorkSelected = true;
      clearPredications();
      getPointAddressName(Provider.of<UserModel>(sharedContext,listen: false).workLocation);
      notifyListeners();
    } else if (type == "house") {
      isFavSlected = false;
      isHouseSelected = true;
      isWorkSelected = false;
      clearPredications();
      getPointAddressName(Provider.of<UserModel>(sharedContext,listen: false).homeLocation);
      notifyListeners();
    }
  }

  void makeAllSelectabeleValueFalse() {
    isFavSlected = false;
    isHouseSelected = false;
    isWorkSelected = false;
    notifyListeners();
  }

//https://maps.googleapis.com/maps/api/place/details/json?place_id=ChIJrTLr-GyuEmsRBfy61i59si0&key=AIzaSyDCSt4ABayMg8O3n9Hvxb_vrs_1oUfWXuA

  Future<LatLng?> getLocationAddressFromPlaceId(String placeId) async {
    try {
      final url =
          "https://maps.googleapis.com/maps/api/place/details/json?place_id=$placeId&key=$API_KEY";
      var response = await http.get(Uri.parse(url));
      if (response.statusCode == 200) {
        var jsonResponse = json.decode(response.body);
        final LatLng location = LatLng(
            (jsonResponse['result']['geometry']['location']['lat']) * 1.0 ??
                null,
            (jsonResponse['result']['geometry']['location']['lng']) * 1.0 ??
                null);
        if (location.latitude == null || location.longitude == null) {
          return null;
        }
        return location;
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }


  Future<void> mapSearch(String query) async {
    try {
      final url =
          "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=$query&key=$API_KEY&components=country:sy";
      final res = await http.get(Uri.parse(url));
      final jsonDate = json.decode(res.body);
      if (res.statusCode == 200) {
        predictions.clear();
        for (var predication in jsonDate['predictions']) {
          predictions.add(PredictionModel.fromJson(predication));
        }
        for (var index = 0; index < predictions.length; index++) {
          LatLng? location = await getLocationAddressFromPlaceId(
              predictions[index].placeId ?? "");

          predictions[index].location = location;
          print(predictions[index].location = location);
          notifyListeners();
        }
        print(res.body.length);
      } else {}
    } catch (e) {}
  }

  void clearPredications() {
    predictions.clear();
    notifyListeners();
  }

  Future<void> getPointAddressName(List<SavedLocationModel>? points) async {
    if (points == null) {
      loadingUntilHandelPointLocation = false;
      notifyListeners();
      allPoints = [];
      return;
    } else {
      if (points.isEmpty) {
        loadingUntilHandelPointLocation = false;
        notifyListeners();
        allPoints = [];
        return;
      } else {
        allPoints.clear();

        for (var point in points) {
          final String addressLocation = await getAddress(
              "${point.location!.latitude},${point.location!.longitude}");
          allPoints.add(PredictionModel(
              addressId: point.addressId,
              clientId: point.addressId,
              location: point.location,
              mainText: addressLocation,
              typeid: point.typeid));
        }
        loadingUntilHandelPointLocation = false;
        predictions = allPoints;
        notifyListeners();
      }
    }
  }


}
