import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

import '../common/constant.dart';
import '../common/tools.dart';
import '../helper/i_shared_context.dart';
import '../helper/save_order_in_sh.dart';
import '../helper/shared_variables.dart';
import '../models/bill_model.dart';
import '../models/car_model.dart';
import '../models/cobon_model.dart';
import '../models/driver.dart';
import '../models/finished_orders.dart';
import '../models/order_model.dart';
import '../models/order_type_model.dart';
import '../models/payment_model.dart';
import '../widgets/suggestion_trip_card.dart';
import 'map_provider.dart';

class DriverProvider with ChangeNotifier, ISharedContext {
  ////// usage var ///////////////
  TextEditingController sourceDetails = TextEditingController();
  TextEditingController desDetails = TextEditingController();
  bool getNearestDriver = false;
  BillModel? billModel;
  bool preferGirle = false;
  int? driverId;
  bool getDriver = false;
  bool sendRequestLoading = false;
  bool checkOrderLoading = true;
  bool cancelOrderLoading = true;
  bool? whileBreak;
  int? sendRequestStatusCode;
  OrderModel? orderModel;
  CarModel? carModel;
  Timer? timerForTracer;
  Driver? chosenDriver;
  List<Driver> allDrivers = [];

  ////// usage var ///////////////
  String? orderStat;
  bool? isReOrder = false;
  Timer? timer;
  bool isLoading = true;
  Map<String, dynamic>? lastOrder = null;
  OrderTypeModel? chosenOrderType;
  int? orderIdForEnd;
  double? clientRating;
  String? clientReview;
  bool billLoading = false;
  List<String>? prices;
  Map<String, String>? queryParam3;

  /////// get Prices Attributes //////
  bool _isOnTrip = false;
  Driver? _selectedDriver;

  Driver? get selectedDriver {
    return _selectedDriver;
  }

  Map<int, String> driverRating = {};
  Map<int, String> driverArrivingTime = {};

  List<OrderTypeModel> orderTypes = [];
  List<SuggestionTripCard> suggestionTripCardList = [];

  set setSelectedDriver(Driver? driver) {
    _selectedDriver = driver;
    notifyListeners();
  }

  bool get isOnTrip {
    return _isOnTrip;
  }

  void changeState() {
    orderStat = "10";
    notifyListeners();
  }

  void makeLastOrderNull() {
    lastOrder = null;
    //notifyListeners();
  }

  void choseTrip(int index) {
    for (var i = 0; i < suggestionTripCardList.length; i++) {
      suggestionTripCardList[i].isAvtive = false;
    }
    suggestionTripCardList[index].isAvtive = true;
    chosenOrderType = suggestionTripCardList[index].orderTypeModel;
    print("done");
    notifyListeners();
  }

  // driverProvider.chosenDriver =
  //                       driverProvider.allDrivers[index];

  void choreseDriver(int index) {
    chosenDriver = allDrivers[index];
    allDrivers[index].isChocesnOne = true;
    for (var i = 0; i < allDrivers.length; i++) {
      if (i != index) allDrivers[i].isChocesnOne = false;
    }
    notifyListeners();
  }

  void makeChoseDriverAsGirl() {
    preferGirle = !preferGirle;
    notifyListeners();
  }


  bool cobonLoading = false;
  bool activatedCobon = false;
  CobonModel? cobon ;

  Future<void> getCobon(String cobonCode) async {
    final queryParam3 = {'cobonCode': "$cobonCode"};
    var url = Uri.http(API_URL, '/MobileClient/getCobonInfo', queryParam3);

    cobonLoading =true;
    var getCobonResult = await http.get(url);
    print(getCobonResult.statusCode);

    if (getCobonResult.statusCode == 200) {
      final cobonJson = json.decode(getCobonResult.body);
      activatedCobon =true;
      cobonLoading = false;
      cobon = CobonModel.fromJson(cobonJson['cobon']);
      notifyListeners();
    } else {
      cobonLoading = false;
      activatedCobon=false;
      print("Error In Get cobon discount");
      notifyListeners();
    }
  }


  Future<void> sendRequest(
      {required int clientId,
      required int driverId,
      required int typeId,
      required LatLng sourceLocation,
      required String sourceDetails,
      required String destinationDetails,
      required LatLng destinationLocation}) async {
    sendRequestLoading = true;
    notifyListeners();

    Map<String, dynamic> queryParam = {
      "clientid": "$clientId",
      "driverId": "${chosenDriver!.driverId}",
      "typeId": "$typeId",
      "sourcelocation":
          "${sourceLocation.latitude},${sourceLocation.longitude}",
      "destinationlocation":
          "${destinationLocation.latitude},${destinationLocation.longitude}",
      "sourcedetails": sourceDetails,
      "destinationdetails": destinationDetails,
    };
    print(queryParam);
    var url = Uri.http(API_URL, '/MobileClient/sendRequest',queryParam);
    var response = await http.post(url);
    print(url);
    print(response.statusCode);
    if (response.statusCode == 200) {
      print(response.body);
      changeIndex(4);
      notifyListeners();
      final requestDate = json.decode(response.body);

      orderModel = OrderModel.fromJson(requestDate);

      orderIdForEnd = requestDate['orderId'] as int;
      sendRequestStatusCode = response.statusCode;
      sendRequestLoading = false;
      notifyListeners();
      await orderCheck(orderId: requestDate['orderId'].toString());
    } else {
      sendRequestLoading = false;
      orderStat = "3";
      notifyListeners();
    }
  }

  timerStopping({required Timer t, required int timeOut}) {
    if (timeOut >= 90 || checkOrderLoading == false) {
      t.cancel();

      notifyListeners();
    }
  }

  // Future<void> orderCheck({required String orderId}) async {
  //   orderStat = null;
  //   checkOrderLoading = true;
  //   notifyListeners();
  //   final queryParam = {
  //     "orderId": orderId,
  //   };
  //   final url = Uri.http(API_URL, '/MobileClient/getOrderById', queryParam);
  //
  //   int timeOut = 0;
  //
  //   timer = Timer.periodic(Duration(seconds: 30), (t) async {
  //     print("hello from Cheack Order Timer ");
  //     if (orderStat != null && orderStat != "1") {
  //       timerStopping(t: t, timeOut: timeOut);
  //     }
  //
  //     timeOut += 30;
  //     final orderResponse = await http.get(url);
  //
  //     print(orderResponse.body);
  //     final orderResponseJson = json.decode(orderResponse.body);
  //     if (orderResponse.statusCode == 200) {
  //       if ( timeOut >= 90) {
  //         checkOrderLoading = true;
  //         orderStat = "0";
  //         print(" orderStat:  $orderStat");
  //         timerStopping(t: t, timeOut: timeOut);
  //         notifyListeners();
  //       } else if (orderResponseJson['order']['status'] == "1" || orderResponseJson['order']['status'] == "0") {
  //         checkOrderLoading = false;
  //         orderStat = "1";
  //         // await getCarLocation();
  //         await startDriverTracing();
  //         await saveOrderInShardePreference(orderModel!);
  //         lastOrder = null;
  //         timerStopping(t: t, timeOut: timeOut);
  //
  //         notifyListeners();
  //         print(" orderStat:  $orderStat");
  //       } else if (orderResponseJson['order']['status'] == "2") {
  //         checkOrderLoading == false;
  //         timerStopping(t: t, timeOut: timeOut);
  //         notifyListeners();
  //       }
  //     } else
  //       print(
  //           "Driver does not respomse to the order Driver does not respomse to the order Driver does not respomse to the order ");
  //   });
  // }

  Future<void> orderCheck({required String orderId}) async {
    orderStat = null;
    checkOrderLoading = true;
    notifyListeners();
    final queryParam = {
      "orderId": orderId,
    };
    final url = Uri.http(API_URL, '/MobileClient/getOrderById', queryParam);

    int timeOut = 0;

    timer = Timer.periodic(Duration(seconds: 30), (t) async {
      print("hello from Cheack Order Timer ");
      if (orderStat != null && orderStat != "1") {
        timerStopping(t: t, timeOut: timeOut);
      }

      timeOut += 30;
      final orderResponse = await http.get(url);

      final orderResponseJson = json.decode(orderResponse.body);
      if (orderResponse.statusCode == 200) {
        if (orderResponseJson['order']['status'] == "3" && timeOut >= 90) {
          checkOrderLoading = false;
          orderStat = "3";
          print(" orderStat:  $orderStat");
          timerStopping(t: t, timeOut: timeOut);
          notifyListeners();
        }
        else if (orderResponseJson['order']['status'] == "1") {
          checkOrderLoading = false;
          orderStat = "1";
          // await getCarLocation();
          await startDriverTracing();
          await saveOrderInShardePreference(orderModel!);
          lastOrder = null;
          notifyListeners();
          print(" orderStat:  $orderStat");
        }
        else if (orderResponseJson['order']['status'] == "2") {
          print(" orderStat:  ${orderResponseJson['order']['status']}");
          checkOrderLoading == false;
          ///////
          orderStat = "2";
          ///////
          timerStopping(t: t, timeOut: timeOut);
          notifyListeners();
        }
      } else
        print(
            "Driver does not respomse to the order Driver does not respomse to the order Driver does not respomse to the order ");
    });
  }


  Future<bool> cancelOrder({required String orderId}) async {
    orderStat = null;
    cancelOrderLoading = true;
    notifyListeners();
    final queryParam = {
      "orderId": orderId,
    };
    print(queryParam);
    try {
      final url = Uri.http(API_URL, '/MobileClient/CancelOrder', queryParam);
      final orderResponse = await http.get(url);
      if (orderResponse.statusCode == 200) {
        cancelOrderLoading = false;
        notifyListeners();
        return true;
      } else {
        cancelOrderLoading = false;
        lastOrder = null;
        notifyListeners();
        return false;
      }
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<bool> getBill(int orderId) async {
    final queryParam3 = {'orderId': "$orderId"};
    var url = Uri.http(API_URL, '/MobileClient/getOrderPrices', queryParam3);
    print(queryParam3);
    var getOrderPrices = await http.get(url);
    print(getOrderPrices.statusCode);
    if (getOrderPrices.statusCode == 200) {
      var getOrderPricesJson = json.decode(getOrderPrices.body);
      prices = getOrderPricesJson['price'].toString().split('-');
      final double totalDistancePrice = double.parse(prices![0]);
      final double hoursPrice = double.parse(prices![1]);
      final double waittingPrice = double.parse(prices![2]);
      final double counterOpenPrice = double.parse(prices![3]);
      final double totalPrice =
          totalDistancePrice + hoursPrice + waittingPrice + counterOpenPrice;

      // "703-0-7-2000"
      billModel = BillModel(
        counterOpenPrice: counterOpenPrice,
        hoursPrice: hoursPrice,
        totalDistancePrice: totalDistancePrice,
        totalPric: totalPrice,
        waittingPrice: waittingPrice,
      );
      billLoading = false;
print(billModel!.totalPric);
      notifyListeners();
      return true;
    } else {
      billLoading = true;
      print("Error In Get Bill");
      notifyListeners();
      return false;
    }
  }

  Future<int> FinshOrder() async {
    Map<String, String> queryParam = {
      'orderId': "$orderIdForEnd",
      'rating': "4",
      'review': "${clientReview ?? " "}"
    };
    var url = Uri.http(API_URL, '/MobileClient/finishOrder', queryParam);
    try {
      var response = await http.post(url);

      if (response.statusCode == 200) {
        print("finish order done");
        return 500;
      } else {
        print(
            " something wrong with finish order whit status code ${response.statusCode}");
        return 500;
      }
    } catch (e) {
      print(" exception-----> : $e  ");
      return 500;
    }
  }

  bool getFinishedOrderLoading = false;
  List<FinishedOrder> finishedOrders = [];

  Future<List<FinishedOrder>> getFinishOrders(String clientId) async {
    getFinishedOrderLoading = true;
    finishedOrders.clear();
    notifyListeners();
    final queryParam = {
      "clientId": clientId,
    };
    try {
      final url = Uri.http(API_URL, '/MobileClient/getFinishOrders', queryParam);
      final response = await http.get(url);
      print(response.statusCode);
      if (response.statusCode == 200) {
        final jsonResponse = json.decode(response.body);
        print('success got finished orders');
        for (var order in jsonResponse['orders']) {
          finishedOrders.add(FinishedOrder.fromJson(order));
        }
        getFinishedOrderLoading = false;
        notifyListeners();

      } else {
        getFinishedOrderLoading = false;
        print("Error In Get finished orders with status code ${response.statusCode}");
        notifyListeners();
      }
      return finishedOrders;
    }catch(e){
      print('error from catch finished orders is $e');
      return finishedOrders;
    }
  }

  bool getPaymentsLoading = false;
  List<PaymentModel> payments = [];

  Future<void> getPayemtns(String clientId) async {
    getPaymentsLoading = true;
    payments.clear();
    notifyListeners();
    final queryParam = {
      "clientId": clientId,
    };
    print(queryParam);
    try {
      final url = Uri.http(API_URL, '/MobileClient/getPayemtns', queryParam);
      final response = await http.get(url);
      print(response.statusCode);
      if (response.statusCode == 200) {
        final jsonResponse = json.decode(response.body);
        print('success got payments');
        for (var pay in jsonResponse['payemnts']) {
          payments.add(PaymentModel.fromJson(pay));
        }
        print(payments[0].paid);
        getPaymentsLoading = false;
        notifyListeners();
      } else {
        getPaymentsLoading = false;
        print("Error In Get payments with status code ${response.statusCode}");
        notifyListeners();
      }
      // return payments;
    }catch(e){
      print('error from catch payments for a client is $e');
      // return payments;
    }
  }


  Future<void> getDriverById(
    int driverId,
  ) async {
    Map<String, String> param = {
      "driverId": "$driverId",
    };

    try {
      var url = Uri.http(API_URL, '/MobileClient/getDriverById', param);
      var getDriver = await http.get(url);

      if (getDriver.statusCode == 200) {
        final getDriverJson = json.decode(getDriver.body);
        chosenDriver = Driver.fromJson(getDriverJson['driver']);
      } else {}
    } catch (e) {
      print("error get or build driver");
    }
  }

  Future<void> getDriverRating(
    int driverId,
  ) async {
    Map<String, String> param = {
      "driverid": "$driverId",
    };
    try {
      var url = Uri.http(API_URL, '/MobileClient/getDriverRating', param);
      var getDriver = await http.get(url);

      if (getDriver.statusCode == 200) {
        final driverRate = json.decode(getDriver.body);
        driverRating[driverId] = getDriver.body;
        print(driverId);
        print(driverRating);
      } else {}
    } catch (e) {
      print("error get or build driver");
    }
  }

  Future<void> getDriverArrivingTime(
      LatLng source, String driverLocation, int driverId) async {
    try {
      var queryParam = {
        'destinations': "${source.latitude},${source.longitude}",
        'origins': driverLocation,
        'key': API_KEY,
      };
      final url = Uri.https(
          'maps.googleapis.com', "/maps/api/distancematrix/json",queryParam);
      var response = await http.get(url);
      print(url);
      print(queryParam);
      print(response.statusCode);
      if (response.statusCode == 200) {
        final responseData = json.decode(response.body);
        driverArrivingTime[driverId] =
            responseData['rows'][0]['elements'][0]['duration']['text'];
        print(driverArrivingTime);
      }
    } catch (e) {
      print(e);
    }
  }

  changeIndex(int newIndex) {
    index = newIndex;
    notifyListeners();
  }

  Future<void> getAllDrivers(LatLng source) async {
    allDrivers.clear();
    getNearestDriver = true;
    driverRating.clear();
    driverArrivingTime.clear();
    notifyListeners();
    try {
      Map<String, dynamic> queryParam = {
        'location': "${source.latitude},${source.longitude}",
      };
      print(source);
      final url = Uri.http(API_URL,
          '/MobileClient/getAvailableDriversDetailwithLocation', queryParam);
      // final url = Uri.http(API_URL, '/MobileClient/getAllDrivers');
      final response = await http.get(url);
      if (response.statusCode == 200) {
        print('success got driverrrs');
        final getDriverJson = json.decode(response.body);
        for (var driver in getDriverJson['drivers']) {
          // print(driver);
          // print( driver['cars']['\$values'][0]);
          if (driver['cars'] != null) {
            if (driver['cars']['\$values'][0]['status'] == 'ready') {
              allDrivers.add(Driver.fromJson(driver));
              await getDriverRating(driver['driverId']);
              await getDriverArrivingTime(
                  source,
                  driver['cars']['\$values'][0]['location'],
                  driver['driverId']);
            }
          }
        }
        if (allDrivers.isNotEmpty) {
          allDrivers[0].isChocesnOne = true;
          chosenDriver = allDrivers[0];
        }
        changeIndex(3);
        getNearestDriver = false;
        notifyListeners();
      } else {}
    } catch (e) {
      print(e);
      print("error while get all drivers $e");
    }
  }

  Future<void> getTripTypes() async {
    orderTypes.clear();
    final url = Uri.http(API_URL, '/MobileClient/getOrderTypes');
//{"types":[{"id":1,"typename":"توصيل","description":"توصيل زبائن","prices":1000,"favouriteTypes":null,"orders":null},{"id":3,"typename":"طلبيات","description":"رحلة توصيل أغراض","prices":800,"favouriteTypes":null,"orders":null},{"id":4,"typename":"VIP","description":"توصيل درجة اعمال","prices":2000,"favouriteTypes":null,"orders":null},{"id":5,"typename":"نساء فقط","description":"توصيل للنساء فقط","prices":1200,"favouriteTypes":null,"orders":null},{"id":6,"typename":"طلبيات خاصة","description":"طلبيات خاصة","prices":3000,"favouriteTypes":null,"orders":null}]}
    try {
      final response = await http.get(url);
      if (response.statusCode == 200) {
        final jsonData = json.decode(response.body);
        for (var orderType in jsonData['types']) {
          final temp = OrderTypeModel.fromJson(orderType);
          print(temp.arriving);
          switch (temp.typeNmae) {
            case "توصيل":
              {
                temp.image = "assets/images/normal_car.png";
                break;
              }
            case "طلبيات":
              {
                temp.image = "assets/images/motor.png";
                break;
              }
            case "VIP":
              {
                temp.image = "assets/images/vip_car.png";
                break;
              }

            default:
              {
                temp.image = "assets/images/car1.png";
                break;
              }
          }
          orderTypes.add(temp);
        }
        if (orderTypes.isNotEmpty) {
          for (var orderType in orderTypes) {
            suggestionTripCardList
                .add(SuggestionTripCard(orderTypeModel: orderType));
          }
          suggestionTripCardList[0].isAvtive = true;
          chosenOrderType = suggestionTripCardList[0].orderTypeModel;
        }
      } else {
        print("the status code ${response.statusCode}");
      }
      notifyListeners();
    } catch (e) {
      print("error while get order types ${e}");
    }
  }

  LatLng? driverlocation;

  Future<void> getCarLocation() async {
    try {
      final Map<String, String> queryParam = {
        "driverId": "${chosenDriver!.driverId}",
      };
      final url = Uri.http(API_URL, '/MobileDriver/getCarLocation', queryParam);
      var res = await http.get(url);
      print(url);
      print(res.statusCode);
      if (res.statusCode == 200) {
        print('get car location successs');
        final jsonData = json.decode(res.body);
        final String carLocation =
            jsonData['car']['location'].toString().trim();
        final List<String> latLngList = carLocation.split(',');
        final LatLng location =
            LatLng(double.parse(latLngList[0]), double.parse(latLngList[1]));

        driverlocation = location;
print('car driver location is $location');
        Provider.of<MapProvider>(sharedContext, listen: false)
            .addDriverMarker(location);

        double distance = await Geolocator.distanceBetween(
            Provider.of<MapProvider>(sharedContext, listen: false)
                .source!
                .latitude,
            Provider.of<MapProvider>(sharedContext, listen: false)
                .source!
                .longitude,
            location.latitude,
            location.longitude);
        final double sourceLat =
            Provider.of<MapProvider>(sharedContext, listen: false)
                .source!
                .latitude;
        final double sourceLng =
            Provider.of<MapProvider>(sharedContext, listen: false)
                .source!
                .longitude;

        final double desLat = driverlocation!.latitude;
        final double desLng = driverlocation!.longitude;

        await getEstimatedTime(sourceLat, sourceLng, desLat, desLng);

        if (distance.toInt() <= 50) {
          orderStat = "10";
        }
        notifyListeners();
        print("the distance is  ${distance.toInt()}");
      }
    } catch (e) {
      print('getting car location exception $e');
    }
  }

  Timer? trackingDriverTimer;

  Future<void> startDriverTracing() async {
    print('this is the start driver tracing function');
    trackingDriverTimer =Timer(
      Duration(seconds: 5),
      () async {
        await getCarLocation();
      },
    );
  }

  Future<void> getEstimatedTime(
      double sourceLat, double sourceLng, double desLat, double desLng) async {
    final url =
        "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=$sourceLat,$sourceLng&destinations=$desLat&$desLng&key=$API_KEY";
try{
    var res = await http.get(Uri.parse(url));
    print("time : ${res.body}");
    print("get estimated time between source and driver success");
  }catch(e) {
  print('get estimated time between source and driver exception is $e');
}
  }




// Future<void> setOrderToSharedPref() async {
//   final sh = await SharedPreferences.getInstance();
//   String orderString = jsonEncode(orderModel);
//   await sh.setString("order", orderString);
// }
// Future<Marker?> driverTracker() async {
//   SharedPreferences sh = await SharedPreferences.getInstance();
//   final Map<String, String> queryParam = {
//     "driverId": "$driverId",
//   };
//   try {
//     final url = Uri.http(API_URL, '/MobileDriver/getCarLocation', queryParam);
//     final response = await http.post(url);
//     print("status code get cat location  ${response.statusCode}");
//     if (response.statusCode == 200) {
//       final jsonResponse = jsonDecode(response.body);
//       carModel = CarModel.fromJson(jsonResponse['car']);
//       print("get the car postion");
//       final currentCarPosList = carModel!.location.split(",");

//       Marker? driverMarker = Marker(
//         infoWindow: InfoWindow(
//           title: "موقع السيارة",
//         ),
//         icon: BitmapDescriptor.defaultMarkerWithHue(
//           BitmapDescriptor.hueYellow,
//         ),
//         markerId: const MarkerId("driverCar"),
//         position: LatLng(
//           double.parse(
//             currentCarPosList[0],
//           ),
//           double.parse(
//             currentCarPosList[1],
//           ),
//         ),
//       );
//       sh.remove("driverMarker");
//       Map<String, dynamic> tempMap = {
//         'lat': driverMarker.position.latitude,
//         'lon': driverMarker.position.longitude
//       };
//       String tempMapString = json.encode(tempMap);
//       await sh.setString(
//         "driverMarker",
//         tempMapString,
//       );
//       return driverMarker;
//     } else {
//       return null;
//     }
//   } catch (e) {
//     print("error while get driver location $e");
//   }

//   return null;
// }

// Future<int> getAvailableDriverIdWithLocation(
//     LatLng source, String token) async {
//   getNearestDriver = true;

//   notifyListeners();

//   final _currentPositionString = "${source.latitude},${source.longitude}";
//   print("_ currentPositionString : $_currentPositionString");
//   Map<String, dynamic> queryParam = {
//     'location': _currentPositionString,
//   };
//   print("location is $_currentPositionString");
//   var url = Uri.http(
//       API_URL, '/MobileClient/getAvailableDriverIdwithLocation', queryParam);
//   print("url is $url");
//   var getDriver =
//       await http.get(url, headers: {"authorization": "Bearer ${token}"});

//   print("status code is  from get driver: ${getDriver.statusCode}");
//   if (getDriver.statusCode == 200) {
//     getNearestDriver = true;
//     var getDriverJson = json.decode(getDriver.body);
//     print(" driver Id is :  $getDriverJson['driverId']");
//     driverId = getDriverJson['driverId'] as int;
//     await getDriverById(driverId!).then((value) {
//       changeIndex(3);
//       getNearestDriver = false;
//       notifyListeners();
//     });

//     return driverId!;
//   } else {
//     getNearestDriver = false;
//     notifyListeners();

//     driverId = 1;
//     return driverId!;
//   }
// }

// void chosenDriver(int index) {
//   driverCardList.forEach((element) {
//     element.isVisibale != false;
//   });
//   driverCardList[index].isVisibale != true;
//   notifyListeners();
// }
}
