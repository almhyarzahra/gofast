import 'dart:async';
import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import '../common/tools.dart';
import '../helper/location-helper.dart';
import '../helper/shared_variables.dart';
import '../models/order_model.dart';
import '../models/saved_location_model.dart';
import '../theme/colors.dart';
import '../widgets/add_location_phase.dart';
import '../widgets/bill_widget.dart';
import '../widgets/check_order.dart';
import '../widgets/destination_widget.dart';
import '../widgets/get_avaliable_driver.dart';
import '../widgets/rating_trip.dart';
import '../widgets/source_widget.dart';
import '../widgets/start_order.dart';
import '../widgets/start_trip.dart';
import '../widgets/tad_snackbar_error.dart';
import 'driver_provider.dart';

class MapProvider with ChangeNotifier {
/////////////////////////////////map variable ////////////////////////////
  GoogleMapController? gmc;
  CameraPosition? kGooglePlex;
  Marker? startMarker;
  Marker destinationMarker = const Marker(markerId: MarkerId("2"));
  Marker? addLocationMarker;
  bool? startOrder = false;
  LatLng? source;
  LatLng? destination;
  bool? showDestination;
  Set<Marker>? myMarker = {};
  Set<Marker>? addLocation = {};
  List<LatLng> polylineCoordinates = [];
  List<LatLng> polylineCoordinatesFromDriverToSource = [];

  PolylinePoints polylinePoints = PolylinePoints();
  final Set<Polyline> polyline = {};
  bool choseDes = false;
  bool choseSour = false;
  bool firstTime = true;
  bool drawRoute = false;
  String? sourceDetails;
  String? desDetails;
  bool getDriver = false;
  OrderModel? orderModel;
  TextEditingController markerDetailsController = TextEditingController();
  TextEditingController markerDesDetatilsContoller = TextEditingController();

  TextEditingController cobonCodeController = TextEditingController();
  bool isButtonPressed=false;

  bool checkHome = false;
  bool checkWork = false;
  Timer? timerForTracer;

  String driverArrivingTime='';

  late final BitmapDescriptor startMarkerIcon;
  late final BitmapDescriptor onMoveIcon;

  //// user addresses ///
  List<SavedLocationModel> userAdddressesPointList = [];
  /// user addresses

  final List<Widget> appPhysis = [
    Consumer<MapProvider>(
      builder: (context, mapProvider, _) => Consumer<DriverProvider>(
        builder: (context, driverProvider, _) => SourceWidget(
            isFavourite: mapProvider.isFavourite,
            sour: true,
            des: false,
            markerDetailsController: mapProvider.markerDetailsController,
            sourceDetailsContoller: driverProvider.sourceDetails,
            onPress: () {
              if (driverProvider.sourceDetails.text.isEmpty) {
                mapProvider.changeFiledColor(driverProvider.sourceDetails.text, context);
                // ScaffoldMessenger.of(context).showSnackBar(MySnackBar(
                //     text: "الرجاء إدخال التفاصيل",
                //     type: MySnackBarType.error,
                //     context: context));
              } else {
                ScaffoldMessenger.of(context).showSnackBar(MySnackBar(
                    text: "حدد مكان الوصول  ",
                    type: MySnackBarType.success,
                    context: context));
                mapProvider.makeLocationCheckFasle();
                mapProvider.chooseSourceFunc();
              }
            },),
      ),
    ),
    Consumer<MapProvider>(
      builder: (context, mapProvider, _) => Consumer<DriverProvider>(
        builder: (context, driverProvider, _) => DestinationWidget(
            sour: false,
            des: true,
            controller: mapProvider.markerDesDetatilsContoller,
            hintText: "أدخل تفاصيل عن مكان الوصول",
            onPress: () async {
              if (mapProvider.markerDesDetatilsContoller.text.isEmpty) {
                ScaffoldMessenger.of(context).showSnackBar(
                  MySnackBar(
                      text: "الرجاء إدخال التفاصيل",
                      type: MySnackBarType.error,
                      context: context),
                );
              } else {
                if (mapProvider.source != null &&
                    mapProvider.destination != null) {
                  mapProvider.choseDes = false;
                  await driverProvider.getTripTypes();
                  await mapProvider
                      .onCameraMove(mapProvider.destinationMarker.position);
                  await mapProvider.setPolyLine(
                      mapProvider.source!, mapProvider.destination!);
                }
              }
            }),
      ),
    ),
    StartOrder(),
    GetAvailableDriver(),
    CheckOrderWidget(),
    StartTrip(),
    BillWidget(),
    RatingTravel(),
    AddLocationPhase(),
  ];
  double zoom = 18.55;

  ///////change button theme for start order widget/////
  void changeButtonTheme(){
    isButtonPressed = !isButtonPressed;
    notifyListeners();
  }

  bool isFavourite=false;
  void changeIsFavourite(){
    isFavourite=!isFavourite;
    notifyListeners();
  }
  /////////////////////////////////map functions ////////////////////////////

  void camerMoveToDestination(LatLng newLatLang) async {
    await gmc!.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(target: newLatLang, zoom: zoom),
      ),
    );
  }

  Color filedColor = Colors.white;
  void changeFiledColor(String value, BuildContext context) {
    if (value.isEmpty) {
      filedColor = Theme.of(context).colorScheme.secondary;
    } else {
      filedColor = Colors.white;
    }
    notifyListeners();
  }

  void chooseSourceFunc() {
    if (source != null) {
      choseSour = false;
      choseDes = true;
      destinationMarker = Marker(
          markerId: const MarkerId("2"),
          icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
          position: LatLng(startMarker!.position.longitude,
              startMarker!.position.longitude));
      myMarker!.add(destinationMarker);
      changeIndex(1);
    }
    notifyListeners();
  }

  changeIndex(int newIndex) {
    index = newIndex;
    notifyListeners();
  }

  void makeLocationCheckFasle() {
    checkHome = false;
    checkWork = false;
    notifyListeners();
  }

  Future<void> initState() async {
    polylinePoints = PolylinePoints();
    await initValuesState();
    notifyListeners();
  }

  Future<void> initalMarker() async {
    startMarkerIcon =
    await BitmapDescriptor.fromAssetImage(
      ImageConfiguration(
          size: Size(
        20,
        20,
      )
      ),
      "assets/images/nave_1.png",
    );
    onMoveIcon = await BitmapDescriptor.fromAssetImage(
      ImageConfiguration(
          size: Size(
        20,
        20,
      )
      ),
      "assets/images/nave_1.png",
    );
  } 

  Future<void> initValuesState() async {
    polylineCoordinates.clear();
    myMarker!.clear();
    polyline.clear();
    changeIndex(0);
    startOrder = true;
    source = null;
    destination = null;
    firstTime = true;
    choseSour = true;
    choseDes = false;
    startMarker = null;
    getDriver = false;
    if (firstTime == true) {
      print('first timeee here');
     if( await handleLocationPermission())
      await goToMyLocation();
    }
    notifyListeners();
  }

  Future<void> initalForAddLocation() async {
    addLocation!.clear();
    changeIndex(7);
    await goToMyLocation();
    notifyListeners();
  }

   bool isPermission = false;
  Future<bool> handleLocationPermission( ) async {

    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    // serviceEnabled = await Geolocator.isLocationServiceEnabled();
    // if (!serviceEnabled) {
    //   await Geolocator.requestPermission();
      // serviceEnabled = await Geolocator.isLocationServiceEnabled();
      // if (!serviceEnabled) {
      //   return Future.error('Location services are disabled.');
      // }
    // }
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // return Future.error('Location permissions are denied');
        return false;
      }
    }
    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return false;
      // return Future.error(
      //     'Location permissions are permanently denied, we cannot request permissions.');
    }
    return true;

  }

  Future<void> goToMyLocation() async {

      if (myMarker!.isNotEmpty) {
        myMarker!.clear();
      }
      try {
        final Position position;
        position = await Geolocator.getCurrentPosition();
        print(position);
        source = LatLng(position.altitude, position.longitude);
        if (index == 7) {
          addLocationMarker = Marker(
            icon: BitmapDescriptor.defaultMarkerWithHue(
              BitmapDescriptor.hueYellow,
            ),
            markerId: const MarkerId("addLocation"),
            position: LatLng(position.latitude, position.longitude),
            onTap: () {},
          );
          addLocation!.add(addLocationMarker!);
          return;
        }

        startMarker = Marker(
          markerId: const MarkerId("1"),
          position: LatLng(position.latitude, position.longitude),
          onTap: () {},
        );

        kGooglePlex = CameraPosition(
          target: LatLng(position.latitude, position.longitude),
          zoom: zoom,
        );
        await onCameraMove(
          LatLng(position.latitude, position.longitude),
        );
        sourceDetails =
        await getAddress('${position.latitude},${position.longitude}');
        markerDetailsController.text = sourceDetails ?? "حرك المؤشر";
      }catch(e){
        print('error from catch in goto location $e');
      }
      notifyListeners();
    // }
  }



  bool makeInfoContainerVisible = true;
  bool changeIocn = false;
  Future<void> onCameraMove(LatLng position) async {
    try {
      makeInfoContainerVisible = false;
      notifyListeners();
      if (firstTime == false) {
        return;
      }
      if (index == 7) {
        addLocation!.clear();
        addLocationMarker = Marker(
          markerId: const MarkerId("addLocation"),
          icon: BitmapDescriptor.defaultMarkerWithHue(
            BitmapDescriptor.hueYellow,
          ),
          position: position,
        );
        addLocation!.add(addLocationMarker!);
        sourceDetails =
        await getAddress('${position.latitude},${position.longitude}');
        notifyListeners();
        return;
      } else if (choseSour == true) {
        source = position;
        myMarker!.clear();

        startMarker = Marker(
          markerId: const MarkerId("1"),
          position: position,
          icon: startMarkerIcon,
        );
        myMarker!.add(startMarker!);
        sourceDetails =
        await getAddress('${position.latitude},${position.longitude}');
        markerDetailsController.text = sourceDetails ?? "حرك المؤشر";
      } else if (choseDes == true) {
        destination = LatLng(position.latitude, position.longitude);
        myMarker!.clear();
        destinationMarker = Marker(
            markerId: const MarkerId("2"),
            icon: startMarkerIcon,
            position: position);
        myMarker!.add(startMarker!);
        myMarker!.add(destinationMarker);
        desDetails =
        await getAddress('${position.latitude},${position.longitude}');
        markerDesDetatilsContoller.text = desDetails ?? "حرك المؤشر";
      }
      makeInfoContainerVisible = true;
      notifyListeners();
    }
    catch(e){
      print('on camera move catch error $e');
    }
  }

  Future<void> setPolyLine(LatLng source, LatLng destination) async {
    drawRoute = false;
    notifyListeners();

    print("the length of markers ${myMarker!.length}");
    drawRoute = true;
    try {
      final result = await polylinePoints.getRouteBetweenCoordinates(
        API_KEY,
        PointLatLng(source.latitude, source.longitude),
        PointLatLng(destination.latitude, destination.longitude),
      );

      if (result.status == "OK") {
        polylineCoordinates.clear();
        polyline.clear();

        result.points.forEach((PointLatLng point) {
          polylineCoordinates.add(LatLng(point.latitude, point.longitude));
        });

        polyline.add(
          Polyline(
              consumeTapEvents: true,
              patterns: [
                PatternItem.gap(0),
                PatternItem.dash(100),
              ],
              polylineId: const PolylineId("3"),
              color: kTeal100,
              points: polylineCoordinates),
        );
      }
      drawRoute = false;
      kGooglePlex = CameraPosition(
        target: destinationMarker.position,
        zoom: 15.55
        // zoom,
      );
      changeIndex(2);
    } catch (e) {
      print("error has been happened while draw the route $e");
      drawRoute = false;
    }
    notifyListeners();
  }

  Future<void> getOrderFromSh() async {
    final sh = await SharedPreferences.getInstance();
    final String? orderString = sh.getString('order');
print('getting order from sharedprefss  $orderString');
    if (orderString == null) {
      await initState();
    } else {
      Map<String, dynamic> orderJson = jsonDecode(orderString);
      orderModel = OrderModel.fromJson(orderJson);
      sourceDetails = orderModel!.sourceDetails;
      desDetails = orderModel!.destenationDetails;
      await sh.setInt("driverId", orderModel!.driverId);
      final sourceLocationList = orderModel!.sourceLocation.split(',');
      final sourceLatlang = LatLng(
        double.parse(sourceLocationList[0]),
        double.parse(
          sourceLocationList[1],
        ),
      );
      startMarker = Marker(
        markerId: const MarkerId("1"),
        position: sourceLatlang,
      );

      myMarker!.add(startMarker!);
      final destinationLocationList = orderModel!.destenationLocation.split(',');
      final destinationLatlang = LatLng(
        double.parse(destinationLocationList[0]),
        double.parse(
          destinationLocationList[1],
        ),
      );
      destinationMarker = Marker(
        icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
        markerId: const MarkerId("2"),
        position: destinationLatlang,
      );
      myMarker!.add(destinationMarker);
      choseDes = true;
      choseSour = true;
      firstTime = false;
      await setPolyLine(sourceLatlang, destinationLatlang);

      final mapString = await sh.getString("driverMarker");
      if (mapString != null) {
        final Map<String, dynamic> decodedMap = json.decode(mapString);
        print("the decodedMap is : $decodedMap");

        final Marker? driverMarker = Marker(
          infoWindow: InfoWindow(
            title: "موقع السيارة",
          ),
          icon: BitmapDescriptor.defaultMarkerWithHue(
            BitmapDescriptor.hueYellow,
          ),
          markerId: const MarkerId("driverCar"),
          position: LatLng(
            decodedMap['lat'],
            decodedMap['lon'],
          ),
        );
        print("driver marker is : $driverMarker");
        myMarker!.add(driverMarker!);
      }

      changeIndex(5);
    }
  }

  Future<void> handelCheckLocation(int type, bool value, LatLng checkedLocation,
      bool source, bool des) async {
    if (type == 1) {
      if (value == false) {
        checkHome = value;
        notifyListeners();
        return;
      }
      if (source) {
        checkHome = value;

        checkWork = false;
        choseSour = true;
        this.source = checkedLocation;
        await onCameraMove(this.source!);
        camerMoveToDestination(this.source!);
      } else if (des) {
        choseSour = false;
        choseDes = true;
        checkHome = value;
        checkWork = false;
        this.destination = checkedLocation;
        await onCameraMove(this.destination!);
        camerMoveToDestination(this.destination!);
      }
    }
    if (type == 2) {
      if (value == false) {
        checkWork = value;
        notifyListeners();
        return;
      }
      if (source) {
        checkWork = value;
        checkHome = false;
        choseSour = true;
        this.source = checkedLocation;
        await onCameraMove(this.source!);
        camerMoveToDestination(this.source!);
      } else if (des) {
        choseSour = false;
        choseDes = true;
        checkWork = value;
        checkHome = false;
        this.destination = checkedLocation;
        await onCameraMove(this.destination!);
        camerMoveToDestination(this.destination!);
      }
    }
  }

  void addDriverMarker(LatLng driverLatLang) async {
    BitmapDescriptor markerbitmap = await BitmapDescriptor.fromAssetImage(
      ImageConfiguration(
        size: Size(
          48,
          48,
        )
      ),
      "assets/images/car1.png",
    );
    final Marker driverMarker = Marker(
        markerId: MarkerId('driverMarker'),
        position: driverLatLang,
        icon: markerbitmap);

    myMarker!.add(driverMarker);
    polyline.where((element) => element.polylineId == 10);
    await setDriverSourceLine(source!, driverLatLang);
    print('driver marker addeddd');
  }

  Future<void> setDriverSourceLine(LatLng source, LatLng driverLocation) async {
    try {
      final result = await polylinePoints.getRouteBetweenCoordinates(
        API_KEY,
        PointLatLng(source.latitude, source.longitude),
        PointLatLng(driverLocation.latitude, driverLocation.longitude),
      );

      if (result.status == "OK") {
        polylineCoordinatesFromDriverToSource.clear();
        result.points.forEach((PointLatLng point) {
          polylineCoordinatesFromDriverToSource
              .add(LatLng(point.latitude, point.longitude));
        });

        polyline.add(
          Polyline(
              polylineId: const PolylineId("10"),
              consumeTapEvents: true,
              patterns: [
                PatternItem.gap(5),
                PatternItem.dash(5),
              ],
              width: 3,
              color: Color(0xFF662C90),
              points: polylineCoordinatesFromDriverToSource),
        );
        print('driver routed drawn success !');
      }
    } catch (e) {
      print("error has been happened while draw the route $e");
    }
  }

}
