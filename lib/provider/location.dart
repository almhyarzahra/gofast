import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../helper/App_Bass.dart';
import '../helper/http_exception.dart';

class SaveClientData with ChangeNotifier {
  Future<void> sendHomeLocation(
      String token, int clientId, String Locationh) async {
    print('before await');

    var url = Uri.parse(APP_BASE_URL +
        '/MobileClient/saveHome?clientId=$clientId&homelocation=$Locationh');

    print('before await');
    try {
      final response = await http.get(url, headers: {
        "Content-Type": "application/json",
        'Authorization': 'Bearer $token'
      });
      print('after await ${response.statusCode}');
      if (response.statusCode != 200) {
        print(response.statusCode);
        print('Error when Adding home location');
      }

      print('response is ${response.body}');
      //  notifyListeners();

    } catch (Exception) {
      print(Exception.toString());
      throw (HttpException('error'));
    }
  }

  Future<void> sendWorkLocation(
      String token, int clientId, String Locationw) async {
    print('before await');

    var url = Uri.parse(APP_BASE_URL +
        '/MobileClient/saveWork?clientId=$clientId&worklocation=$Locationw');

    print('before await');
    try {
      final response = await http.post(url, headers: {
        "Content-Type": "application/json",
        'Authorization': 'Bearer $token'
      });
      print('after await ${response.statusCode}');
      if (response.statusCode != 200) {
        print(response.statusCode);
        print('Error when Adding work location');
      }

      print('response is ${response.body}');
      //  notifyListeners();

    } catch (Exception) {
      print(Exception.toString());
      throw (HttpException('error'));
    }
  }

  Future<void> sendFavoritLocation(
      String token, int clientId, String Locationf) async {
    print('before await');

    var url = Uri.parse(APP_BASE_URL +
        '/MobileClient/saveFavourite?clientId=$clientId&favlocation=$Locationf');

    print('before await');
    try {
      final response = await http.post(url, headers: {
        "Content-Type": "application/json",
        'Authorization': 'Bearer $token'
      });
      print('after await ${response.statusCode}');
      if (response.statusCode == 200) {

      }
else{
        print(response.statusCode);
        print('Error when Adding favorit location');
      }
      print('response is ${response.body}');
      //  notifyListeners();

    } catch (Exception) {
      print(Exception.toString());
      throw (HttpException('error'));
    }
  }
}
