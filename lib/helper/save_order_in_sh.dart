import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

import '../models/order_model.dart';

Future<void> saveOrderInShardePreference(OrderModel orderModel) async {
  final sh = await SharedPreferences.getInstance();
  if (sh.containsKey('order')) {
    await sh.remove('order');
    String orderString = jsonEncode(orderModel);
    await sh.setString("order", orderString);
  }
}
