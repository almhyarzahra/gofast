// ignore_for_file: unnecessary_null_comparison

import 'dart:convert';

import 'package:http/http.dart' as http;
import '../common/tools.dart';

Future<String> getAddress(String latlng) async {
  final url =
      'https://maps.googleapis.com/maps/api/geocode/json?latlng=$latlng&language=ar&key=$API_KEY';
  final response = await http.get(Uri.parse(url));
  // print(response.body);
  // print(response.statusCode);
  final responseData = json.decode(response.body);
  if (responseData['results'].length >= 5) {
    return responseData['results'][4]['formatted_address'];
  } else {
    return responseData['results'][3] != null
        ? responseData['results'][3]['formatted_address']
        : responseData['results'][2]['formatted_address'];
  }
}
