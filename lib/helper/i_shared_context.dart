import 'package:flutter/cupertino.dart';

abstract class ISharedContext {
  static BuildContext? _sharedContext;

  static void init(BuildContext context) => _sharedContext = context;

  BuildContext get sharedContext => _getSharedContex();

  static BuildContext _getSharedContex() {
    if (_sharedContext != null) {
      return _sharedContext!;
    } else {
      throw Exception('Shared Context is not initailze dio');
    }
  }
}
