class OrderModel {
  int orderId;
  int orderType;
  int? paid;
  int clientId;
  int driverId;
  String orderStat;
  String sourceDetails;
  String destenationDetails;
  String sourceLocation;
  String destenationLocation;


  OrderModel({
    required this.orderId,
    required this.orderType,
    required this.paid,
    required this.clientId,
    required this.driverId,
    required this.orderStat,
    required this.sourceDetails,
    required this.destenationDetails,
    required this.sourceLocation,
    required this.destenationLocation,
  });

  factory OrderModel.fromJson(Map<String, dynamic> json) => OrderModel(
        orderId: json['orderId'],
        orderType: json['orderType']??0,
        paid: json['paid'] ,
        clientId: json['clientId'],
        driverId: json['driverId'],
        orderStat: json['status'] ?? '0',
        sourceDetails: json['sourceDetails'] ?? '',
        destenationDetails: json['destenationDetails']?? '',
        sourceLocation: json['sourceLocation'] ?? '',
        destenationLocation: json['destenationLocation']?? '',
      );

  Map<String, dynamic> toJson() => {
        "orderId": this.orderId,
        "paid": this.paid,
        "orderType": this.orderType,
        "clientId": this.clientId,
        "driverId": this.driverId,
        "status": this.orderStat,
        "sourceLocation": this.sourceLocation,
        "destenationLocation": this.destenationLocation,
        "sourceDetails": this.sourceDetails,
        "destenationDetails": this.destenationDetails
      };
}
