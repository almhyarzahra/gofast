class BillModel {
  final double totalDistancePrice;
  final double hoursPrice;
  final double waittingPrice;
  final double counterOpenPrice;
  final double totalPric;

  BillModel({
    required this.totalDistancePrice,
    required this.counterOpenPrice,
    required this.hoursPrice,
    required this.waittingPrice,
    required this.totalPric,
  });
}
