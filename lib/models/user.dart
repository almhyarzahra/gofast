class User {
  int clientId;
  String clientName;
  String clientNumber;
  String clientMobile;
  bool clientGender;
  String? clientBirthday;
  bool isActive;
  String? notes;
  String token;
  String? address;
  String? mainphoto;
  int activationCodesId;
  int activationCodeDuration;
  String? code;
  String? orders;

  User(
      {required this.clientId,
      required this.clientNumber,
      required this.clientName,
      required this.clientMobile,
      required this.clientGender,
      required this.token,
      required this.activationCodesId,
      required this.activationCodeDuration,
      required this.isActive,
      required this.code,
      this.clientBirthday,
      this.address,
      this.mainphoto,
      this.notes,
      this.orders});

  factory User.fromJson(Map<String, dynamic> json) => User(
        clientId: json['clientId'] as int,
        clientNumber: json["clientNumber"].trim(),
        clientName: json["clientName"].trim(),
        clientMobile: json["clientMobile"].trim(),
        clientGender: json["clientGender"],
        clientBirthday: json["clientBirthday"].toString(),
        token: json["token"],
        activationCodesId: json['activationCodes']['\$values'].isEmpty
            ? 0
            : json['activationCodes']['\$values'][0]['id'],
        activationCodeDuration: json['activationCodes']['\$values'].isEmpty
            ? 0
            : json['activationCodes']['\$values'][0]['duration'],
        code: json['activationCodes']['\$values'].isEmpty
            ? ""
            : json['activationCodes']['\$values'][0]['code'],
        isActive: json['isActive'] as bool,
      );

  Map<String, dynamic> toJson() => {
        "clientId": this.clientId,
        "clientNumber": this.clientNumber,
        "clientName": this.clientName,
        "clientMobile": this.clientMobile,
        "clientGender": this.clientGender,
        "clientBirthday": this.clientBirthday,
        "token": this.token,
        "activationCodes":{
          "\$values": [{
            "id": this.activationCodesId,
            "duration": this.activationCodeDuration,
            "code": this.code
          }
          ],
        },
        "isActive": this.isActive,
      };
}
