
import 'package:intl/intl.dart';

class PaymentModel {
int paymentId;
int paid;
String paymentdate;
int clientId;
int isfromorder;


PaymentModel({
required this.paymentId,
required this.paid,
required this.clientId,
required this.paymentdate,
required this.isfromorder,
});

factory PaymentModel.fromJson(Map<String, dynamic> json) => PaymentModel(
paymentId: json['id'],
paid: json['paid'] ,
clientId: json['clientId'],
paymentdate:DateFormat('yyyy-MM-dd').format( DateTime.parse(json['paymentdate'])),
isfromorder: json['isfromorder'] ?? 1,
);

Map<String, dynamic> toJson() => {
"id": this.paymentId,
"paid": this.paid,
"clientId": this.clientId,
"paymentdate": this.paymentdate,
"isfromorder": this.isfromorder,
};
}
