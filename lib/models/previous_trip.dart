class PreviousTrip {
  String? userid;
  String? id;
  String? date;
  String? time;
  String? price;
  String? startPointAddress;
  String? startPointCordinates;
  String? endPointAddress;
  String? endPointCordinates;
  Map<String, dynamic>? namedriver;
  bool? isshowdetails;
  PreviousTrip(
      {required this.userid,
      required this.id,
      required this.namedriver,
      required this.date,
      required this.startPointCordinates,
      required this.endPointAddress,
      required this.startPointAddress,
      required this.endPointCordinates,
      required this.price,
      required this.time,
      this.isshowdetails = false});
}



