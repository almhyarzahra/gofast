class OrderTypeModel {
  int id;
  String typeNmae;
  String description;
  double prices;
  String? favouriteTypes;
  String? order;
  String? image;
  int? arriving;

  OrderTypeModel({
    required this.id,
    required this.typeNmae,
    required this.description,
    required this.prices,
    this.favouriteTypes,
    this.order,
    this.image,
    this.arriving,
  });

  factory OrderTypeModel.fromJson(Map<String, dynamic> json) => OrderTypeModel(
        id: json['id'],
        typeNmae: json['typename'],
        description: json['description'],
        prices: json['prices'] * 1.0,
        favouriteTypes: json['favouriteTypes'] == null
            ? null
            : json['favouriteTypes'].toString(),
        order: json['order'] == null ? null : json['favouriteTypes'].toString(),
        arriving: json['arriving'],
      );
}
