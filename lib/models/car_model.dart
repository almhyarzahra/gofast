class CarModel {
  int id;
  int madeyear;
  int driverId;
  String carNumber;
  String status;
  String manufacture;
  String tarvelDistance;
  String carModel;
  String location;

  CarModel({
    required this.carModel,
    required this.driverId,
    required this.carNumber,
    required this.id,
    required this.location,
    required this.madeyear,
    required this.manufacture,
    required this.status,
    required this.tarvelDistance,
  });

  factory CarModel.fromJson(Map<String, dynamic> json) => CarModel(
        carModel: json["carModel"].trim(),
        driverId: json["driverId"],
        carNumber: json["carNumber"].trim(),
        id: json["id"],
        location: json["location"].trim(),
        madeyear: json["madeyear"],
        manufacture: json["manufacture"].trim(),
        status: json["status"].trim(),
        tarvelDistance: json["tarvelDistance"].trim(),
      );

  Map<String, dynamic> toJson() => {
        "id": this.id,
        "carNumber": this.carNumber,
        "status": this.status,
        "madeyear": this.madeyear,
        "manufacture": this.manufacture,
        "tarvelDistance": this.tarvelDistance,
        "carModel": this.carModel,
        "driverId": this.driverId,
        "location": this.location,
      };
}
