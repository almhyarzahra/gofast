import 'package:google_maps_flutter/google_maps_flutter.dart';

class PredictionModel {
  String? placeId;
  String? reference;
  LatLng? location;
  String? mainText;
  int? addressId;
  int? clientId;
  int? typeid;

  PredictionModel({
    this.placeId,
    this.location,
    this.mainText,
    this.reference,
    this.addressId,
    this.clientId,
    this.typeid,
  });

  factory PredictionModel.fromJson(Map<String, dynamic> json) =>
      PredictionModel(
        placeId: json['place_id'],
        mainText: json['structured_formatting']['main_text'],
        reference: json['reference'],
      );
}
