import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class Sugesstion {
  String? id;
  String? image;
  String? titleCar;
  String? price;
  bool? is_selected;

  Sugesstion(
      {@required this.image,
      @required this.titleCar,
      @required this.price,
      this.is_selected = false,
      @required this.id});
}
