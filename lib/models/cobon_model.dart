import 'user.dart';

class CobonModel {
  int id;
  int cobonValue;
  String creationDate;
  String activationCode;
  bool activated ;
  int clientId;
  int cobonType;
  User? client;

  CobonModel({
  required this.id,
  required this.cobonValue,
  required this.creationDate,
  required this.activationCode,
    this.activated = false,
  required this.clientId,
  required this.cobonType,
  required this.client,
  });

  factory CobonModel.fromJson(Map<String, dynamic> json) => CobonModel(
    id: json['id'] as int,
    cobonValue: json['cobonValue'],
    creationDate: json['creationDate'],
    activationCode: json['activationCode'],
    activated: json['activated'] as bool,
    clientId: json['clientId'] as int,
    cobonType: json['cobontype'],
    client: json['client'] != null ? User.fromJson(json['client']) : null,
  );

  // Map<String, dynamic> toJson() => {
  //   'id':this.id,
  //   'cobonValue':this.cobonValue,
  //   'creationDate':this.creationDate,
  //   'activationCode':this.activationCode,
  //   "activated": this.activated,
  //   "clientId": this.clientId,
  //   'cobontype':this.cobonType,
  //
  // };
}