class Driver {
  int driverId;
  String driverName;
  String driverGender;
  String certificate;
  String mobile;
  String?  token;
  bool isChocesnOne = false;
  Car? car;
  Driver({
    required this.driverId,
    required this.driverName,
    required this.driverGender,
    required this.certificate,
    required this.mobile,
     this.car,
    required this.token,
  });

  factory Driver.fromJson(Map<String, dynamic> json) => Driver(
        driverId: json['driverId'],
        driverName: json['driverName'],
        driverGender: json['driverGender'],
        certificate: json['certificate'],
        mobile: json['mobile'],
        token: json['token'],
        car: Car.fromJson(
          json["cars"]['\$values'][0],
        ),
      );
}

class Car {
  int? id;
  String? carNumber;
  String? manufacture;
  String? carModel;
  String? tarvelDistance;
  String? status;
  String? carLocation;
  Car({
    this.id,
    this.carNumber,
    this.carModel,
    this.manufacture,
    this.tarvelDistance,
    this.status,
    this.carLocation,
  });
  factory Car.fromJson(Map<String, dynamic> json) => Car(
      id: json['id'],
      carNumber: json['carNumber'],
      manufacture: json['manufacture'],
      carModel: json['carModel'].toString().trim(),
      tarvelDistance: json['tarvelDistance'].toString().trim(),
      status: json['status'],
      carLocation: json['location'],
  );
}
