import 'package:intl/intl.dart';

class FinishedOrder {
  int orderId;
  String orderDate;
  int? rating;
  String? review;
  int approximateCost;
  int cost;
  String orderStat;
  int? paid;

  int orderType;
  int clientId;
  int driverId;
  String sourceDetails;
  String destenationDetails;
  String sourceLocation;
  String destenationLocation;

  String driverarrivingtime;
  String clientarrivingtime;
  String orderstarttime;
  String? waitingtime;
  String? arrivingtime;
  String estimatedarrivingtime;


  FinishedOrder({
    required this.orderId,
    required this.orderDate,
  this.rating,this.review,
    required this.approximateCost,
    required this.cost,
    required this.orderStat,
    required this.paid,
    required this.orderType,
    required this.clientId,
    required this.driverId,
    required this.sourceDetails,
    required this.destenationDetails,
    required this.sourceLocation,
    required this.destenationLocation,
    required this.driverarrivingtime,
    required this.clientarrivingtime,
    required this.orderstarttime,
     this.waitingtime,
     this.arrivingtime,
    required this.estimatedarrivingtime,
  });

  factory FinishedOrder.fromJson(Map<String, dynamic> json) => FinishedOrder(
    orderId: json['orderId'],
    orderDate:DateFormat('yyyy-MM-dd').format( DateTime.parse(json['orderDate'])),
    rating: json['raring'],
    review: json['review'],
    approximateCost: json['approximateCost'],
    cost: json['cost'],
    orderStat: json['status'] ?? '0',
    orderType: json['orderType']??0,
    paid: json['paid'] ,
    clientId: json['clientId'],
    driverId: json['driverId'],
    sourceDetails: json['sourceDetails'] ?? '',
    destenationDetails: json['destenationDetails']?? '',
    sourceLocation: json['sourceLocation'] ?? '',
    destenationLocation: json['destenationLocation']?? '',
    clientarrivingtime:DateFormat('yyyy-MM-dd').format( DateTime.parse(json['clientarrivingtime'])),
    driverarrivingtime:DateFormat('yyyy-MM-dd').format( DateTime.parse(json['driverarrivingtime'])),
    orderstarttime:DateFormat('yyyy-MM-dd').format( DateTime.parse(json['orderstarttime'])),
    arrivingtime: json['arrivingtime'],
    waitingtime: json['waitingtime'],
    estimatedarrivingtime:DateFormat('yyyy-MM-dd').format( DateTime.parse(json['estimatedarrivingtime'])),
  );

  Map<String, dynamic> toJson() => {
    "orderId": this.orderId,
    'orderDate':this.orderDate,
    "rating":this.rating,
    "review":this.review,
    "approximateCost": this.approximateCost,
    "cost": this.cost,
    "paid": this.paid,
    "orderType": this.orderType,
    "clientId": this.clientId,
    "driverId": this.driverId,
    "status": this.orderStat,
    "sourceLocation": this.sourceLocation,
    "destenationLocation": this.destenationLocation,
    "sourceDetails": this.sourceDetails,
    "destenationDetails": this.destenationDetails,
    "driverarrivingtime": this.driverarrivingtime,
    "clientarrivingtime": this.clientarrivingtime,
    "orderstarttime": this.orderstarttime,
    "waitingtime": this.waitingtime,
    "arrivingtime": this.arrivingtime,
    "estimatedarrivingtime": this.estimatedarrivingtime,
  };
}
