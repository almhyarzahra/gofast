import 'package:google_maps_flutter/google_maps_flutter.dart';

class SavedLocationModel {
  int? addressId;
  int? clientId;
  LatLng? location;
  int? typeid;

  SavedLocationModel({
    this.addressId,
    this.clientId,
    this.location,
    this.typeid,
  });
}
