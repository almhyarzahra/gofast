import 'package:flutter/material.dart';

import 'screens/home_page.dart';
import 'screens/location_add_byuser.dart';
import 'screens/previous_trips.dart';
import 'screens/profile_config.dart';
import 'screens/user_profile.dart';
import 'screens/verfiy.dart';

class Routes {
  static var verifyScreenPath = "/verify";
  static var homeScreenPath = "/home";
  static var ratingtravel = "/ratingtravel";
  static var previoustrips = "/previous-trips";
  static var profileConfig = "/config-profile";
  static var userProfile = "/user-profile";
  static var addLocation = '/locationadd';
  static var selectlocatin = '/selectlocatin';
  static var credit = '/credit';
}

var routes = <String, WidgetBuilder>{
  Routes.verifyScreenPath: (context) => const VerifyScreen(),
  Routes.homeScreenPath: (context) => HomePage(),
  Routes.previoustrips: (context) => PreviousTrips(),
  Routes.profileConfig: (context) => ProfileConfig(),
  Routes.userProfile: (context) => UserProfile(),
  Routes.addLocation: (context) => LocationAddByUser(),
};
