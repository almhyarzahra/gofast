import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:provider/provider.dart';

import '../provider/auth_provider.dart';
import '../theme/colors.dart';
import 'start_page_body_widget.dart';
import 'start_page_provider.dart';

class StartPages extends StatelessWidget {
  const StartPages({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    final double height = MediaQuery.of(context).size.height;
    return Consumer<AuthProvider>(
      builder: (context, authProvider, _) => Directionality(
        textDirection: TextDirection.rtl,
        child: Directionality(
          textDirection: TextDirection.rtl,
          child: Scaffold(
            appBar: AppBar(
              systemOverlayStyle: SystemUiOverlayStyle(
                  statusBarColor: Colors.transparent,
                  statusBarBrightness: Brightness.dark,
                  systemNavigationBarIconBrightness: Brightness.dark),
              backgroundColor: Colors.white,
              elevation: 0,
            ),
            body: Consumer<StartPageProvider>(
              builder: (context, startPageProvider, _) => Column(
                children: [
                  Expanded(
                    flex: 4,
                    child: PageView(
                        onPageChanged: (value) {
                          startPageProvider.indexIncrement(value);
                        },
                        scrollDirection: Axis.horizontal,
                        reverse: true,
                        children: [
                          for (var i in startPageProvider.startPages)
                            StartPageWidget(startPageModel: i)
                        ]),
                  ),
                  SizedBox(
                    height: height*0.03,
                    // height: 30,
                  ),
                  Expanded(
                    flex: 1,
                    child: Padding(
                      padding: const EdgeInsets.only(bottom: 2.0),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          startPageProvider.index ==
                                  startPageProvider.startPages.length - 1
                              ? Container(
                                  width: width / 1.5,
                                  child: ElevatedButton(
                                      style: ButtonStyle(
                                        shape: MaterialStateProperty.all<
                                                RoundedRectangleBorder>(
                                            RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(25.0),
                                        )),
                                        elevation: MaterialStateProperty.all(0),
                                        padding: MaterialStateProperty.all(
                                          EdgeInsets.symmetric(
                                              horizontal: 30, vertical: 15),
                                        ),
                                      ),
                                      onPressed: () async {
                                        await authProvider.showStartPageDone();
                                        // await authProvider.checkIsFirstTimeInApp();
                                      },
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text("تسجيل الدخول"),
                                          Icon(Icons.arrow_forward),
                                        ],
                                      )),
                                )
                              : InkWell(
                                  onTap: () async {
                                    await authProvider.showStartPageDone();
                                  },
                                  child: Text(
                                    "تخطي ",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 23,
                                      color: kTeal100,
                                    ),
                                  )),
                          SizedBox(
                            height: height*0.03,
                          ),
                          Container(
                            width: double.infinity,
                            alignment: Alignment.center,
                            child: Directionality(
                              textDirection: TextDirection.ltr,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(15)),
                                        color: startPageProvider.index == 0
                                            ? kTeal50
                                            : Colors.grey[400]),
                                    height: 5.0,
                                    width: startPageProvider.index == 0
                                        ? 22
                                        : 15.0,
                                  ),
                                  SizedBox(
                                    width: 7,
                                  ),
                                  Container(
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(20)),
                                        color: startPageProvider.index == 1
                                            ? kTeal50
                                            : Colors.grey[400]),
                                    height: 5.0,
                                    width: startPageProvider.index == 1
                                        ? 22
                                        : 15.0,
                                  ),
                                  SizedBox(
                                    width: 7,
                                  ),
                                  Container(
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(20)),
                                        color: startPageProvider.index == 2
                                            ? kTeal50
                                            : Colors.grey[400]),
                                    height: 5.0,
                                    width: startPageProvider.index == 2
                                        ? 22
                                        : 15.0,
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
