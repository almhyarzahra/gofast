import 'package:flutter/material.dart';

import '../theme/colors.dart';
import 'page_model.dart';

class StartPageWidget extends StatelessWidget {
  const StartPageWidget({Key? key, required this.startPageModel})
      : super(key: key);

  final StartPageModel startPageModel;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Expanded(
            flex: 1,
            child: Text(
              startPageModel.title,
              style: TextStyle(
                fontSize: 30,
                color: kTeal100,
              ),
            ),
          ),
          Expanded(
            flex: 9,
            child: Container(
              height:MediaQuery.of(context).size.height*0.4,
              // 380,
              child: Image.asset(startPageModel.image),
            ),
          ),
          Expanded(
            flex: 1,
            child: Text(
              startPageModel.subTitle,
              style: TextStyle(
                fontSize: 35,
                color: kTeal100,
              ),
            ),
          ),
          startPageModel.description == null &&
                  startPageModel.description2 == null
              ? Container()
              : Expanded(
                  flex: 1,
                  child: Column(
                    children: [
                      Text(
                        startPageModel.description ?? "",
                        style: TextStyle(
                          color: Colors.grey[500],
                        ),
                      ),
                      Text(
                        startPageModel.description2 ?? "",
                        style: TextStyle(
                          color: Colors.grey[500],
                        ),
                      ),
                    ],
                  ),
                ),
        ],
      ),
    );
  }
}
