import 'package:flutter/cupertino.dart';

import 'page_model.dart';

class StartPageProvider extends ChangeNotifier {
  List<StartPageModel> startPages = [
    StartPageModel(
      title: "احصل على تجربة جديدة !",
      image: "assets/images/slide-2.png",
      subTitle: "اطلب الرحلة ",
      description: "اطلب رحلتك الان ليصلك أقرب سائق",
      description2: "ضمن نطاق تواجدك",
    ),
    StartPageModel(
      title: "احصل على تجربة جديدة !",
      image: "assets/images/slide-1.png",
      subTitle: "قم بتأكيد سائقك",
      description: "اختر الكابتن المناسب لرحلتك",
      description2: "واحصل على الراحة والأمان والمصداقية",
    ),
    StartPageModel(
      title: "احصل على تجربة جديدة !",
      image: "assets/images/slide-3.png",
      subTitle: " تتبع تفاصيل رحلتك ",
    ),
  ];
  int index = 0;

  void indexIncrement(int newIndex) {
    index = newIndex;
    notifyListeners();
  }
}
