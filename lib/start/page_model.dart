class StartPageModel {
  String title;
  String image;
  String subTitle;
  String? description;
  String? description2;

  StartPageModel({
    required this.title,
    required this.subTitle,
    required this.image,
    this.description,
    this.description2,
  });
}
