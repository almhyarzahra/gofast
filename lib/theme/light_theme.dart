import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'colors.dart';
import 'fonts.dart';

TextTheme buildTextTheme(TextTheme base, String? language,
    [String font = 'Raleway']) {
  var newBase = kTextTheme(base, language);
  return newBase
      .copyWith(
        displaySmall: GoogleFonts.getFont(
          font,
          textStyle: newBase.displaySmall!.copyWith(
            fontWeight: FontWeight.w700,
          ),
        ),
        headlineMedium: GoogleFonts.getFont(
          font,
          textStyle: newBase.headlineMedium!.copyWith(
            fontWeight: FontWeight.w700,
          ),
        ),
        headlineSmall: GoogleFonts.getFont(
          font,
          textStyle: newBase.headlineSmall!.copyWith(
            fontWeight: FontWeight.w500,
          ),
        ),
        titleLarge: GoogleFonts.getFont(
          font,
          textStyle: newBase.titleLarge!.copyWith(fontSize: 18.0),
        ),
        bodySmall: GoogleFonts.getFont(
          font,
          textStyle: newBase.bodySmall!.copyWith(
            fontWeight: FontWeight.w400,
            fontSize: 14.0,
          ),
        ),
        titleMedium: GoogleFonts.getFont(
          font,
          textStyle: newBase.titleMedium!.copyWith(
            fontWeight: FontWeight.w400,
            fontSize: 16.0,
          ),
        ),
        labelLarge: GoogleFonts.getFont(
          font,
          textStyle: newBase.labelLarge!.copyWith(
            fontWeight: FontWeight.w400,
            fontSize: 14.0,
          ),
        ),
      )
      .apply(
        displayColor: kGrey900,
        bodyColor: kGrey900,
      )
      .copyWith(
        displayLarge: kHeadlineTheme(newBase).displayLarge!.copyWith(),
        displayMedium: kHeadlineTheme(newBase).displayMedium!.copyWith(),
        headlineSmall: kHeadlineTheme(newBase).headlineSmall!.copyWith(),
        titleLarge: kHeadlineTheme(newBase).titleLarge!.copyWith(),
      );
}

IconThemeData customIconTheme(IconThemeData original) {
  return original.copyWith(color: kGrey900);
}

const ColorScheme kColorScheme = ColorScheme(
  primary: kTeal100,
  secondary: kTeal50,
  surface: kSurfaceWhite,
  background: Colors.white,
  error: kErrorRed,
  onPrimary: kLightBG,
  onSecondary: kGrey900,
  onSurface: kGrey900,
  onBackground: kGrey900,
  onError: kSurfaceWhite,
  brightness: Brightness.light,
);

ThemeData buildLightTheme(String? language, [String fontFamily = 'Raleway']) {
  final base = ThemeData.light();

  return base.copyWith(
    brightness: Brightness.light,
    canvasColor: Colors.white,
    // buttonColor: kTeal400,
    cardColor: Colors.white,
    buttonTheme: const ButtonThemeData(
        colorScheme: kColorScheme,
        textTheme: ButtonTextTheme.normal,
        buttonColor: kDarkBG),
    primaryColorLight: kLightBG,
    primaryIconTheme: customIconTheme(base.iconTheme),
    textTheme: buildTextTheme(base.textTheme, language, fontFamily),
    primaryTextTheme:
        buildTextTheme(base.primaryTextTheme, language, fontFamily),
    iconTheme: customIconTheme(base.iconTheme),
    hintColor: Colors.black26,
    primaryColor: kLightPrimary,
    // accentColor: kLightAccent,
    scaffoldBackgroundColor: kLightBG,
    appBarTheme: const AppBarTheme(
      elevation: 0,
      // ignore: deprecated_member_use
      titleTextStyle: TextStyle(
        color: kDarkBG,
        fontSize: 18.0,
        fontWeight: FontWeight.w800,
      ),
      // textTheme: TextTheme(
      //   titleLarge: TextStyle(
      //     color: kDarkBG,
      //     fontSize: 18.0,
      //     fontWeight: FontWeight.w800,
      //   ),
      // ),
      iconTheme: IconThemeData(
        color: kLightAccent,
      ),
    ),
    pageTransitionsTheme: const PageTransitionsTheme(builders: {
      TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
      TargetPlatform.android: FadeUpwardsPageTransitionsBuilder(),
    }),
    tabBarTheme: const TabBarTheme(
      labelColor: Colors.black,
      unselectedLabelColor: Colors.black,
      labelPadding: EdgeInsets.zero,
      labelStyle: TextStyle(fontSize: 13),
      unselectedLabelStyle: TextStyle(fontSize: 13),
    ),
    colorScheme: kColorScheme
        .copyWith(error: kErrorRed)
        .copyWith(background: Colors.white),
  );
}
