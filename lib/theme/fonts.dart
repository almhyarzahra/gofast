import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

/// Google fonts constant setting: https://fonts.google.com/
TextTheme kTextTheme(theme, String? language) {
  switch (language) {
    // case 'vi':
    //   return GoogleFonts.ralewayTextTheme(theme);
    case 'ar':
      return GoogleFonts.tajawalTextTheme(theme);
    default:
      return GoogleFonts.tajawalTextTheme(theme);
  }
}

TextTheme kHeadlineTheme(theme, [language = 'ar']) {
  switch (language) {
    // case 'vi':
    //   return GoogleFonts.ralewayTextTheme(theme);
    case 'ar':
      return GoogleFonts.tajawalTextTheme(theme);
    default:
      return GoogleFonts.tajawalTextTheme(theme);
  }
}
