import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'helper/i_shared_context.dart';
import 'provider/auth_provider.dart';
import 'provider/credit_provider.dart';
import 'provider/driver_provider.dart';
import 'provider/location.dart';
import 'provider/map_helper_provider.dart';
import 'provider/map_provider.dart';
import 'provider/sugesstion_provider.dart';
import 'provider/user_provider.dart';
import 'routes.dart';
import 'screens/home_page.dart';
import 'screens/login_screen.dart';
import 'start/start_page_provider.dart';
import 'start/start_pages.dart';
import 'theme/index.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(MultiProvider(providers: [
    ChangeNotifierProvider(create: (_) => AuthProvider()..loadTokenFromPrefs()),
    ChangeNotifierProvider(create: (_) => MapHelper()),
    ChangeNotifierProvider(
        create: (_) => MapProvider()..getOrderFromSh()
          ..initalMarker()),
          // ..getOrderFromSh()),
    ChangeNotifierProvider(create: (_) => SugesstionProvider()),
    ChangeNotifierProvider(create: (_) => UserModel()),
    ChangeNotifierProvider(create: (_) => DriverProvider()),
    ChangeNotifierProvider(create: (_) => StartPageProvider()),
    ChangeNotifierProvider(create: (_) => ListTripProvider()),
    ChangeNotifierProvider(create: (_) => SaveClientData()),
  ], child: const MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    ISharedContext.init(context);
    return ScreenUtilInit(
        designSize: const Size(412, 870),
        builder: (BuildContext context, _) {
          return Consumer<AuthProvider>(builder: (context, authProvider, _) {
            return MaterialApp(
              title: 'Takke',
              localizationsDelegates: const [
                GlobalCupertinoLocalizations.delegate,
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
              ],
              supportedLocales: const [
                Locale('en', 'US'),
                Locale(
                    'ar', 'AE'), // OR Locale('ar', 'AE') OR Other RTL locales
              ],
              locale: const Locale('ar', 'AE'),
              debugShowCheckedModeBanner: false,
              theme: buildLightTheme('ar'),
              routes: routes,
              home: authProvider.firstTimeInApp == null
                  ? StartPages()
                  : authProvider.token != null
                      ? HomePage()
                      : LoginScreen(),
            );
          });
        });
  }
}
