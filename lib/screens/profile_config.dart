import 'package:flutter/material.dart';
import '../../widgets/profile_form.dart';

class ProfileConfig extends StatelessWidget {
  const ProfileConfig(
      {this.birthDate, this.gender, this.name, this.fromProfile, Key? key})
      : super(key: key);

  final String? name;
  final String? gender;
  final String? birthDate;
  final bool? fromProfile;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ProfileForm(
        birthDate: birthDate,
        gender: gender,
        name: name,
        fromProfile: fromProfile,
      ),
    );
  }
}
