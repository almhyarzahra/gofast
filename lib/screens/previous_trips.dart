import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import '../models/finished_orders.dart';
import '../models/previous_trip.dart';

import '../provider/auth_provider.dart';
import '../provider/driver_provider.dart';
import '../theme/colors.dart';

class PreviousTrips extends StatefulWidget {
  const PreviousTrips({Key? key}) : super(key: key);

  @override
  _PreviousTripsState createState() => _PreviousTripsState();
}

class _PreviousTripsState extends State<PreviousTrips> {
  bool showdetails = false;


  @override
  Widget build(BuildContext context) {
    var devicewidth = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        title: Text(
          "رجوع",
          style: TextStyle(color: kTeal100),
        ),
      ),
      body: FutureBuilder<List<FinishedOrder>>(
        future: Provider.of<DriverProvider>(context).getFinishOrders(Provider.of<AuthProvider>(context,listen:false).user!.clientName),
        builder: (context, AsyncSnapshot<List<FinishedOrder>> snapshot) {
          if (snapshot.hasData) {
            return SafeArea(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    // SizedBox(
                    //   child: TextButton.icon(
                    //       onPressed: () {
                    //         setState(() {
                    //           Navigator.of(context).pop();
                    //         });
                    //       },
                    //       icon: Icon(
                    //         Icons.arrow_back,
                    //         color: kTeal100,
                    //       ),
                    //       label: Text(
                    //         "رجوع",
                    //         style: TextStyle(
                    //             color: kTeal100,
                    //             fontSize: 20.sp,
                    //             fontWeight: FontWeight.bold),
                    //       )),
                    // ),
                    // Padding(padding: EdgeInsets.symmetric(horizontal: 2)),
                    Row(
                      children: [
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          "الرحلات السابقة",
                          style: TextStyle(fontSize: 16.sp),
                        ),
                      ],
                    ),
                    Expanded(
                      child: ListView.builder(
                          itemCount: snapshot.data!.length,
                          padding: EdgeInsets.all(10),
                          itemBuilder: (context, index) => Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                        width: devicewidth * .7,
                                        padding: EdgeInsets.all(10),
                                        child: Row(
                                          children: [
                                            Text(
                                                '${snapshot.data![index].review ?? '' }'),
                                            SizedBox(
                                              width: 20,
                                            ),
                                            Text(
                                                "${snapshot.data![index].orderDate!} PM"),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        padding: EdgeInsets.all(10),
                                        width: devicewidth * .2,
                                        child: Text(
                                            " ${snapshot.data![index].cost} ل.س"),
                                      ),
                                    ],
                                  ),
                                  Container(
                                    // padding: EdgeInsets.symmetric(horizontal: 10),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Image.asset(
                                          'assets/images/from_to.png',
                                          height: 70.h,
                                        ),
                                        SizedBox(
                                          width: 20.3.w,
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              snapshot.data![index]
                                                  .sourceDetails!,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 14.sp,
                                                  color: Color(0xFF303030)),
                                            ),
                                            Text(
                                              snapshot.data![index]
                                                  .sourceLocation!,
                                              style: TextStyle(
                                                color: Color(0xFFD6D6D6),
                                                fontSize: 11.sp,
                                              ),
                                            ),
                                            SizedBox(
                                              height: 4.h,
                                            ),
                                            Container(
                                              height: 1.0.h,
                                              margin: EdgeInsets.all(0),
                                              width: 200.w,
                                              color: Colors.grey,
                                            ),
                                            SizedBox(
                                              height: 3.h,
                                            ),
                                            Text(
                                              snapshot.data![index]
                                                  .destenationDetails!,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 14.sp,
                                                  color: Color(0xFF303030)),
                                            ),
                                            Text(
                                              snapshot.data![index]
                                                  .destenationLocation!,
                                              style: TextStyle(
                                                color: Color(0xFFD6D6D6),
                                                fontSize: 11.sp,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                          child: showdetails ==
                                                      true
                                              ? Container(
                                                  height: 100.h,
                                                  child: Row(children: [
                                                    // Padding(
                                                    //   padding:
                                                    //       const EdgeInsets.only(
                                                    //           left: 8,
                                                    //           right: 2),
                                                    //   child: Image.asset(
                                                    //     Provider.of<DriverProvider>(context,listen: false).allDrivers.where((element) => element==snapshot.data![index].driverId).first.driverName,
                                                    //     height: 100.h,
                                                    //     width: 90.w,
                                                    //   ),
                                                    // ),
                                                    Container(
                                                      child: Column(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .center,
                                                          children: [
                                                            Text(
                                                              " ${Provider.of<DriverProvider>(context,listen: false).allDrivers.where((element) => element==snapshot.data![index].driverId).first.driverName}",
                                                              style: TextStyle(
                                                                fontSize: 16.sp,
                                                                color:
                                                                    kLightAccent,
                                                              ),
                                                            ),
                                                            // Row(
                                                            //   mainAxisAlignment:
                                                            //       MainAxisAlignment
                                                            //           .spaceBetween,
                                                            //   children: [
                                                            //     Icon(
                                                            //       Icons.star,
                                                            //       color:
                                                            //           kTeal50,
                                                            //       size: 20.sp,
                                                            //     ),
                                                            //     Text(
                                                            //       "${snapshot.data![index].namedriver!['driverrating']}",
                                                            //       style:
                                                            //           TextStyle(
                                                            //         fontSize:
                                                            //             16.sp,
                                                            //         color:
                                                            //             kLightAccent,
                                                            //       ),
                                                            //     )
                                                            //   ],
                                                            // ),
                                                          ]),
                                                    )
                                                  ]))
                                              : null),

                                      InkWell(
                                        onTap: () {
                                          setState(() {
                                            // snapshot.data![index]
                                            //         .isshowdetails =
                                            //     !snapshot.data![index]
                                            //         .isshowdetails!;
                                            showdetails = !showdetails;
                                          });
                                        },
                                        child: AnimatedContainer(
                                          duration: Duration(seconds: 1),
                                          curve: Curves.fastOutSlowIn,
                                          width: devicewidth * .20,
                                          child: Row(
                                            children: [
                                              Text(
                                                'تفاصيل',
                                                style: TextStyle(
                                                  color: kTeal100,
                                                  fontSize: 16.sp,
                                                ),
                                              ),
                                              SizedBox(
                                                width: 3,
                                              ),
                                              showdetails == true
                                                  ? Icon(
                                                      Icons
                                                          .arrow_upward_outlined,
                                                      color: kTeal100,
                                                      size: 16.sp,
                                                    )
                                                  : Icon(
                                                      Icons.arrow_downward,
                                                      color: kTeal100,
                                                      size: 16.sp,
                                                    )
                                            ],
                                          ),
                                        ),
                                      )
                                      // InkWell(
                                      //     onTap: (){},
                                      //     child: Text(" ${Icon(Icons.arrow_downward,color: Colors.purple,)} تفاصيل  ")),
                                    ],
                                  ),
                                  Divider(
                                    color: Colors.black,
                                  )
                                ],
                              )),
                    ),
                  ]),
            );
          } else
            return CircularProgressIndicator();
        },
      ),
    );
  }
}
