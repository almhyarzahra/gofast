import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

import '../provider/driver_provider.dart';

class PaymentScreen extends StatelessWidget {
  const PaymentScreen();

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        leading: Container(),
        actions: [
          IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_forward,
              color: Theme.of(context).colorScheme.secondary,
            ),
          )
        ],
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: Text(
          'محفظتي',
          style: TextStyle(
              color: Theme.of(context).colorScheme.primary,
              fontSize: 20,
              fontWeight: FontWeight.w300),
        ),
        centerTitle: true,
      ),

      body: Consumer<DriverProvider>(
      builder: (context, driverProvider, _) {
        return driverProvider.getPaymentsLoading? Center(child: CircularProgressIndicator(),) : Column(
          children: [
            Expanded(
              child: ListView.builder(
                itemCount: driverProvider.payments.length,
                itemBuilder: (context,index){
                  return Container(
                    margin: EdgeInsets.all(8),
                    padding: EdgeInsets.all(8),
                    child:  Card(
                      elevation: 2,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
                      color: Colors.white70,
                      child: ListTile(
                        leading: Icon(
                          Icons.paid_outlined,
                          color: Theme.of(context).colorScheme.secondary,

                          // color: Theme.of(context).colorScheme.primary,

                        ),
                        title: Row(
                          children: [

                            Text(
                              'المبلغ المدفوع : ',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,

                                  fontWeight: FontWeight.normal,
                                  fontSize: 16.sp),
                            ),
                            Text(
                              'ل.س ${driverProvider.payments[index].paid.toString()}',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,

                                  fontWeight: FontWeight.normal,
                                  fontSize: 16.sp),
                            ),
                          ],
                        ),
                        subtitle: Row(
                          children: [
                            Text(
                              'التاريخ : ',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,

                                  fontWeight: FontWeight.normal,
                                  fontSize: 16.sp),
                            ),
                            Text(
                              driverProvider.payments[index].paymentdate,
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,

                                  fontWeight: FontWeight.normal,
                                  fontSize: 16.sp),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                }),
            ),
          ],
        );
      }
      ),

    );
  }
}
