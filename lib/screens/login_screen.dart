import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

import '../../common/tools.dart';
import '../../theme/colors.dart';
import '../provider/auth_provider.dart';
import 'verfiy.dart';

class LoginScreen extends StatelessWidget {
  LoginScreen({Key? key}) : super(key: key);


  // final RegExp regExp = RegExp(r'(^(?:[+0]9)?[0-9]{10,12}$)');

  final RegExp regExp = RegExp(r'(^0(93|99|94|96|98|95)\d{7}$)');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: SvgPicture.asset("assets/images/logo2.svg"),
        centerTitle: true,
      ),
      body: Consumer<AuthProvider>(builder: (context, authProvider, __) {
        return authProvider.isLoading
            ? const Center(
                child: CircularProgressIndicator(),
              )
            : SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // ClipPath(
                    //   clipper: ShapeBorderClipper(
                    //       shape: RoundedRectangleBorder(
                    //           borderRadius: BorderRadius.only(
                    //               bottomLeft: Radius.elliptical(
                    //     3000,
                    //     900,
                    //   )))),
                    //   child: Container(
                    //     alignment: Alignment.bottomRight,
                    //     width: double.infinity,
                    //     child: Padding(
                    //       padding: const EdgeInsets.symmetric(
                    //           horizontal: 20.0, vertical: 20.0),
                    //       child: InkWell(
                    //         child: Row(
                    //           children: [
                    //             Icon(
                    //               Icons.arrow_back,
                    //               color: Colors.white,
                    //               size: 22,
                    //             ),
                    //             SizedBox(
                    //               width: 25,
                    //             ),
                    //             Text(
                    //               "رجوع",
                    //               style: TextStyle(
                    //                 color: Colors.white,
                    //               ),
                    //             ),
                    //           ],
                    //         ),
                    //       ),
                    //     ),
                    //     decoration: BoxDecoration(
                    //       color: kTeal50,
                    //     ),
                    //     height: 140.h,
                    //   ),
                    // ),
                    SizedBox(
                      height: 50.h,
                    ),
                    Center(child: SvgPicture.asset("assets/images/point.svg")),
                    SizedBox(height: 20.h,),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 36.w),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Center(
                            child: Text(
                              "ادخل رقم الهاتف الخاص بك من أجل انشاء حساب او تسجيل الدخول",
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ),
                          SizedBox(
                            height: 42.h,
                          ),
                          Container(
                            height: 65.0.h,
                            width: 342.w,
                            alignment: AlignmentDirectional.center,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(14.0),
                                color: Colors.white,
                                boxShadow: const [
                                  BoxShadow(
                                      blurRadius: 10.0, color: Colors.black12)
                                ]),
                            child: TextFormField(
                              controller: authProvider.phone,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.only(
                                    left: 68.0.w,
                                    right: 68.0.w,
                                    top: 21.0.h,
                                    bottom: 16.0.h),
                                suffixIcon: Image.asset(
                                  'assets/images/sy.png',
                                  width: 24.w,
                                  height: 15.h,
                                ),
                                // suffixText: '963+',
                                border: InputBorder.none,
                                hintText: 'أدخل رقم هاتفك',
                              ),
                              keyboardType: TextInputType.phone,
                            ),
                          ),
                          SizedBox(
                            height: 25.h,
                          ),
                          //_buildTermsText(context),
                          SizedBox(height: 200.h),
                          InkWell(
                            onTap: () async {
                              if (authProvider.phone.text.isEmpty) {
                                showErrorSnakBar(
                                    context, 'يرجى إدخال رقم الهاتف');
                              } else if (!regExp.hasMatch(authProvider.phone.text)) {
                                showErrorSnakBar(
                                    context, 'يرحى إدخال رقم هاتف صحيح');
                              } else {
                                await authProvider
                                    .login(authProvider.phone.text)
                                    .then((result) {
                                  if (result == false &&
                                      authProvider.loginStatusCode == 200) {
                                    Navigator.pushAndRemoveUntil(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              VerifyScreen(),
                                        ),
                                        (route) => false);
                                  } else {
                                    print(
                                        "the status code is ${authProvider.loginStatusCode}");
                                    showErrorSnakBar(context,
                                        "حدث خطأ ما اثناء تسجيل الدخول ");
                                  }
                                });
                              }
                            },
                            child: Container(
                              
                              width: double.infinity,
                              height: 60.h,
                              padding: const EdgeInsets.symmetric(horizontal: 10),
                              decoration: BoxDecoration(
                                color: kTeal50,
                                borderRadius: BorderRadius.circular(10)
                              ),
                              
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "الموافقة والمتابعة",
                                  ),
                                  Icon(Icons.arrow_forward),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              );
      }),
    );
  }

  Widget _buildTermsText(context) {
    return RichText(
      // textAlign: TextAlign.center,
      text: TextSpan(
        children: <TextSpan>[
          TextSpan(
            text: 'من خلال إنشاء حساب ، فإنك توافق على\n',
            style: TextStyle(
              fontSize: 16.sp,
              color: Colors.black,
            ),
          ),
          TextSpan(
              text: 'بنود الخدمة وخصوصية السياسة',
              style: TextStyle(
                  fontSize: 16.sp,
                  color: Theme.of(context).colorScheme.primary),
              recognizer: TapGestureRecognizer()..onTap = () {}),
        ],
      ),
    );
  }
}
