

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

import '../provider/driver_provider.dart';

class FinishedOrdersScreen extends StatelessWidget {
  const FinishedOrdersScreen();

  @override
  Widget build(BuildContext context) {
   final size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        leading: Container(),
        actions: [
          IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_forward,
              color: Theme.of(context).colorScheme.secondary,
            ),
          )
        ],
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: Text(
          'رحلاتي السابقة',
          style: TextStyle(
              color: Theme.of(context).colorScheme.primary,
              fontSize: 20,
              fontWeight: FontWeight.w300),
        ),
        centerTitle: true,
      ),
      body: Consumer<DriverProvider>(
          builder: (context, driverProvider, _) => driverProvider.getPaymentsLoading? Center(child: CircularProgressIndicator()): Column(
            children:[Expanded(child:
            ListView.builder(
                itemCount: driverProvider.finishedOrders.length,
                itemBuilder: (context,index){
               return Card(
                 margin: EdgeInsets.all(8),
                 elevation: 2,
                 shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
                 color: Colors.white70,
                 child: Column(children: [
                   Row(
                     children: [
                       Text(
                         'تاريخ الطلب : ',
                         style: TextStyle(
                             color: Theme.of(context).colorScheme.primary,
                             fontWeight: FontWeight.normal,
                             fontSize: 16.sp),
                       ),
                       Text(
                         ' ${driverProvider.finishedOrders[index].orderDate.toString()}',
                         style: TextStyle(
                             color: Theme.of(context).colorScheme.primary,
                 
                             fontWeight: FontWeight.normal,
                             fontSize: 16.sp),
                       ),
                     ],
                   ),
                 SizedBox(height: size.height*0.01,),
                   Row(
                     children: [
                       Text(
                         'الكلفة : ',
                         style: TextStyle(
                             color: Theme.of(context).colorScheme.primary,
                             fontWeight: FontWeight.normal,
                             fontSize: 10.sp),
                       ),
                       Text(
                         'ل.س ${driverProvider.finishedOrders[index].cost.toString()}',
                         style: TextStyle(
                             color: Theme.of(context).colorScheme.primary,
                 
                             fontWeight: FontWeight.normal,
                             fontSize: 10.sp),
                       ),
                       SizedBox(width: size.width*0.3,),
                       Text(
                         'المبلغ المدفوع : ',
                         style: TextStyle(
                             color: Theme.of(context).colorScheme.primary,
                             fontWeight: FontWeight.normal,
                             fontSize: 16.sp),
                       ),
                       Text(
                         'ل.س ${driverProvider.finishedOrders[index].paid.toString()}',
                         style: TextStyle(
                             color: Theme.of(context).colorScheme.primary,
                 
                             fontWeight: FontWeight.normal,
                             fontSize: 16.sp),
                       ),
                     ],
                   ),
                   SizedBox(height: size.height*0.01,),
                   Row(
                     children: [
                       Text(
                         'حالة الطلب : ',
                         style: TextStyle(
                             color: Theme.of(context).colorScheme.primary,
                             fontWeight: FontWeight.normal,
                             fontSize: 16.sp),
                       ),
                       Text(
                         ' ${driverProvider.finishedOrders[index].orderStat == '1' ? 'مقبول' :driverProvider.finishedOrders[index].orderStat == '3'? 'مرفوض' : 'مكتمل'}',
                         style: TextStyle(
                             color: Theme.of(context).colorScheme.primary,
                 
                             fontWeight: FontWeight.normal,
                             fontSize: 16.sp),
                       ),
                     ],
                   ),
                   SizedBox(height: size.height*0.01,),
                   Row(
                     children: [
                       Text(
                         'من : ',
                         style: TextStyle(
                             color: Theme.of(context).colorScheme.primary,
                             fontWeight: FontWeight.normal,
                             fontSize: 10.sp),
                       ),
                       Text(
                         ' ${driverProvider.finishedOrders[index].sourceDetails}',
                         style: TextStyle(
                             color: Theme.of(context).colorScheme.primary,
                 
                             fontWeight: FontWeight.normal,
                             fontSize: 10.sp),
                       ),
                 SizedBox(width: size.width*0.05,),
                       Text(
                         ' الى : ',
                         style: TextStyle(
                             color: Theme.of(context).colorScheme.primary,
                             fontWeight: FontWeight.normal,
                             fontSize: 10.sp),
                       ),
                       Text(
                         ' ${driverProvider.finishedOrders[index].destenationDetails}',
                         style: TextStyle(
                             color: Theme.of(context).colorScheme.primary,
                 
                             fontWeight: FontWeight.normal,
                             fontSize: 10.sp),
                       ),
                     ],
                   ),
                   SizedBox(height: size.height*0.01,),
                 
                   Row(
                     children: [
                       Text(
                         'وقت بدء الطلب : ',
                         style: TextStyle(
                             color: Theme.of(context).colorScheme.primary,
                             fontWeight: FontWeight.normal,
                             fontSize: 10.sp),
                       ),
                       Text(
                         ' ${driverProvider.finishedOrders[index].orderstarttime.toString()}',
                         style: TextStyle(
                             color: Theme.of(context).colorScheme.primary,
                 
                             fontWeight: FontWeight.normal,
                             fontSize: 10.sp),
                       ),
                     ],
                   ),
                 
                 ],),
               );
            }),)]
          )
      ),);
  }
}
