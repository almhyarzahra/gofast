import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import '../helper/shared_variables.dart';
import '../provider/driver_provider.dart';
import '../provider/map_provider.dart';
import '../theme/colors.dart';
import '../widgets/app_drawer.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final String assetName = 'assets/images/loc_icon.svg';
    return Scaffold(
      drawer: AppDrawer(),
      body: Consumer<MapProvider>(
        builder: (context, mapProvider, _) => Directionality(
          textDirection: TextDirection.rtl,
          child: Consumer<DriverProvider>(
            builder: (context, driverProvider, _) {
              return Scaffold(
                  body: mapProvider.kGooglePlex != null && mapProvider.startMarker != null
                      ? mapProvider.drawRoute
                          ? Container(
                              width: double.infinity,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  CircularProgressIndicator(),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  Text("جاري رسم الطريق"),
                                ],
                              ),
                            )
                          : SizedBox(
                              width: double.infinity,
                              child: Stack(
                                children: [
                                  GoogleMap(
                                    zoomControlsEnabled: false,
                                    onCameraMove: (position) async {
                                      mapProvider.changeIocn = true;
                                      mapProvider.onCameraMove(
                                        position.target,
                                      );
                                    },
                                    polylines: mapProvider.polyline,
                                    markers: index == 7
                                        ? mapProvider.addLocation!
                                        : mapProvider.myMarker!,
                                    onTap: (position) {
                                      mapProvider.onCameraMove(position);
                                    },
                                    mapType: MapType.terrain,
                                    initialCameraPosition: mapProvider.kGooglePlex!,
                                    onMapCreated: (GoogleMapController controller) async {
                                      mapProvider.makeLocationCheckFasle();
                                      mapProvider.gmc = controller;
                                      mapProvider.myMarker!.add(mapProvider.startMarker!);
                                    },
                                    rotateGesturesEnabled: true,
                                    zoomGesturesEnabled: true,
                                  ),
                                  index == 0 || index == 1
                                      ? Visibility(
                                          visible: mapProvider.makeInfoContainerVisible,
                                          child: AnimatedContainer(
                                            duration: Duration(seconds: 2),
                                            child: mapProvider.appPhysis[index!],
                                          ),
                                        )
                                      : Align(
                                          alignment: Alignment.bottomCenter,
                                          child: BottomSheet(
                                            enableDrag: false,
                                            builder: (context) {
                                              return Column(
                                                mainAxisSize: MainAxisSize.min,
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.symmetric( vertical: 10),
                                                    width: 35,
                                                    height: 5,
                                                    decoration: BoxDecoration(
                                                        color: Colors.grey,
                                                        borderRadius: BorderRadius.horizontal(
                                                                left: Radius.circular(5),
                                                                right: Radius.circular(5))),
                                                  ),
                                                  Container(
                                                      padding:EdgeInsets.symmetric(horizontal: 10),
                                                      decoration: BoxDecoration(
                                                          borderRadius:BorderRadius.only(
                                                            topLeft:Radius.circular(20),
                                                            topRight:Radius.circular(20),
                                                          ),
                                                          color:Theme.of(context).colorScheme.background),
                                                      child: mapProvider.appPhysis[index!]),
                                                ],
                                              );
                                            },
                                            onClosing: () {},

                                            // child: mapProvider.appPhysis[index!],
                                          ),
                                        ),
                                  Positioned(
                                    top: 0,
                                    right: 0,
                                    child: Padding(
                                      padding: EdgeInsets.only(top: 37,),
                                      child: InkWell(
                                        onTap: () {
                                          Scaffold.of(context).openDrawer();
                                        },
                                        child: Container(
                                          width: 60.87.w,
                                          height: 52.43.h,
                                          margin: EdgeInsets.only(right: 14.8.w),
                                          padding: EdgeInsets.only(left: 14.8.w),
                                          alignment: Alignment.centerLeft,
                                          decoration: BoxDecoration(
                                              borderRadius: BorderRadius.horizontal(
                                                      left: Radius.circular(50),
                                                      right: Radius.circular(50)),
                                              color: kTeal50),
                                          child: Icon(
                                            Icons.menu,
                                            size: 30.sp,
                                            color: Colors.white,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                    top: 0,
                                    left: 0,
                                    child: Visibility(
                                      visible: mapProvider.choseSour,
                                      child: InkWell(
                                        onTap: () async {
                                          if (mapProvider.choseSour) {
                                            mapProvider.initState();
                                          }
                                        },
                                        child: SvgPicture.asset(
                                          assetName,
                                          fit: BoxFit.scaleDown,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            )
                      : Center(
                          child: CircularProgressIndicator(),
                        ));
            },
          ),
        ),
      ),
    );
  }
}
