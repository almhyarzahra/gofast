import 'dart:async';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

import '../../common/tools.dart';
import '../../helper/shared_variables.dart';
import '../../theme/colors.dart';
import '../provider/auth_provider.dart';
import '../provider/map_provider.dart';
import '../provider/user_provider.dart';
import '../widgets/verfiy_text_form_fiald.dart';
import 'home_page.dart';
import 'profile_config.dart';

class VerifyScreen extends StatefulWidget {
  const VerifyScreen({Key? key}) : super(key: key);

  @override
  State<VerifyScreen> createState() => _VerifyScreenState();
}

class _VerifyScreenState extends State<VerifyScreen> {
  final TextEditingController _verifcationCodeController =
      TextEditingController();
  int secondsRemaining = 30;
  bool enableResend = false;
  late Timer _timer;
  late GlobalKey verifyFormKey;
  @override
  initState() {
    verifyFormKey = GlobalKey<FormState>();
    super.initState();
    _timer = Timer.periodic(const Duration(seconds: 1), (_) {
      if (secondsRemaining != 0) {
        setState(() {
          secondsRemaining--;
        });
      } else {
        setState(() {
          enableResend = true;
        });
      }
    });
  }

  @override
  void dispose() {
    _timer.cancel();
    _verifcationCodeController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: SvgPicture.asset("assets/images/logo2.svg"),
        centerTitle: true,
      ),
      body: Consumer<AuthProvider>(
        builder: (context, authProvider, _) => SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // ClipPath(
              //   clipper: ShapeBorderClipper(
              //       shape: RoundedRectangleBorder(
              //           borderRadius: BorderRadius.only(
              //               bottomLeft: Radius.elliptical(
              //     3000,
              //     900,
              //   )))),
              //   child: Container(
              //     alignment: Alignment.bottomRight,
              //     width: double.infinity,
              //     child: Padding(
              //       padding: const EdgeInsets.symmetric(
              //           horizontal: 20.0, vertical: 20.0),
              //       child: InkWell(
              //         child: Row(
              //           children: [
              //             Icon(
              //               Icons.arrow_back,
              //               color: Colors.white,
              //               size: 22,
              //             ),
              //             SizedBox(
              //               width: 25,
              //             ),
              //             Text(
              //               "رجوع",
              //               style: TextStyle(
              //                 color: Colors.white,
              //               ),
              //             ),
              //           ],
              //         ),
              //       ),
              //     ),
              //     decoration: BoxDecoration(
              //       color: kTeal50,
              //     ),
              //     height: 140.h,
              //   ),
              // ),
              Center(child: SvgPicture.asset("assets/images/verify.svg")),
              SizedBox(
                height: 50.h,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 36.w),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 1.h,
                    ),
                    Center(
                      child: Text(
                        'كود التأكيد',
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                    SizedBox(
                      height: 40.h,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'أدخل الرمز المكون من 6 أرقام المرسل اليك على',
                          style: TextStyle(fontSize: 16),
                        ),
                        Directionality(
                          textDirection: TextDirection.ltr,
                          child: Text(
                            // '+963 935 787 399 $activationCode',
                            '$activationCode',
                            style: TextStyle(fontSize: 16, color: kTeal100),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 51.h,
                    ),
                    Directionality(
                      textDirection: TextDirection.ltr,
                      child: Form(
                        key: verifyFormKey,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 3, vertical: 5),
                          child: Row(
                            children: [
                              Expanded(
                                flex: 1,
                                child: SizedBox(
                                  width: 10,
                                  child: VerfiyTextFormFaild(
                                    controller: authProvider.verifyNumer1,
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 7,
                              ),
                              Expanded(
                                flex: 1,
                                child: SizedBox(
                                  width: 10,
                                  child: VerfiyTextFormFaild(
                                    controller: authProvider.verifyNumer2,
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 7,
                              ),
                              Expanded(
                                flex: 1,
                                child: SizedBox(
                                  width: 10,
                                  child: VerfiyTextFormFaild(
                                    controller: authProvider.verifyNumer3,
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 7,
                              ),
                              Expanded(
                                flex: 1,
                                child: SizedBox(
                                  width: 10,
                                  child: VerfiyTextFormFaild(
                                    controller: authProvider.verifyNumer4,
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 7,
                              ),
                              Expanded(
                                flex: 1,
                                child: SizedBox(
                                  width: 10,
                                  child: VerfiyTextFormFaild(
                                    controller: authProvider.verifyNumer5,
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 7,
                              ),
                              Expanded(
                                flex: 1,
                                child: SizedBox(
                                  width: 10,
                                  child: VerfiyTextFormFaild(
                                    controller: authProvider.verifyNumer6,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 40.h,
                    ),
                    Center(child: _buildResendText()),
                    SizedBox(
                      height: 50.h,
                    ),
                    Center(
                      child: Consumer<UserModel>(
                          builder: (context, userModel, _) => InkWell(
                            onTap: () async {
                                _verifcationCodeController.text =
                                    authProvider.verifyNumer1.text +
                                        authProvider.verifyNumer2.text +
                                        authProvider.verifyNumer3.text +
                                        authProvider.verifyNumer4.text +
                                        authProvider.verifyNumer5.text +
                                        authProvider.verifyNumer6.text;
                                if (_verifcationCodeController
                                    .text.isEmpty) {
                                  showErrorSnakBar(
                                      context, 'يرجى إدخال رمز التحقق ');
                                } else if (_verifcationCodeController
                                        .text.length <
                                    6) {
                                  showErrorSnakBar(context,
                                      'رمز التحقق يتألف من 6 أرقام على الأقل');
                                } else if (_verifcationCodeController
                                        .text !=
                                    activationCode) {
                                  showErrorSnakBar(context,
                                      'الرمز المدخل لا يطابق الرمز الصحيح');
                                } else if (_verifcationCodeController
                                        .text ==
                                    activationCode) {
                                  await authProvider
                                      .activationCodesCheack();
                                  if (authProvider.needRegister == true) {
                                    Navigator.pushAndRemoveUntil(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              ProfileConfig()),
                                      (route) => false,
                                    );
                                  } else {
                                    Navigator.pushAndRemoveUntil(
                                        context,
                                        MaterialPageRoute(builder: (context) => HomePage()),
                                            (route) => false);
                                    // Navigator.pushReplacement(
                                    //   context,
                                    //   MaterialPageRoute(
                                    //       builder: (context) =>
                                    //           HomePage()),
                                    // );
                                    authProvider.notif();
                                  }
                                }
                              },
                            child: Container(
                                width: double.infinity,
                              height: 60.h,
                              padding: const EdgeInsets.symmetric(horizontal: 10),
                              decoration: BoxDecoration(
                                color: kTeal50,
                                borderRadius: BorderRadius.circular(10)
                              ),
                                
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text("تأكيد الرمز"),
                                    Icon(Icons.arrow_forward),
                                  ],
                                )),
                          )),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildResendText() {
    return RichText(
      textAlign: TextAlign.center,
      text: TextSpan(
        children: <TextSpan>[
          TextSpan(
              style: TextStyle(
                  fontSize: 15.sp,
                  // color: Theme.of(context).colorScheme.secondaryContainer,
                  color: Colors.black),
              text: 'أعد الإرسال خلال '),
          TextSpan(
              text:
                  enableResend ? ' المحاولة مجدداً' : '$secondsRemaining ثانية',
              style: TextStyle(
                  fontSize: 15.sp,
                  fontWeight: FontWeight.bold,
                  color: Theme.of(context).colorScheme.secondaryContainer),
              recognizer: TapGestureRecognizer()
                ..onTap = enableResend
                    ? () {
                        setState(() {
                          secondsRemaining = 60;
                          enableResend = false;
                        });
                      }
                    : null),
        ],
      ),
    );
  }
}
