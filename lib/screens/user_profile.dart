import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import '../provider/auth_provider.dart';
import '../theme/colors.dart';
import '../widgets/my_info_row.dart';
import 'profile_config.dart';

class UserProfile extends StatelessWidget {
  const UserProfile({Key? key}) : super(key: key);

  Widget build(BuildContext context) {
    final mdh = MediaQuery.of(context).size.height;

    return SafeArea(
      child: Scaffold(
        appBar: null,
        body: Consumer<AuthProvider>(
          builder: (context, authProvider, _) => Column(
            children: [
              Container(
                child: Stack(
                  children: [
                    Positioned(
                      child: Container(
                        width: double.infinity,
                        height: mdh * .2,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                bottomRight: Radius.circular(50),
                                bottomLeft: Radius.circular(50)),
                            color: kTeal50),
                      ),
                    ),
                    Positioned(
                        child: Container(
                      child: Row(
                        children: [
                          IconButton(
                            icon: Icon(Icons.arrow_back),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                          ),
                        ],
                      ),
                    )),
                    Positioned(
                        bottom: 0,
                        left: 0,
                        child: Container(
                          child: Row(
                            children: [
                              IconButton(
                                icon: Icon(Icons.edit_outlined),
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => ProfileConfig(
                                                birthDate: authProvider.user!
                                                            .clientBirthday !=
                                                        null
                                                    ? authProvider
                                                        .user!.clientBirthday!
                                                        .substring(0, 10)
                                                    : "لم يتم التحديد",
                                                fromProfile: true,
                                                name:
                                                    authProvider.user!.clientName,
                                                gender: authProvider
                                                            .user!.clientGender ==
                                                        false
                                                    ? "ذكر"
                                                    : "انثى",
                                              )));
                                },
                              ),
                            ],
                          ),
                        )),
                    Positioned(
                      child: Center(
                        child: Material(
                          color: Theme.of(context).colorScheme.primary,
                          elevation: 20,
                          child: Container(
                            margin: EdgeInsets.only(top: 80),
                            alignment: Alignment.topCenter,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(200),
                            ),
                            width: 150.h,
                            child: Image.asset(
                              "assets/images/go_fast.png",
                              fit: BoxFit.contain,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.all(30),
                width: double.infinity,
                child: Column(
                  children: [
                    MyInfoRow(
                      icon: Icons.person,
                      title: "الاسم :",
                      value: "${authProvider.user!.clientName}",
                    ),
                    MyInfoRow(
                      icon: Icons.mobile_friendly_outlined,
                      title: "رقم الهاتف :",
                      value: "${authProvider.user!.clientMobile}",
                    ),
                    MyInfoRow(
                      icon: Icons.calendar_month,
                      title: "تاريخ الميلاد :",
                      value:
                          "${authProvider.user!.clientBirthday != null ? authProvider.user!.clientBirthday!.substring(0, 10) : "لم يتم التحديد "}",
                    ),
                    MyInfoRow(
                      icon: authProvider.user!.clientGender == false
                          ? Icons.male
                          : Icons.female,
                      title: "الجنس :",
                      value:
                          "${authProvider.user!.clientGender == false ? "ذكر" : "انثى"}",
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
