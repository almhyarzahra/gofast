import 'package:accordion/accordion.dart';
import 'package:flutter/material.dart';
import '../common/tools.dart';
import '../helper/shared_variables.dart';
import '../routes.dart';

class LocationAddByUser extends StatefulWidget {
  const LocationAddByUser({Key? key}) : super(key: key);
  static const routeName = '';
  @override
  _LocationAddByUserState createState() => _LocationAddByUserState();
}

class _LocationAddByUserState extends State<LocationAddByUser> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            'إضافة مواقع',
            style: TextStyle(color: Colors.yellow, fontSize: 20),
            textAlign: TextAlign.left,
          ),
        ),
        body: SafeArea(
            child: Padding(
                padding: const EdgeInsets.all(2.0),
                child: Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    // padding: EdgeInsets.only(top: ),
                    // duration: Duration(seconds: 1),
                    // curve: Curves.fastOutSlowIn,
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                              width: MediaQuery.of(context).size.width * .9,
                              height: MediaQuery.of(context).size.height * .7,
                              color: Colors.white,
                              //colo,
                              child: Accordion(
                                  maxOpenSections: 3,
                                  headerPadding: EdgeInsets.symmetric(
                                      vertical: 7, horizontal: 15),
                                  children: [
                                    AccordionSection(
                                      headerBackgroundColor: Color(0xFFFFC907),
                                      isOpen: true,
                                      leftIcon: Icon(Icons.add_location,
                                          color: Color(0xff671055)),
                                      header: Text(
                                        'إضافة موقع',
                                        style:
                                            TextStyle(color: Color(0xff671055)),
                                        textAlign: TextAlign.right,
                                      ),
                                      content: Container(
                                          height: MediaQuery.of(context)
                                              .size
                                              .height,
                                          width:
                                              MediaQuery.of(context).size.width,
                                          child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.end,
                                              children: [
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  children: <Widget>[
                                                    buildFloatingButton(
                                                        Icons
                                                            .favorite_border_outlined,
                                                        () {
                                                      whatSelected = 'favorite';
                                                      Navigator.pushNamed(
                                                          context,
                                                          Routes.selectlocatin);
                                                      // provmaps.getUserLocation;
                                                    }, "favorite"),
                                                    Text("المفضلة"),
                                                  ],
                                                ),
                                                SizedBox(
                                                    height:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .height *
                                                            .05),
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  children: <Widget>[
                                                    buildFloatingButton(
                                                        Icons.home_outlined,
                                                        () {
                                                      whatSelected = 'home';
                                                      favoriteLocations = [];
                                                      homeLocation = '';
                                                      workLocation = '';
                                                      Navigator.pushNamed(
                                                          context,
                                                          Routes.selectlocatin);
                                                      // provmaps.getUserLocation;
                                                    }, "home"),
                                                    Text("المنزل"),
                                                  ],
                                                ),
                                                SizedBox(
                                                    height:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .height *
                                                            .05),
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  children: <Widget>[
                                                    buildFloatingButton(
                                                        Icons
                                                            .work_outline_rounded,
                                                        () {
                                                      whatSelected = 'work';
                                                      Navigator.pushNamed(
                                                          context,
                                                          Routes.selectlocatin);
                                                      // provmaps.getUserLocation;
                                                    }, "work"),
                                                    Text("العمل"),
                                                  ],
                                                ),
                                                SizedBox(
                                                    height:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .height *
                                                            .05),
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  children: <Widget>[
                                                    buildFloatingButton(
                                                        Icons
                                                            .add_location_outlined,
                                                        () {
                                                      whatSelected = 'location';
                                                      Navigator.pushNamed(
                                                          context,
                                                          Routes.selectlocatin);
                                                      // provmaps.getUserLocation;
                                                    }, "add_location"),
                                                    Text("موقعك الحالي"),
                                                  ],
                                                ),
                                              ])),
                                    )
                                  ]))
                        ])))));
  }
}
