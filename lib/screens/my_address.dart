import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../provider/map_helper_provider.dart';
import '../provider/map_provider.dart';
import '../widgets/vertical_text_icon.dart';

class MyAddressesPage extends StatelessWidget {
  MyAddressesPage(
      {Key? key,
      this.titleIndex = 0,
      required this.isSource,
      required this.isDes})
      : super(key: key);

  final bool isSource;
  final bool isDes;

  final int titleIndex;
  final List<String> titls = [
    "عناويني",
    'اختر نقطة الإنطلاق',
    'اختر نقطة الوصل'
  ];

  final TextEditingController _query = TextEditingController();
  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        leading: Container(),
        actions: [
          IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_forward,
              color: Theme.of(context).colorScheme.secondary,
            ),
          )
        ],
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: Text(
          titls[titleIndex],
          style: TextStyle(
              color: Theme.of(context).colorScheme.primary,
              fontSize: 20,
              fontWeight: FontWeight.w300),
        ),
        centerTitle: true,
      ),
      body: Consumer<MapHelper>(
        builder: (context, mapHlperProvider, _) => SingleChildScrollView(
          child: mapHlperProvider.loadingUntilHandelPointLocation
              ? Center(
                  child: Container(child: CircularProgressIndicator()),
                )
              : Column(
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Card(
                        elevation: 3.0,
                        shape: RoundedRectangleBorder(
                          side: BorderSide(
                            color: Theme.of(context).colorScheme.background,
                          ),
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                        child: Padding(
                          padding: EdgeInsets.all(7.0),
                          child: TextFormField(
                            controller: _query,
                            onChanged: (value) async {
                              value = _query.text;
                              if (value.isNotEmpty) {
                                mapHlperProvider.makeAllSelectabeleValueFalse();
                                await mapHlperProvider.mapSearch(value);
                              } else {
                                mapHlperProvider.clearPredications();
                              }
                            },
                            decoration: InputDecoration(
                              prefixIcon: Icon(
                                Icons.place_outlined,
                                color: Theme.of(context).colorScheme.secondary,
                              ),
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 7.0, horizontal: 20),
                      width: double.infinity,
                      child: IntrinsicHeight(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            VerticalTextIcon(
                                icon: Icons.favorite,
                                title: "المفضلة",
                                type: "fav"),
                            SizedBox(
                              height: 27,
                              child: VerticalDivider(
                                color: Colors.grey,
                                width: 1,
                                thickness: 0.2,
                              ),
                            ),
                            VerticalTextIcon(
                                icon: Icons.house,
                                title: "المنزل",
                                type: "house"),
                            SizedBox(
                              height: 27,
                              child: VerticalDivider(
                                color: Colors.grey,
                                width: 1,
                                thickness: 0.2,
                              ),
                            ),
                            VerticalTextIcon(
                                icon: Icons.store_mall_directory,
                                title: "العمل",
                                type: "work"),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(8.0),
                      width: double.infinity,
                      child: const Text(
                        "نتائج البحث ",
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      decoration: BoxDecoration(
                          color: Theme.of(context).colorScheme.primary),
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 50, right: 10),
                      child: mapHlperProvider.predictions.isEmpty
                          ? Center(
                              // child: CircularProgressIndicator(),
                              )
                          : ListView.separated(
                              physics: const NeverScrollableScrollPhysics(),
                              itemCount: mapHlperProvider.predictions.length,
                              shrinkWrap: true,
                              separatorBuilder: (context, index) => Padding(
                                padding: EdgeInsets.only(right: width * .08),
                                child: Divider(
                                  thickness: 3,
                                  color: Color(0xFFEAEBEC),
                                ),
                              ),
                              itemBuilder: ((context, index) {
                                return InkWell(
                                  onTap: () {
                                    isDes == true || isSource == true
                                        ? AwesomeDialog(
                                            dialogType: DialogType.NO_HEADER,
                                            btnOkOnPress: () async {
                                              if (isSource || isDes) {
                                                await Provider.of<MapProvider>(
                                                        context,
                                                        listen: false)
                                                    .onCameraMove(
                                                  mapHlperProvider
                                                      .predictions[index]
                                                      .location!,
                                                );
                                                Provider.of<MapProvider>(
                                                        context,
                                                        listen: false)
                                                    .camerMoveToDestination(
                                                  mapHlperProvider
                                                      .predictions[index]
                                                      .location!,
                                                );

                                                Navigator.pop(context);
                                              }
                                            },
                                            context: context,
                                            body: Container(
                                              child: Column(
                                                children: [
                                                  Center(
                                                    child: isSource
                                                        ? Text(
                                                            "اختيار هذه النقطة على أنها نقطة الانطلاق؟")
                                                        : Text(
                                                            "اختيار هذه النقطة على أنها نقطة الوصول؟"),
                                                  ),
                                                ],
                                              ),
                                            )).show()
                                        : null;
                                  },
                                  child: Padding(
                                    padding:
                                        EdgeInsets.symmetric(vertical: 7.0),
                                    child: Row(
                                      children: [
                                        Container(
                                          padding: EdgeInsets.all(5),
                                          decoration: BoxDecoration(
                                              color: Color(0xFFEAEBEC),
                                              borderRadius:
                                                  BorderRadius.circular(5)),
                                          child: Icon(
                                            Icons.place,
                                            color: Colors.white,
                                          ),
                                        ),
                                        SizedBox(
                                          width: width * .03,
                                        ),
                                        Text(
                                          "${mapHlperProvider.predictions[index].mainText}",
                                          style: TextStyle(
                                              color: Color(0xFF8F9BB3)),
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              }),
                            ),
                    ),
                  ],
                ),
        ),
      ),
    );
  }
}
