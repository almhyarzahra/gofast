// import 'package:flutter/material.dart';
// import 'package:flutter_rating_bar/flutter_rating_bar.dart';
// import 'package:flutter_screenutil/flutter_screenutil.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:provider/provider.dart';
// import '../provider/map_model.dart';
// import '../routes.dart';

// class RatingTravelScreen extends StatefulWidget {
//   const RatingTravelScreen({Key? key}) : super(key: key);

//   @override
//   _RatingTravelScreenState createState() => _RatingTravelScreenState();
// }

// class _RatingTravelScreenState extends State<RatingTravelScreen> {
//   TextEditingController? RatingController;
//   var numberRating;
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: SafeArea(
//         child: Stack(
//           alignment: Alignment.center,
//           children: [
//             Positioned(
//               top: 0,
//               child: Container(
//                 height: MediaQuery.of(context).size.height,
//                 width: MediaQuery.of(context).size.width,
//                 child: GoogleMap(
//                   zoomControlsEnabled: false,
//                   mapType: MapType.normal,
//                   markers: provmaps.markers,
//                   polylines: provmaps.polylines,
//                   onCameraMove: provmaps.onCameraMove,
//                   initialCameraPosition:
//                       CameraPosition(target: provmaps.initialPos, zoom: 14.0),
//                   onMapCreated: provmaps.onCreated,
//                   onCameraIdle: () async {
//                     // provmaps.getMoveCamera();
//                   },
//                 ),
//               ),
//             ),
//             Positioned(
//               bottom: 0,
//               right: 0,
//               left: 0,
//               child: ClipRRect(
//                 borderRadius: BorderRadius.only(
//                     topLeft: Radius.circular(25),
//                     topRight: Radius.circular(25)),
//                 child: AnimatedContainer(
//                   duration: Duration(seconds: 1),
//                   curve: Curves.fastOutSlowIn,
//                   color: Colors.white,
//                   child: Column(
//                     children: [
//                       SizedBox(
//                         height: 10.h,
//                       ),
//                       Text("قيم رحلتك"),
//                       SizedBox(
//                         height: 10.h,
//                       ),
//                       numberRating != null
//                           ? Text("لقد hhh قيمت حتى ${numberRating} نجوم")
//                           : Text("لقيد قيمت حتى 0 نجوم"),
//                       SizedBox(
//                         height: 10.h,
//                       ),
//                       RatingBar.builder(
//                           initialRating: 3,
//                           allowHalfRating: true,
//                           itemCount: 5,
//                           direction: Axis.horizontal,
//                           itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
//                           itemBuilder: (context, _) => Icon(
//                                 Icons.star,
//                                 color: Colors.amber,
//                               ),
//                           onRatingUpdate: (rating) {
//                             setState(() {
//                               numberRating = rating;
//                               print(rating);
//                               print("hello");
//                               print(numberRating);
//                             });
//                           }),
//                       SizedBox(
//                         height: 10.h,
//                       ),
//                       ClipRRect(
//                         borderRadius: BorderRadius.circular(10),
//                         child: Container(
//                           margin: EdgeInsets.symmetric(horizontal: 20),
//                           color: Colors.black12,
//                           child: TextFormField(
//                             minLines: 3,
//                             maxLines: null,
//                             keyboardType: TextInputType.multiline,
//                             controller: RatingController,
//                             decoration: InputDecoration(
//                               contentPadding: EdgeInsets.all(10),
//                               border: InputBorder.none,
//                               hintText: "اصف رحلتك مع الكابتن ",
//                             ),
//                           ),
//                         ),
//                       ),
//                       Container(
//                         margin:
//                             EdgeInsets.symmetric(horizontal: 10, vertical: 10),
//                         height: MediaQuery.of(context).size.width * .13,
//                         width: MediaQuery.of(context).size.width * .50,
//                         child: ElevatedButton(
//                           child: Text(
//                             'تأكيد',
//                             style: TextStyle(fontSize: 20.sp),
//                           ),
//                           style: ElevatedButton.styleFrom(
//                             foregroundColor: Colors.white,
//                             backgroundColor: Colors.indigo[900],
//                             shape: const RoundedRectangleBorder(
//                                 borderRadius:
//                                     BorderRadius.all(Radius.circular(5))),
//                           ),
//                           onPressed: () {
//                             setState(() {
//                               Navigator.pushReplacementNamed(
//                                   context, Routes.previoustrips);
//                             });
//                           },
//                         ),
//                       )
//                     ],
//                   ),
//                 ),
//               ),
//             )
//           ],
//         ),
//       ),
//     );
//   }
// }
