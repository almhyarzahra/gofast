import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../provider/driver_provider.dart';

class DriverInfoWidget extends StatelessWidget {
  const DriverInfoWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<DriverProvider>(
      builder: (context, driverProvider, _) => Container(
        child: Container(
          width: double.infinity,
          child: Column(
            children: [
              Row(
                children: [
                  Text("تم العثور على السائق :"),
                  Text(
                    driverProvider.chosenDriver!.driverName,
                    style: TextStyle(
                      color: Theme.of(context).colorScheme.secondary,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 15),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Row(
                    children: [
                      Text("الجنس : "),
                      Text(
                        '${driverProvider.chosenDriver!.driverGender}',
                        style: TextStyle(
                          color: Theme.of(context).colorScheme.secondary,
                          fontWeight: FontWeight.bold,
                        ),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      Text("الشهادة : "),
                      Text(
                        '${driverProvider.chosenDriver!.certificate}',
                        style: TextStyle(
                          color: Theme.of(context).colorScheme.secondary,
                          fontWeight: FontWeight.bold,
                        ),
                      )
                    ],
                  ),
                ],
              ),
              SizedBox(height: 15),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("رقم الهاتف : "),
                  Text(
                    '${driverProvider.chosenDriver!.mobile}',
                    style: TextStyle(
                      color: Theme.of(context).colorScheme.secondary,
                      fontWeight: FontWeight.bold,
                    ),
                  )
                ],
              ),
              SizedBox(height: 15),
              Row(
                children: [
                  Text(" السيارة : "),
                  Text(
                    '${driverProvider.chosenDriver!.car!.manufacture}  ${driverProvider.chosenDriver!.car!.carModel}',
                    style: TextStyle(
                      color: Theme.of(context).colorScheme.secondary,
                      fontWeight: FontWeight.bold,
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
