import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import '../provider/auth_provider.dart';
import '../provider/driver_provider.dart';
import '../provider/map_helper_provider.dart';
import '../provider/map_provider.dart';
import '../provider/user_provider.dart';
import '../routes.dart';
import '../screens/finished_orders_screen.dart';
import '../screens/login_screen.dart';
import '../screens/my_address.dart';
import '../screens/my_payments.dart';
import '../theme/colors.dart';

class AppDrawer extends StatelessWidget {
  const AppDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<AuthProvider>(
      builder: (context, authProvider, _) => SafeArea(
        top: true,
        child: Theme(
          data: Theme.of(context).copyWith(
            canvasColor: Color.fromRGBO(102, 44, 144, 1),
          ),
          child: Drawer(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(
                height: 30.h,
              ),
              Row(
                children: [
                  CircleAvatar(
                    radius: 30,
                    backgroundColor: kTeal100,
                    child: Icon(
                      Icons.person,
                      color: Colors.grey,
                      size: 40.sp,
                    ),
                  ),
                  SizedBox(
                    width: 18.w,
                  ),
                  Text(
                    authProvider.user == null
                        ? "new"
                        : authProvider.user!.clientName,
                    style: TextStyle(
                        color: kTeal100,
                        fontWeight: FontWeight.bold,
                        fontSize: 20.sp),
                  )
                ],
              ),
              SizedBox(
                height: 30.h,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                child: Divider(
                  color: kGrey400,
                  height: 1,
                ),
              ),
              Container(
                child: Column(
                  children: [
                    ListTile(
                      onTap: () {
                        Navigator.pop(context);
                        Navigator.pushNamed(context, Routes.userProfile);
                      },
                      leading: Icon(
                        Icons.person,
                       // color: Colors.white,
                      ),
                      title: Text(
                        "الملف الشخصي",
                        style: TextStyle(
                            color: kTeal100,
                            fontWeight: FontWeight.normal,
                            fontSize: 16.sp),
                      ),
                    ),
                    Consumer<UserModel>(
                      builder: (context, userProvider, _) => ListTile(
                        onTap: () async {
                          // Navigator.pop(context);

                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => MyAddressesPage(
                                isSource: false,
                                isDes: false,
                              ),
                            ),
                          );
                          userProvider.getAllFavouriteAddress().then((value) async {
                            await Provider.of<MapHelper>(context, listen: false)
                                .getPointAddressName(value);
                          });
                        },
                        leading: Icon(
                          Icons.location_on,
                          //color: Colors.white,
                        ),
                        title: Text(
                          " عناويني",
                          style: TextStyle(
                            color: kTeal100,
                            fontWeight: FontWeight.normal,
                            fontSize: 16.sp,
                          ),
                        ),
                      ),
                    ),
                    ListTile(
                      onTap: () async{
                        Navigator.pop(context);
                        Navigator.push(context, MaterialPageRoute(builder: (context)=> FinishedOrdersScreen()));
                        await Provider.of<DriverProvider>(context,listen: false).getFinishOrders(
                            Provider.of<AuthProvider>(context,listen: false).user!.clientId.toString()
                        );
                      },
                      leading: Icon(
                        Icons.book_online,
                        //color: Colors.white,
                      ),
                      title: Text(
                        "سجل الرحلات",
                        style: TextStyle(
                            color: kTeal100,
                            fontWeight: FontWeight.normal,
                            fontSize: 16.sp),
                      ),
                    ),
                    ListTile(
                      onTap: () async{
                        Navigator.pop(context);
                        Navigator.push(context, MaterialPageRoute(builder: (context)=> PaymentScreen()));
                        await Provider.of<DriverProvider>(context,listen: false).getPayemtns(
                            Provider.of<AuthProvider>(context,listen: false).user!.clientId.toString()
                        );
                      },
                      leading: Image.asset(
                        "assets/images/wallet.png",
                        width: 25.w,
                        height: 25.h,
                        color: Colors.black,
                      ),
                      title: Text(
                        "محفظتي",
                        style: TextStyle(
                            color: kTeal100,
                            fontWeight: FontWeight.normal,
                            fontSize: 16.sp),
                      ),
                    ),

                    // ListTile(
                    //   leading: Icon(
                    //     Icons.settings,
                    //     color: Colors.white,
                    //   ),
                    //   title: Text(
                    //     "الاعدادات",
                    //     style: TextStyle(
                    //         color: Colors.white,
                    //         fontWeight: FontWeight.normal,
                    //         fontSize: 16.sp),
                    //   ),
                    // ),
                    // ListTile(
                    //   leading: Icon(
                    //     Icons.star,
                    //     color: Colors.white,
                    //   ),
                    //   title: Text(
                    //     "تقييم التطبيق",
                    //     style: TextStyle(
                    //         color: Colors.white,
                    //         fontWeight: FontWeight.normal,
                    //         fontSize: 16.sp),
                    //   ),
                    // ),
                  ],
                ),
              ),
              SizedBox(
                height: 70.h,
              ),
              Consumer<MapProvider>(
                builder: (context, mapProvider, _) => Container(
                  child: ListTile(
                    leading: Icon(
                      Icons.logout,
                     // color: Colors.white,
                    ),
                    title: Text(
                      "تسجيل الخروج",
                      style: TextStyle(
                          color: kTeal100,
                          fontWeight: FontWeight.normal,
                          fontSize: 16.sp),
                    ),
                    onTap: () async {
                      authProvider.logOut().then((value) async {
                        mapProvider.initValuesState();
                        Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(
                                builder: (context) => LoginScreen()),
                            (route) => false);
                      });
                    },
                  ),
                ),
              ),
            ],
          )),
        ),
      ),
    );
  }
}
