import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../helper/shared_variables.dart';
import '../provider/auth_provider.dart';
import '../provider/map_provider.dart';
import '../provider/user_provider.dart';
import '../screens/home_page.dart';
import 'tad_snackbar_error.dart';

class AddLocationPhase extends StatelessWidget {
  const AddLocationPhase({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<UserModel>(
      builder: (context, userProvider, _) => Positioned(
        left: 0,
        right: 0,
        bottom: 0,
        child: Container(
          padding: EdgeInsets.all(30),
          width: double.infinity,
          color: Theme.of(context).colorScheme.background,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                userProvider.getAddressLoading
                    ? CircularProgressIndicator()
                    : ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(25.0),
                          ),
                        ),
                        child: Icon(Icons.arrow_circle_left_rounded),
                        onPressed: () async {
                          if (addLocationtype == 1) {
                            await userProvider
                                .saveHomeLocation(
                                    Provider.of<AuthProvider>(context,
                                            listen: false)
                                        .user!
                                        .clientId,
                                    Provider.of<MapProvider>(context,
                                            listen: false)
                                        .addLocationMarker!
                                        .position)
                                .then((value) async {
                              if (value) {
                                ScaffoldMessenger.of(context).showSnackBar(
                                  MySnackBar(
                                    text: "تم بنجاح حفظ الموقع الجديد",
                                    type: MySnackBarType.success,
                                    context: context,
                                  ),
                                );
                                // await userProvider.getHome();
                              } else {
                                ScaffoldMessenger.of(context).showSnackBar(
                                  MySnackBar(
                                    text: "حدث خطأ اثناء حفظ الموقع ",
                                    type: MySnackBarType.error,
                                    context: context,
                                  ),
                                );
                              }
                            });
                          } else if (addLocationtype == 2) {
                            await userProvider
                                .saveWorkLocation(
                                    Provider.of<AuthProvider>(context,
                                            listen: false)
                                        .user!
                                        .clientId,
                                    Provider.of<MapProvider>(context,
                                            listen: false)
                                        .addLocationMarker!
                                        .position)
                                .then((value) async {
                              if (value) {
                                ScaffoldMessenger.of(context).showSnackBar(
                                  MySnackBar(
                                    text: "تم بنجاح حفظ الموقع الجديد",
                                    type: MySnackBarType.success,
                                    context: context,
                                  ),
                                );
                                await userProvider.getWorkLocation();
                              } else {
                                ScaffoldMessenger.of(context).showSnackBar(
                                  MySnackBar(
                                    text: "حدث خطأ اثناء حفظ الموقع ",
                                    type: MySnackBarType.error,
                                    context: context,
                                  ),
                                );
                              }
                            });
                          }
                        },
                      ),
                TextButton(
                  onPressed: () {
                    Provider.of<MapProvider>(context, listen: false)
                        .changeIndex(0);
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(builder: (context) => HomePage()),
                        (route) => false);
                  },
                  child: Text("العودة للصفحة الرئيسة"),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
