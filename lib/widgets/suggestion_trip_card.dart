import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../models/order_type_model.dart';
import '../provider/driver_provider.dart';
import '../theme/colors.dart';

// ignore: must_be_immutable
class SuggestionTripCard extends StatelessWidget {
  SuggestionTripCard({Key? key, required this.orderTypeModel})
      : super(key: key);
  bool isAvtive = false;
  OrderTypeModel orderTypeModel;
  @override
  Widget build(BuildContext context) {
    String assetName = orderTypeModel.image!;
    Widget svg = Image.asset(
      assetName,
    );
    return Consumer<DriverProvider>(
      builder: (context, value, _) => Card(
        shape: RoundedRectangleBorder(
          //<-- SEE HERE

          borderRadius: BorderRadius.circular(20.0),
        ),
        color: isAvtive
            ? Theme.of(context).colorScheme.secondary
            : Color(0xffD6D6D6),
        child: Padding(
          padding: const EdgeInsets.all(9.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              // Icon(Icons.motorcycle_outlined),
              Expanded(child: svg),
              SizedBox(
                height: 10,
              ),
              Text("${orderTypeModel.typeNmae}"),
              SizedBox(
                height: 10,
              ),
              //Text("${orderTypeModel.description}"),
              SizedBox(
                height: 10,
              ),


              Container(

                child: Row(
                  children: [
                    isAvtive && value.activatedCobon ==true
                        ? FittedBox(
                      child: Text(
                        "${orderTypeModel.prices} ",
                        style: TextStyle(
                          decoration:
                          TextDecoration.lineThrough,
                          decorationColor: Colors.red,
                          decorationThickness: 2,
                          fontSize: 17,
                          // fontWeight: FontWeight.w800,
                        ),
                        overflow: TextOverflow.ellipsis,
                      ),
                    )
                        : Container(),
                   SizedBox(
                      width: 5,
                    ),
                    // value.cobon!.cobonValue
                    FittedBox(
                        child: Text("${(isAvtive && value.activatedCobon == true )? (orderTypeModel.prices - value.cobon!.cobonValue) : orderTypeModel.prices}  ل.س ",overflow: TextOverflow.ellipsis, )),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              FittedBox(child: Text(" معدل وصول ${orderTypeModel.arriving} دقائق ", style: TextStyle(fontSize: 12 ),)),
              // Text("${orderTypeModel.description}"),
            ],
          ),
        ),
      ),
    );
  }
}
