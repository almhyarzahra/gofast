import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

import '../provider/driver_provider.dart';
import '../provider/map_provider.dart';
import '../screens/home_page.dart';

class RatingTravel extends StatefulWidget {
  const RatingTravel({Key? key}) : super(key: key);

  @override
  _RatingTravelState createState() => _RatingTravelState();
}

class _RatingTravelState extends State<RatingTravel> {
  TextEditingController RatingController = TextEditingController();
  var numberRating;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 10,
        ),
        Text("قيم رحلتك"),
        SizedBox(
          height: 14.h,
        ),
        numberRating != null
            ? Text("لقد قيمت حتى ${numberRating} نجوم")
            : Text("لقد قيمت حتى 0 نجوم"),
        SizedBox(
          height: 40.1.h,
        ),
        RatingBar.builder(
            unratedColor: Color(0xffE2E2E2),
            initialRating: 0,
            allowHalfRating: true,
            itemCount: 5,
            direction: Axis.horizontal,
            itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
            itemBuilder: (context, _) => Icon(
                  Icons.star,
                  color: Theme.of(context).colorScheme.secondary,
                  size: 38.31.sp,
                ),
            onRatingUpdate: (rating) {
              setState(() {
                numberRating = rating;
                print(numberRating);
                print(rating);
              });
            }),
        SizedBox(
          height: 43.6.h,
        ),
        Container(
          height: 105.88.h,
          decoration: BoxDecoration(
            color: Colors.black12,
            borderRadius: BorderRadius.vertical(
              top: Radius.circular(5),
            ),
          ),
          child: TextFormField(
            onChanged: (value) {
              Provider.of<DriverProvider>(context, listen: false).clientReview =
                  value;
            },
            minLines: 3,
            maxLines: null,
            keyboardType: TextInputType.multiline,
            controller: RatingController,
            decoration: InputDecoration(
              fillColor: Color(0xffE2E2E2),
              contentPadding: EdgeInsets.all(10),
              border: InputBorder.none,
              hintText: "اصف رحلتك مع الكابتن ",
            ),
          ),
        ),
        SizedBox(
          height: 24.9.h,
        ),
        Consumer<MapProvider>(
          builder: (context, mapProvider, _) => Consumer<DriverProvider>(
            builder: (context, driverProvider, _) => ElevatedButton(
              style: ButtonStyle(
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                )),
                elevation: MaterialStateProperty.all(7),
                padding: MaterialStateProperty.all(
                  EdgeInsets.symmetric(
                      horizontal: MediaQuery.of(context).size.width * .3,
                      vertical: 15),
                ),
              ),
              child: Text(
                "تأكيد ",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              onPressed: () async {
                await driverProvider.FinshOrder();
                driverProvider.desDetails.clear();
                driverProvider.sourceDetails.clear();
                mapProvider.startMarker = null;
                Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const HomePage(),
                  ),
                  (route) => false,
                );
                await mapProvider.initState();
              },
            ),
          ),
        ),
        SizedBox(
          height: 20,
        )
      ],
    );
  }
}
