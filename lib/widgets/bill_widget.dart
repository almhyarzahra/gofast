import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../provider/driver_provider.dart';
import '../provider/map_provider.dart';
import '../screens/home_page.dart';

class BillWidget extends StatelessWidget {
  const BillWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<MapProvider>(
        builder: (context, mapProvider, _) => Consumer<DriverProvider>(
      builder: (context, driverProvider, child) {
        if (driverProvider.billLoading == false) {
          return Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              decoration: BoxDecoration(
                border:
                    Border.all(color: Theme.of(context).colorScheme.primary),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black12,
                    offset: const Offset(
                      0.0,
                      0.0,
                    ),
                    blurRadius: 7.0,
                    spreadRadius: 2.0,
                  ), //BoxShadow
                  BoxShadow(
                    color: Colors.black12,
                    offset: const Offset(0.0, 0.0),
                    blurRadius: 0.0,
                    spreadRadius: 0.0,
                  ),
                ],
                borderRadius: BorderRadius.all(Radius.circular(20)),
                color: Theme.of(context).colorScheme.background,
              ),
              width: double.infinity,

              child: Column(

                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("المبلغ المدفوع",style: TextStyle(fontWeight: FontWeight.w700,),),
                      SizedBox(width: 5,),
                      Text("${driverProvider.billModel!.totalPric ?? 0.0}",style: TextStyle(fontWeight: FontWeight.w700),),

                    ],
                  ),
                  SizedBox(height: MediaQuery
                      .of(context)
                      .size
                      .height * 0.03,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("قيمة العداد الإبتدائية",style: TextStyle(fontSize: 20),),
                      Text("${driverProvider.billModel!.counterOpenPrice}",style: TextStyle(fontSize: 20)),

                    ],
                  ),
                  // Divider(endIndent: 50,indent: 50,),
                  SizedBox(height: MediaQuery
                      .of(context)
                      .size
                      .height * 0.02,),
                  Row(

                      mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                    Text("قيمة المسافة المقطوعة",style: TextStyle(fontSize: 20)),
                    Text("${driverProvider.billModel!.totalDistancePrice}",style: TextStyle(fontSize: 20)),

                  ]),
                  // Divider(endIndent: 50,indent: 50,),
                  SizedBox(height: MediaQuery
                      .of(context)
                      .size
                      .height * 0.02,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("قيمة مدة الرحلة",style: TextStyle(fontSize: 20)),
                      Text("${driverProvider.billModel!.hoursPrice}",style: TextStyle(fontSize: 20)),
                    ],
                  ),
                  // Divider(endIndent: 50,indent: 50,),
                  SizedBox(height: MediaQuery
                      .of(context)
                      .size
                      .height * 0.02,),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("غرامة دقائق انتظار السائق",style: TextStyle(fontSize: 20)),
                      Text("${driverProvider.billModel!.waittingPrice}",style: TextStyle(fontSize: 20)),
                      // Text("100"),
                    ],
                  ),
                  // Divider(endIndent: 50,indent: 50,),
                  SizedBox(height: MediaQuery
                      .of(context)
                      .size
                      .height * 0.02,),

                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                    Text("المجموع النهائي",style: TextStyle(fontSize: 20)),
                    Text("${driverProvider.billModel!.totalPric}",style: TextStyle(fontSize: 20)),

                  ]),

                  SizedBox(height: MediaQuery
                      .of(context)
                      .size
                      .height * 0.03,),

                  ElevatedButton(
                    onPressed: () async {
                      final sh = await SharedPreferences.getInstance();
                      sh.remove('order');
                      sh.remove('driverMarker');
                      mapProvider.changeIndex(7);
                      // await                     Provider.of<DriverProvider>(context, listen: false).getBill(sh.get('order'));
                      if (Provider.of<DriverProvider>(context, listen: false)
                          .timerForTracer !=
                          null) {
                        Provider.of<DriverProvider>(context, listen: false)
                            .timerForTracer!
                            .cancel();
                      }
                      if (mapProvider.timerForTracer != null) {
                        mapProvider.timerForTracer!.cancel();
                      }
                      if (Provider.of<DriverProvider>(context, listen: false)
                          .trackingDriverTimer !=
                          null) {
                        Provider.of<DriverProvider>(context, listen: false)
                            .trackingDriverTimer!
                            .cancel();
                      }

                      // Navigator.pushAndRemoveUntil(
                      //   context,
                      //   MaterialPageRoute(
                      //       builder: (context) => const HomePage()),
                      //   (route) => false,
                      // );
                      // await Provider.of<MapProvider>(context, listen: false)
                      //     .initState();
                    },
                    child: Text("موافق "),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.03,
                  ),
                  TextButton(
                    onPressed: () {},
                    child: Text("الإبلاغ عن مشكلة "),
                  ),
                ],
              ),
            ),
          );
        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    ));
  }
}
