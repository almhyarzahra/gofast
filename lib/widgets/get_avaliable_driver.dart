import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../provider/auth_provider.dart';
import '../provider/driver_provider.dart';
import '../provider/map_provider.dart';
import 'driver_card.dart';

class GetAvailableDriver extends StatelessWidget {
  const GetAvailableDriver({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<MapProvider>(
      builder: (context, mapProvider, _) => Consumer<DriverProvider>(
        builder: (context, driverProvider, _) => Center(
          child: Column(
            children: [
              Row(
                children: [
                  // TextButton(
                  //   onPressed: () async {
                  //     driverProvider.desDetails.clear();
                  //     driverProvider.sourceDetails.clear();
                  //     mapProvider.startMarker = null;
                  //     Navigator.pushReplacement(
                  //       context,
                  //       MaterialPageRoute(
                  //         builder: (context) => const HomePage(),
                  //       ),
                  //     );
                  //     await mapProvider.initState();
                  //   },
                  //   child: Text(
                  //     "الغاء الطلب",
                  //     style: TextStyle(fontWeight: FontWeight.bold),
                  //   ),
                  // )
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: Text("اختر السائق المناسب "),
                  )
                ],
              ),
              CarouselSlider(
                options: CarouselOptions(
                  viewportFraction: 0.6,
                  initialPage: 0,
                  aspectRatio: 10 / 6.5,
                  enableInfiniteScroll: false,
                  onPageChanged: (index, reason) async {
                    driverProvider.choreseDriver(index);
                  },
                ),
                items: driverProvider.allDrivers.map((i) {
                  return Builder(
                    builder: (context) {
                      return DriverCard(
                          driver: i,
                          driverArrivingTime: driverProvider
                              .driverArrivingTime[i.driverId]!
                              .split(' ')
                              .first,
                          driverRating:
                              driverProvider.driverRating[i.driverId]!);
                    },
                  );
                }).toList(),
              ),
              driverProvider.getNearestDriver
                  ? CircularProgressIndicator()
                  : Column(
                      children: [
                        SizedBox(
                          height: 10,
                        ),
                        driverProvider.sendRequestLoading == true
                            ? Center(
                                child: CircularProgressIndicator(),
                              )
                            : Consumer<AuthProvider>(
                                builder: (context, authProvider, _) =>
                                    ElevatedButton(
                                  style: ButtonStyle(
                                    shape: MaterialStateProperty.all<
                                            RoundedRectangleBorder>(
                                        RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10.0),
                                    )),
                                    elevation: MaterialStateProperty.all(7),
                                    padding: MaterialStateProperty.all(
                                      EdgeInsets.symmetric(
                                          horizontal: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              .2,
                                          vertical: 15),
                                    ),
                                  ),
                                  child: Text(
                                    " تأكيد السائق",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  onPressed: () async {
                                    print(
                                        "user id is : ${authProvider.user!.clientId}");
                                    await driverProvider.sendRequest(
                                      clientId: authProvider.user!.clientId,
                                      driverId: driverProvider.driverId!,
                                      typeId:
                                          driverProvider.chosenOrderType!.id,
                                      destinationLocation: mapProvider.source!,
                                      sourceLocation: mapProvider.destination!,
                                      sourceDetails: mapProvider.sourceDetails!,
                                      destinationDetails:
                                          mapProvider.desDetails!,
                                    );
                                  },
                                ),
                              ),
                      ],
                    ),
              SizedBox(
                height: 10,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
