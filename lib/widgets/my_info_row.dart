import 'package:flutter/material.dart';

class MyInfoRow extends StatelessWidget {
  const MyInfoRow(
      {Key? key,
      required this.title,
      required this.value,
      required this.icon,
      this.titleColor,
      this.elevation,
      this.padding})
      : super(key: key);

  final String title;
  final String value;
  final IconData icon;
  final Color? titleColor;
  final double? elevation;
  final double? padding;
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: elevation ?? 7,
      child: Padding(
        padding: EdgeInsets.all(padding ?? 17),
        child: Row(
          children: [
            Icon(
              icon,
              color: titleColor ?? Theme.of(context).colorScheme.primary,
            ),
            SizedBox(
              width: 5,
            ),
            Text(
              title,
              style: TextStyle(
                color: titleColor ?? Theme.of(context).colorScheme.primary,
                fontSize: 20,
              ),
            ),
            SizedBox(
              width: 15,
            ),
            Text(
              value,
              style: TextStyle(fontWeight: FontWeight.bold),
            )
          ],
        ),
      ),
    );
  }
}
