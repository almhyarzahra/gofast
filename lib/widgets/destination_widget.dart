import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

import '../provider/map_helper_provider.dart';
import '../provider/map_provider.dart';
import '../provider/user_provider.dart';
import '../screens/my_address.dart';
import '../theme/colors.dart';
import 'location_check.dart';

class DestinationWidget extends StatelessWidget {
  const DestinationWidget({
    Key? key,
    required this.controller,
    required this.hintText,
    required this.onPress,
    required this.sour,
    required this.des,
  }) : super(key: key);
  final String hintText;
  final Function()? onPress;
  final TextEditingController controller;
  final des;
  final sour;

  @override
  Widget build(BuildContext context) {
    return Positioned(
      left: 0,
      right: 0,
      bottom: 0,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 20),
        width: double.infinity,
        height: 350.h,
        decoration: BoxDecoration(
          border: Border.all(color: kTeal100),
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25), topRight: Radius.circular(25))),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Stack(
            children: [
              Container(
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          "نقطة الوصول",
                          style: TextStyle(
                            color: Theme.of(context).colorScheme.primary,
                            fontSize: 16,
                          ),
                        ),
                        InkWell(
                            onTap: () {
                              Provider.of<MapProvider>(context, listen: false)
                                  .choseSour = true;
                              Provider.of<MapProvider>(context, listen: false)
                                  .choseDes = false;
                              Provider.of<MapProvider>(context, listen: false)
                                  .myMarker!
                                  .removeWhere(
                                      (element) => element.markerId == "2");
                              Provider.of<MapProvider>(context, listen: false)
                                  .changeIndex(0);
                            },
                            child: Icon(
                              Icons.arrow_forward,
                              color: Theme.of(context).colorScheme.secondary,
                            ))
                      ],
                    ),
                    ClipPath(
                      // clipper: ShapeBorderClipper(
                      //     shape: RoundedRectangleBorder(
                      //         borderRadius: BorderRadius.all(
                      //   Radius.elliptical(
                      //     50,
                      //     100,
                      //   ),
                      // ))),
                      child: Container(
                        color: Colors.white,
                        child: Consumer<MapProvider>(
                          builder: (context, mapProvider, _) => Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              TextFormField(
                                  enabled: false,
                                  controller:
                                      mapProvider.markerDetailsController,
                                  decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.symmetric(horizontal: 30),
                                    fillColor: Color(0xFFE0E0E0),
                                    filled: true,
                                  )),
                              // Container(
                              //   color: mapProvider.filedColor,
                              //   child: controller.text.isNotEmpty
                              //       ? Divider(
                              //           color: Theme.of(context)
                              //               .colorScheme
                              //               .secondary,
                              //           thickness: 1,
                              //         )
                              //       : Container(),
                              // ),
                              const SizedBox(height: 20),
                              Text(" تفاصيل الوجهة ", style: TextStyle(color: Colors.black,fontSize: 16,fontWeight: FontWeight.bold),textAlign: TextAlign.start,),
                              Stack(
                                children: [
                                  TextFormField(
                                      enabled: false,
                                      controller: controller,
                                      decoration: InputDecoration(
                                        hintText: "اين تريد ان تذهب ؟",
                                        hintStyle:
                                            TextStyle(color: Colors.black),
                                        contentPadding: EdgeInsets.symmetric(
                                            horizontal: 30),
                                        fillColor: controller.text.isNotEmpty
                                            ? Colors.white
                                            : Theme.of(context)
                                                .colorScheme
                                                .secondary,
                                        filled: true,
                                      )),
                                  Align(
                                      alignment: Alignment.centerLeft,
                                      child: Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 10.0, vertical: 2.0),
                                        child: IconButton(
                                            onPressed: () async {
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                  builder: (context) =>
                                                      MyAddressesPage(
                                                    isDes: true,
                                                    isSource: false,
                                                    titleIndex: 2,
                                                  ),
                                                ),
                                              );
                                              Provider.of<UserModel>(context,
                                                      listen: false)
                                                  .getAllFavouriteAddress()
                                                  .then((value) async {
                                                await Provider.of<MapHelper>(
                                                        context,
                                                        listen: false)
                                                    .getPointAddressName(value);
                                              });
                                            },
                                            icon: Icon(
                                              Icons.search,
                                              size: 30,
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                            )),
                                      )),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    ElevatedButton(
                      style: ButtonStyle(
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(25.0),
                        )),
                        elevation: MaterialStateProperty.all(7),
                        padding: MaterialStateProperty.all(
                          EdgeInsets.symmetric(horizontal: 30, vertical: 15),
                        ),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "متابعة الطلب",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Icon(Icons.arrow_forward),
                        ],
                      ),
                      onPressed: onPress,
                    ),
                    // Consumer<MapProvider>(
                    //   builder: (context, mapProvider, _) => Container(
                    //     width: double.infinity,
                    //     child: Row(
                    //       children: [
                    //         Provider.of<UserModel>(context, listen: false)
                    //                     .homeLocation !=
                    //                 null
                    //             ? LocationCheck(
                    //                 type: 1,
                    //                 title: "المنزل",
                    //                 vlaue: mapProvider.checkHome,
                    //                 des: des,
                    //                 source: sour,
                    //               )
                    //             : Container(),
                    //         Provider.of<UserModel>(context, listen: false)
                    //                     .workLocation !=
                    //                 null
                    //             ? LocationCheck(
                    //                 type: 2,
                    //                 title: "العمل",
                    //                 vlaue: mapProvider.checkWork,
                    //                 des: des,
                    //                 source: sour,
                    //               )
                    //             : Container()
                    //       ],
                    //     ),
                    //   ),
                    // ),
                  ],
                ),
              ),
              // Positioned(
              //   top: controller.text.isEmpty ? 36 : 43,
              //   right: 10,
              //   child: Image.asset("assets/images/from_to.png"),
              // )
            ],
          ),
        ),
      ),
    );
  }
}
