import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../theme/colors.dart';

class VerfiyTextFormFaild extends StatelessWidget {
  const VerfiyTextFormFaild({Key? key, required this.controller})
      : super(key: key);
  final TextEditingController controller;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      inputFormatters: [
        LengthLimitingTextInputFormatter(1),
        FilteringTextInputFormatter.digitsOnly
      ],
      keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next,
      controller: controller,
      validator: (value) {
        value = controller.text;
        if (value.isEmpty) {
          return "*";
        } else {
          return null;
        }
      },
      onChanged: (value) {
        if (value.length == 1) FocusScope.of(context).nextFocus();
        if (value.isEmpty) FocusScope.of(context).previousFocus();
      },
      decoration: InputDecoration(
        border: OutlineInputBorder(
            borderSide: BorderSide(
              color: kTeal50,
            ),
            borderRadius: BorderRadius.all(Radius.circular(16))),
        focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: kTeal50,
            ),
            borderRadius: BorderRadius.all(Radius.circular(16))),
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: kTeal50,
            ),
            borderRadius: BorderRadius.all(Radius.circular(16))),
      ),
    );
  }
}
