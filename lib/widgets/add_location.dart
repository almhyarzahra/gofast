import 'package:accordion/accordion.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AddLocation extends StatefulWidget {
  const AddLocation({Key? key}) : super(key: key);

  @override
  _AddLocationState createState() => _AddLocationState();
}

class _AddLocationState extends State<AddLocation> {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height * .3,
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.only(top: 3.7.h),
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * .3,
                  color: Colors.white,
                  //colo,
                  child: Accordion(
                      maxOpenSections: 3,
                      headerPadding:
                          EdgeInsets.symmetric(vertical: 7, horizontal: 15),
                      children: [
                        AccordionSection(
                          headerBackgroundColor: Color(1111111111),
                          isOpen: false,
                          leftIcon:
                              Icon(Icons.add_location, color: Colors.white),
                          header: Text(
                            'إضافة موقع',
                            textAlign: TextAlign.right,
                          ),
                          content: Container(
                              height: MediaQuery.of(context).size.height,
                              width: MediaQuery.of(context).size.width,
                              child: SingleChildScrollView(
                                physics: AlwaysScrollableScrollPhysics(),
                                child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: [
                                          Text("المفضلة"),
                                          IconButton(
                                            onPressed: null,
                                            icon: Icon(
                                                Icons.favorite_border_outlined),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: [
                                          Text("المنزل"),
                                          IconButton(
                                            onPressed: null,
                                            icon: Icon(Icons.home_outlined),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: [
                                          Text("العمل"),
                                          IconButton(
                                            onPressed: null,
                                            icon: Icon(
                                                Icons.work_outline_rounded),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: [
                                          Text("موقعك الحالي "),
                                          IconButton(
                                            onPressed: null,
                                            icon: Icon(
                                                Icons.add_location_outlined),
                                          ),
                                        ],
                                      ),
                                    ]),
                              )),
                        )
                      ]))
            ]));
  }
}
