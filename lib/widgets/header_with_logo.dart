import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class HeaderWithLogo extends StatelessWidget {
  const HeaderWithLogo({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: const Color(0xFFF0F4F7),
      child: Stack(
        alignment: Alignment.center,
        children: [
          Image.asset(
            'assets/images/map.png',
            height: 563.h,
            fit: BoxFit.fitHeight,
          ),
          Image.asset('assets/images/splash.png'),
        ],
      ),
    );
  }
}
