import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:provider/provider.dart';
import '../provider/auth_provider.dart';
import '../provider/map_helper_provider.dart';
import '../provider/map_provider.dart';
import '../provider/user_provider.dart';
import '../screens/my_address.dart';
import '../theme/colors.dart';
import 'location_check.dart';
import 'tad_snackbar_error.dart';

class SourceWidget extends StatelessWidget {
  const SourceWidget({
    Key? key,
    required this.markerDetailsController,
    required this.sourceDetailsContoller,
    required this.onPress,
    required this.sour,
    required this.des,
    required this.isFavourite,
  }) : super(key: key);
  final Function()? onPress;
  final TextEditingController markerDetailsController;
  final TextEditingController sourceDetailsContoller;
  final des;
  final sour;
  final isFavourite;

  @override
  Widget build(BuildContext context) {
    return Positioned(
      left: 0,
      right: 0,
      bottom: 0,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 20),
        width: double.infinity,
        height: 350.h,
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: kTeal100),
            borderRadius: BorderRadius.only(topLeft: Radius.circular(25), topRight: Radius.circular(25))),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            child: Column(
              children: [
                Text(
                  "نقطة الإنطلاق",
                  style: TextStyle(
                    color: Theme.of(context).colorScheme.primary,
                    fontSize: 16,
                  ),
                ),
                ClipPath(
                  // clipper: ShapeBorderClipper(
                  //     shape: RoundedRectangleBorder(
                  //         borderRadius: BorderRadius.all(
                  //   Radius.elliptical(
                  //     50,
                  //     100,
                  //   ),
                  // ))),
                  child: Stack(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.white
                        ),
                        child: Consumer<MapProvider>(
                          builder: (context, mapProvider, _) => Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("ادخل العنوان", style: TextStyle(color: Colors.black,fontSize: 16,fontWeight: FontWeight.bold),textAlign: TextAlign.start,),
                              TextFormField(
                                  enabled: false,
                                  controller: markerDetailsController,
                                  decoration: InputDecoration(
                                    prefixIcon: IconButton(onPressed: null,icon: SvgPicture.asset("assets/images/cus_address.svg")),
                                    // contentPadding:
                                    //     EdgeInsets.symmetric(horizontal: 0),
                                    fillColor: Color(0xFFE0E0E0),
                                    filled: true,
                                  )),
                              Container(
                                color: mapProvider.filedColor,
                                child: mapProvider.filedColor == Colors.white
                                    ? Divider(
                                        color: Theme.of(context).colorScheme.secondary,
                                        thickness: 1,
                                      )
                                    : Container(),
                              ),
                              Text("اسم البناء / رقم البيت", style: TextStyle(color: Colors.black,fontSize: 16,fontWeight: FontWeight.bold),textAlign: TextAlign.start,),
                              TextFormField(
                                validator: (value) {
                                  value = sourceDetailsContoller.text;
                                  if (value.isEmpty) {
                                    mapProvider.changeFiledColor(value, context);
                                    return 'الرجاء إدخال التفاصيل';
                                  } else
                                    return null;
                                },
                                onChanged: (value) {
                                  mapProvider.changeFiledColor(value, context);
                                },
                                controller: sourceDetailsContoller,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    contentPadding: EdgeInsets.symmetric(horizontal: 30),
                                    fillColor: Color(0xFFE0E0E0),
                                    // sourceDetailsContoller.text.isEmpty
                                    //     ? Theme.of(context)
                                    //         .colorScheme
                                    //         .secondary
                                    //     : mapProvider.filedColor,
                                    filled: true,
                                    hintText: "أدخل تفاصيل البناء",
                                    //hintStyle: TextStyle(color: Colors.black)
                                    ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        //margin: EdgeInsets.only(top: 15, bottom: 15, left: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            InkWell(
                                onTap: () {
                                  Provider.of<UserModel>(context, listen: false).saveFavoriteLocation(
                                          Provider.of<AuthProvider>(context,listen: false).user!.clientId,
                                          Provider.of<MapProvider>(context,listen: false).source!)
                                      .then((value) async {
                                    if (value) {
                                      Provider.of<MapProvider>(context,listen: false).changeIsFavourite();
                                      // ScaffoldMessenger.of(context)
                                      //     .showSnackBar(
                                      //   MySnackBar(
                                      //     text: "تم بنجاح حفظ الموقع الجديد",
                                      //     type: MySnackBarType.success,
                                      //     context: context,
                                      //   ),
                                      // );
                                      // await Provider.of<UserModel>(context,
                                      //         listen: false)
                                      //     .getAllFavouriteAddress();
                                    } else {
                                      ScaffoldMessenger.of(context).showSnackBar(
                                        MySnackBar(
                                          text: "حدث خطأ اثناء حفظ الموقع ",
                                          type: MySnackBarType.error,
                                          context: context,
                                        ),
                                      );
                                    }
                                  });
                                },
                                child: sourceDetailsContoller.text.isNotEmpty
                                    ? Icon(
                                        isFavourite
                                            ? Icons.favorite
                                            : Icons.favorite_border,
                                        size: 20,
                                        color: Theme.of(context).colorScheme.secondary,
                                      )
                                    : Container()),
                            InkWell(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => MyAddressesPage(
                                      isDes: false,
                                      isSource: true,
                                      titleIndex: 1,
                                    ),
                                  ),
                                );
                                Provider.of<MapHelper>(context, listen: false).predictions.clear();
                                Provider.of<UserModel>(context, listen: false).getAllFavouriteAddress()
                                    .then((value) async {
                                  await Provider.of<MapHelper>(context,listen: false).getPointAddressName(value);
                                });
                                Provider.of<UserModel>(context, listen: false).getWorkLocation()
                                    .then((value) async {
                                  await Provider.of<MapHelper>(context,listen: false).getPointAddressName(value);
                                });
                                Provider.of<UserModel>(context, listen: false).getHomeLocation()
                                    .then((value) async {
                                  await Provider.of<MapHelper>(context,listen: false).getPointAddressName(value);
                                });
                              },
                              child: Icon(
                                Icons.search,
                                size: 20,
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  style: ButtonStyle(
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(borderRadius: BorderRadius.circular(25.0),)),
                    elevation: MaterialStateProperty.all(7),
                    padding: MaterialStateProperty.all( EdgeInsets.symmetric(horizontal: 30, vertical: 15),
                    ),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "اختر نقطة الإنطلاق",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Icon(Icons.arrow_forward),
                    ],
                  ),
                  onPressed: onPress,
                ),
                // Consumer<MapProvider>(
                //   builder: (context, mapProvider, _) => Container(
                //     width: double.infinity,
                //     child: Row(
                //       children: [
                //         Provider.of<UserModel>(context, listen: false)
                //                     .homeLocation !=
                //                 null
                //             ? LocationCheck(
                //                 type: 1,
                //                 title: "المنزل",
                //                 vlaue: mapProvider.checkHome,
                //                 des: des,
                //                 source: sour,
                //               )
                //             : Container(),
                //         Provider.of<UserModel>(context, listen: false)
                //                     .workLocation !=
                //                 null
                //             ? LocationCheck(
                //                 type: 2,
                //                 title: "العمل",
                //                 vlaue: mapProvider.checkWork,
                //                 des: des,
                //                 source: sour,
                //               )
                //             : Container()
                //       ],
                //     ),
                //   ),
                // ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
