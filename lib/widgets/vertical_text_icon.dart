import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../provider/map_helper_provider.dart';

class VerticalTextIcon extends StatelessWidget {
  const VerticalTextIcon({
    Key? key,
    required this.icon,
    required this.title,
    required this.type,
  }) : super(key: key);
  final String title;
  final IconData icon;
  final String type;

  @override
  Widget build(BuildContext context) {
    Color secondary = Theme.of(context).colorScheme.secondary;
    return Consumer<MapHelper>(
      builder: (context, mapHelperProvider, _) => InkWell(
        onTap: () {
          mapHelperProvider.cahngeSelectedValue(type);
        },
        child: Column(children: [
          Icon(
            icon,
            size: 25,
            color: type == 'fav' && mapHelperProvider.isFavSlected
                ? secondary
                : type == 'work' && mapHelperProvider.isWorkSelected
                    ? secondary
                    : type == 'house' && mapHelperProvider.isHouseSelected
                        ? secondary
                        : Colors.grey,
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            title,
            style: TextStyle(
              fontSize: 15,
              color: type == 'fav' && mapHelperProvider.isFavSlected
                  ? secondary
                  : type == 'work' && mapHelperProvider.isWorkSelected
                      ? secondary
                      : type == 'house' && mapHelperProvider.isHouseSelected
                          ? secondary
                          : Colors.grey,
            ),
          ),
        ]),
      ),
    );
  }
}
