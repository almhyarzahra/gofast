import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../provider/map_provider.dart';

class AddLocationWidget extends StatelessWidget {
  const AddLocationWidget(
      {Key? key,
      required this.title,
      required this.typeId,
      required this.onTap})
      : super(key: key);

  final String title;
  final int typeId;
  final Function() onTap;

  @override
  Widget build(BuildContext context) {
    return Consumer<MapProvider>(
      builder: (context, mapProvider, _) => InkWell(
        onTap: onTap,
        child: Material(
          elevation: 7,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                Icon(Icons.add),
                SizedBox(
                  width: 10,
                ),
                Text(title),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
