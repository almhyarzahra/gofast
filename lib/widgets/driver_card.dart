import 'package:flutter/material.dart';

import '../models/driver.dart';

class DriverCard extends StatelessWidget {
  const DriverCard({
    Key? key,
    required this.driver,
    required this.driverArrivingTime,
    required this.driverRating,
    this.isVisibale,
  }) : super(key: key);

  final bool? isVisibale;
  final Driver driver;
  final String driverArrivingTime;
  final String driverRating;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 5.0),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
        color: driver.isChocesnOne
            ? Color(0xffD6D6D6)
            : Color(0xffD6D6D6).withOpacity(0.2),
        child: Padding(
          padding: EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CircleAvatar(
                backgroundImage: AssetImage(
                  "assets/images/driver-icon-2.jpg",
                ),
                radius: 40,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(driver.driverName),
                    Row(
                      children: [
                        Icon(
                          Icons.star,
                          color: Theme.of(context).colorScheme.secondary,
                        ),
                        Text(driverRating),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 7,
              ),
              Row(
                children: [
                  Icon(
                    Icons.car_repair,
                    color: Theme.of(context).colorScheme.primary,
                  ),
                  Text(driver.car!.carModel.toString()),
                ],
              ),
              SizedBox(
                height: 7,
              ),
              Row(
                children: [
                  Icon(
                    Icons.location_on,
                    color: Theme.of(context).colorScheme.primary,
                  ),
                  Text(" زمن الوصول ${driverArrivingTime} دقيقة "),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
