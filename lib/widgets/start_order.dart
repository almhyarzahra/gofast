import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

import '../common/tools.dart';
import '../provider/driver_provider.dart';
import '../provider/map_provider.dart';
import '../theme/colors.dart';

class StartOrder extends StatelessWidget {
  const StartOrder({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<MapProvider>(
      builder: (context, mapProvider, _) => Consumer<DriverProvider>(
        builder: (context, driverProvider, _) => SizedBox(
          width: double.infinity,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  TextButton(
                      onPressed: () {},
                      child: Text(
                        "اقتراحات الرحلة",
                        style: TextStyle(color: Colors.black),
                      )),
                  IconButton(
                      onPressed: () {
                        mapProvider.polylineCoordinates.clear();
                        mapProvider.choseDes = true;
                        mapProvider.changeIndex(1);
                      },
                      icon: Icon(
                        Icons.arrow_forward,
                        color: Theme.of(context).colorScheme.secondary,
                      ))
                ],
              ),
              CarouselSlider(
                items: driverProvider.suggestionTripCardList,
                options: CarouselOptions(
                  onPageChanged: (index, reason) {
                    driverProvider.choseTrip(index);
                  },
                  disableCenter: true,
                  viewportFraction: 0.4,
                  enlargeCenterPage: false,
                  aspectRatio: 14 / 6,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children:[
                InkWell(
                  onTap: () {
                    driverProvider.makeChoseDriverAsGirl();
                  },
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: Column(
                      children: [
                        SvgPicture.asset(
                          "assets/images/prefer_girl.svg",
                          color: driverProvider.preferGirle == false
                              ? null
                              : Theme.of(context).colorScheme.primary,
                        ),
                        Text(
                          "تفضيل السائق فتاة",
                          style: TextStyle(
                            color: driverProvider.preferGirle == false
                                ? null
                                : Theme.of(context).colorScheme.primary,
                            fontSize: 8,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                VerticalDivider(width: 40,),
                // ElevatedButton(
                //   style: ButtonStyle(
                //     shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                //         RoundedRectangleBorder(
                //           borderRadius: BorderRadius.circular(10.0),
                //         )),
                //     elevation: MaterialStateProperty.all(7),
                //     padding: MaterialStateProperty.all(
                //       EdgeInsets.symmetric(
                //         horizontal: MediaQuery.of(context).size.width * .025,
                //         vertical: 10,
                //       ),
                //     ),
                //     backgroundColor:mapProvider.isButtonPressed == false? MaterialStateProperty.all(kGrey200): MaterialStateProperty.all(kTeal100),
                //   ),
                //   onPressed: () async {
                //     mapProvider.changeButtonTheme();
                //   },
                //   child: Text('طلب عن شخص اخر',style: TextStyle(color:mapProvider.isButtonPressed? Colors.white: kGrey900),),
                // ),
                // VerticalDivider(width: 25,),
                SizedBox(
                  width: MediaQuery.of(context).size.width * .25,
                  child: TextField(
                    onSubmitted: (value) async{
                      print(value);
                      await driverProvider.getCobon(value);
                      mapProvider.cobonCodeController.clear();
                      if(driverProvider.activatedCobon == false){
                        showErrorSnakBar(context,'تأكد من صحة كود الخصم');
                      }
                    },
                    controller:mapProvider.cobonCodeController ,
                      decoration: InputDecoration(
                        hintText: 'ادخل كوبون حسم',
                        ),
                  ),
                ),
                Icon(Icons.discount_outlined,color:Theme.of(context).colorScheme.primary ,)
              ]
              ),
              SizedBox(
                height: 10,
              ),
              ElevatedButton(
                style: ButtonStyle(
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  )),
                  elevation: MaterialStateProperty.all(7),
                  padding: MaterialStateProperty.all(
                    EdgeInsets.symmetric(
                      horizontal: MediaQuery.of(context).size.width * .2,
                      vertical: 15,
                    ),
                  ),
                ),
                onPressed: () async {
                  driverProvider.driverId = 0;
                  driverProvider.chosenDriver = null;
                  mapProvider.getDriver = true;
                  // await driverProvider.getAvailableDriverIdWithLocation(
                  //   mapProvider.source!,
                    // authProvider.user!.token,
                  // );
                  await driverProvider.getAllDrivers(mapProvider.source!,);
                },
                child: Text('اطلب الرحلة الأن'),
              ),
              SizedBox(
                height: 10,
              )
            ],
          ),
        ),
      ),
    );
  }
}
