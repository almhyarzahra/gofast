import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../provider/map_provider.dart';

class TripInformation extends StatelessWidget {
  const TripInformation({Key? key, required this.height}) : super(key: key);

  final double height;

  @override
  Widget build(BuildContext context) {
    return Consumer<MapProvider>(
      builder: (context, mapProvider, _) => Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 30),
        child: Material(
          elevation: 10,
          child: Container(
            height: height * .1,
            decoration: BoxDecoration(
              border: Border(
                right: BorderSide(
                  width: 16.0,
                  color: Theme.of(context).colorScheme.primary,
                ),
              ),
              color: Colors.white,
            ),
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 7),
            child: Row(
              children: [
                Expanded(
                  flex: 2,
                  child: Column(
                    children: [
                      Expanded(
                        child: Row(
                          children: [
                            Expanded(
                              flex: 0,
                              child: const Text("موقع الإنطلاق  : "),
                            ),
                            Expanded(
                              flex: 2,
                              child: Text(
                                mapProvider.sourceDetails.toString().isEmpty
                                    ? "حرك العلامة على الخارطة"
                                    : mapProvider.sourceDetails.toString(),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Row(
                          children: [
                            Expanded(
                              flex: 0,
                              child: const Text("  مكان الذهاب : "),
                            ),
                            Expanded(
                              flex: 2,
                              child: Text(
                                mapProvider.destination == null
                                    ? " لم يتم الإختيار بعد"
                                    : mapProvider.desDetails.toString(),
                                maxLines: null,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
