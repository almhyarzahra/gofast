import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import '../common/tools.dart';
import '../helper/http_exception.dart';
import '../provider/user_provider.dart';
import '../screens/home_page.dart';
import '../theme/colors.dart';

class ProfileForm extends StatefulWidget {
  const ProfileForm({
    Key? key,
    this.name,
    this.fromProfile,
    this.birthDate,
    this.gender,
  }) : super(key: key);

  final String? name;
  final String? gender;
  final String? birthDate;
  final bool? fromProfile;

  @override
  _ProfileFormState createState() => _ProfileFormState();
}

class _ProfileFormState extends State<ProfileForm> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _bearthDateController = TextEditingController();
  String? dropdownValue;

  Map<String, String> _userSignupData = {
    'Username': '',
    'address': '',
    'birthDate': '',
    'gender': 'ذكر'
  };

  void _showAwesomeDialog(BuildContext context, String error) {
    AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.RIGHSLIDE,
        headerAnimationLoop: false,
        title: 'Error',
        desc: error,
        btnOkOnPress: () {},
        btnOkIcon: Icons.cancel,
        btnOkColor: Colors.red)
      ..show();
  }

  _datePicker(BuildContext context) async {
    showDatePicker(
            context: context,
            initialDate: DateTime.now(),
            firstDate: DateTime(1900),
            initialEntryMode: DatePickerEntryMode.calendarOnly,
            lastDate: DateTime(3000))
        .then((picked) {
      if (picked != null) {
        setState(() {
          _bearthDateController.text = DateFormat('yyyy/MM/dd').format(picked);
        });
      }
    });
  }

  String replaceArabicNumber(String input) {
    const english = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    const arabic = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];

    for (int i = 0; i < english.length; i++) {
      input = input.replaceAll(arabic[i], english[i]);
    }

    return input;
  }

  Future<void> _submit() async {
    if (!_formKey.currentState!.validate()) {
      return;
    }
    if (_userSignupData['gender']!.isEmpty) {
      showErrorSnakBar(context, 'اختر الجنس');
      return;
    }
    _formKey.currentState!.save();
    setState(() {});
    try {
      print('hi before call');
      print(_userSignupData);

      bool res =
          await Provider.of<UserModel>(context, listen: false).updateClient(
        birthdate: _userSignupData['birthDate']!,
        gender: _userSignupData['gender']! == 'ذكر' ? false : true ,
        clientName: _userSignupData['Username']!,
      );
      print('hi after call');

      if (res) {
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => HomePage()),
            (route) => false);
      }
    } on HttpException catch (error) {
      _showAwesomeDialog(context, error.toString());
      print(error.toString());
    } catch (error) {
      _showAwesomeDialog(context, error.toString());

      print(error.toString());
    } finally {
      setState(() {});
    }
  }

  late TextEditingController name = TextEditingController();
  late TextEditingController birthDay = TextEditingController();
  @override
  void initState() {
    super.initState();
    if (widget.name != null &&
        widget.birthDate != null &&
        widget.gender != null) {
      _userSignupData['birthDate'] = widget.birthDate!;
      _userSignupData['gender'] = widget.gender!;
      _userSignupData['Username'] = widget.name!;
      name.text = _userSignupData['Username']!;
      _bearthDateController.text = _userSignupData['birthDate']!;
    }
  }

  @override
  Widget build(BuildContext context) {
    Provider.of<UserModel>(context);
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          ClipPath(
            clipper: ShapeBorderClipper(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
              bottomLeft: Radius.elliptical(
                200,
                100,
              ),
              bottomRight: Radius.elliptical(
                200,
                100,
              ),
            ))),
            child: Container(
              alignment: Alignment.centerRight,
              width: double.infinity,
              child: Padding(
                padding: const EdgeInsets.symmetric(
                    horizontal: 20.0, vertical: 20.0),
                child: InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Row(
                    children: [
                      Icon(
                        Icons.arrow_back,
                        color: Colors.white,
                        size: 22,
                      ),
                      SizedBox(
                        width: 25,
                      ),
                      Text(
                        "رجوع",
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              decoration: BoxDecoration(
                color: kTeal50,
              ),
              height: 200.h,
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25),
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  SizedBox(height: MediaQuery.of(context).size.height * 0.03),
                  TextFormField(
                    controller: name,
                    style: TextStyle(height: 1),
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(
                            color: Theme.of(context).colorScheme.secondary),
                      ),
                      border: UnderlineInputBorder(
                        borderSide: BorderSide(
                            color: Theme.of(context).colorScheme.secondary),
                      ),
                      hintText: widget.name ?? 'الاسم الكامل',
                    ),
                    validator: (val) {
                      if (val!.isEmpty) {
                        return 'هذا الحقل مطلوب';
                      }
                      return null;
                    },
                    onSaved: (val) {
                      _userSignupData['Username'] = val!;
                    },
                  ),

                  SizedBox(height: MediaQuery.of(context).size.height * 0.03),

                  InkWell(
                    onTap: () async {
                      _datePicker(context);
                    },
                    child: TextFormField(
                      enabled: false,
                      style: TextStyle(height: 1),
                      controller: _bearthDateController,
                      keyboardType: TextInputType.datetime,
                      decoration: InputDecoration(
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                              color: Theme.of(context).colorScheme.secondary),
                        ),
                        border: UnderlineInputBorder(
                          borderSide: BorderSide(
                              color: Theme.of(context).colorScheme.secondary),
                        ),
                        hintText: 'تاريخ الميلاد',
                      ),
                      validator: (val) {
                        if (val!.isEmpty) {
                          return 'هذا الحقل مطلوب';
                        }
                        return null;
                      },
                      onSaved: (val) {
                        _userSignupData['birthDate'] =
                            _bearthDateController.text;
                      },
                    ),
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height * 0.03),

                  SizedBox(
                    height: 55,
                    child: DropdownButton<String>(
                      isExpanded: true,
                      value: dropdownValue ?? widget.gender,
                      hint: Padding(
                        padding: const EdgeInsets.only(right: 8),
                        child: Text(
                          "الجنس",
                          style: TextStyle(color: Colors.grey),
                        ),
                      ),
                      underline: SizedBox(),
                      icon: const Icon(Icons.arrow_drop_down),
                      elevation: 16,
                      style: const TextStyle(color: Colors.deepPurple),
                      onChanged: (String? newValue) {
                        setState(() {
                          dropdownValue = newValue!;
                        });
                        if (newValue != null) {
                          _userSignupData['gender'] = newValue;
                        }
                      },
                      items: <String>[
                        'ذكر',
                        'أنثى',
                      ].map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Padding(
                            padding: const EdgeInsets.only(right: 8),
                            child: Text(value),
                          ),
                        );
                      }).toList(),
                    ),
                  ),

                  SizedBox(height: MediaQuery.of(context).size.height * 0.05),

                  Container(
                    margin: EdgeInsets.only(bottom: 5),
                    height: 55,
                    width: MediaQuery.of(context).size.width / 1.2,
                    child: ElevatedButton(
                        style: ButtonStyle(
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(25.0),
                          )),
                          padding: MaterialStateProperty.all(
                            EdgeInsets.symmetric(horizontal: 30, vertical: 15),
                          ),
                        ),
                        onPressed: _submit,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              widget.fromProfile != null
                                  ? "تعديل الحساب"
                                  : "إنشاء حساب",
                              style: TextStyle(fontSize: 18),
                            ),
                          ],
                        )),
                  ),
                  // AnimatedOpacity(
                  //   opacity: 1.0,
                  //   duration: const Duration(milliseconds: 1000),
                  //   child: _isLoading
                  //       ? Center(child: CircularProgressIndicator())
                  //       : Container(
                  //           color: Colors.purpleAccent,
                  //           height: mediaQueryData.size.height * 0.07,
                  //           child: ElevatedButton(
                  //             onPressed: _submit,
                  //             child: Ink(
                  //               decoration: BoxDecoration(
                  //                   borderRadius: BorderRadius.circular(5.0)),
                  //               child: Container(
                  //                 alignment: Alignment.center,
                  //                 child: Text(
                  //                   "إرسال معلومات التسجيل",
                  //                   textAlign: TextAlign.center,
                  //                   style: TextStyle(color: Colors.white),
                  //                 ),
                  //               ),
                  //             ),
                  //           ),
                  //         ),
                  // ),
                  SizedBox(height: MediaQuery.of(context).size.height * 0.05),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
