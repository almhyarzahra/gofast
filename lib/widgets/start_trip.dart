import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../models/driver.dart';
import '../provider/driver_provider.dart';
import '../provider/map_provider.dart';

class StartTrip extends StatelessWidget {
  const StartTrip({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<MapProvider>(
      builder: (context, mapProvider, _) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            children: [
              Container(
                margin: EdgeInsets.only(right: 20),
                child: Column(
                  children: [
                    ListTile(
                      // trailing: Text("الوقت"),
                      title: Text(
                          Provider.of<MapProvider>(context, listen: false)
                              .sourceDetails!),
                    ),
                    ListTile(
                      // trailing: Text("الوقت"),
                      title: Text(
                          Provider.of<MapProvider>(context, listen: false)
                              .desDetails!),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 25),
                alignment: Alignment.centerRight,
                child: SizedBox(
                    height: 60,
                    width: 50,
                    child: Image.asset("assets/images/from_to.png")),
              ),
            ],
          ),
          ListTile(
            // trailing: Text("الوقت"),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: [
                CircleAvatar(
                    radius: 25,
                    backgroundColor: Colors.grey,
                    child: CircleAvatar(
                      radius: 23,
                      backgroundImage: AssetImage(
                        "assets/images/driver-icon-2.jpg",
                      ),
                    )),
                SizedBox(
                  width: 20,
                ),
                Column(
                  children: [
                    Text(Provider.of<DriverProvider>(context, listen: false)
                        .chosenDriver!
                        .driverName),
                    Row(
                      children: [
                        Text(Provider.of<DriverProvider>(context, listen: false).driverRating[
                        Provider.of<DriverProvider>(context, listen: false)
                            .chosenDriver!.driverId].toString()),
                        Icon(
                          Icons.star,
                          color: Theme.of(context).colorScheme.secondary,
                        ),
                      ],
                    ),
                  ],
                )
              ],
            ),
          ),
          Center(
            child: ElevatedButton(
                onPressed: () async {
                  print(Provider.of<DriverProvider>(context, listen: false).orderModel!.orderId);
                  await Provider.of<DriverProvider>(context,listen: false).getBill(Provider.of<DriverProvider>(context, listen: false).orderModel!.orderId).then((value) {
                    if(value) mapProvider.changeIndex(6) ;
                  }
                  );
                },
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(
                      Theme.of(context).colorScheme.primary),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  )),
                  elevation: MaterialStateProperty.all(7),
                  padding: MaterialStateProperty.all(
                    EdgeInsets.symmetric(
                        horizontal: MediaQuery.of(context).size.width * .22,
                        vertical: 15),
                  ),
                ),
                child: Text(
                  "انتهت الرحلة",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                )),
          ),
          SizedBox(
            height: 10,
          )
        ],
      ),
    );
  }
}
