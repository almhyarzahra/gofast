import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../provider/map_provider.dart';
import '../provider/user_provider.dart';

class LocationCheck extends StatelessWidget {
  const LocationCheck({
    Key? key,
    required this.title,
    required this.vlaue,
    required this.type,
    required this.des,
    required this.source,
  }) : super(key: key);
  final String title;
  final bool vlaue;
  final int type;
  final bool des;
  final bool source;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(title),
        Checkbox(
          value: vlaue,
          onChanged: (val) async {
            if (type == 1) {
              await Provider.of<MapProvider>(context, listen: false)
                  .handelCheckLocation(
                      type,
                      val!,
                      Provider.of<UserModel>(context, listen: false)
                          .homeLocation[0]
                          .location!,
                      source,
                      des);
            }
          },
        )
      ],
    );
  }
}
