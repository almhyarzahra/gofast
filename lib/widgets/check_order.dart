import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../helper/shared_variables.dart';
import '../provider/driver_provider.dart';
import '../provider/map_provider.dart';
import '../screens/home_page.dart';

class CheckOrderWidget extends StatelessWidget {
  const CheckOrderWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<DriverProvider>(
      builder: (context, driverProvider, _) => Padding(
        padding: const EdgeInsets.symmetric(horizontal: 50),
        child: Column(
          children: [
            Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              color: Color(0xffE2E2E2),
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 30,
                ),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: driverProvider.orderStat == null ||
                            driverProvider.orderStat == '0'
                        ? [
                            const Text(
                              " يرجى الانتظار لحين تأكيد",
                            ),
                            const Text(
                              "الطلب من قبل الكابتن ",
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Text(driverProvider
                                        .chosenDriver!.driverName),
                                    Row(
                                      children: [
                                        Icon(
                                          Icons.star,
                                          color: Theme.of(context)
                                              .colorScheme
                                              .secondary,
                                        ),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Text(driverProvider.driverRating[
                                                driverProvider
                                                    .chosenDriver!.driverId]
                                            .toString()),
                                      ],
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    Row(
                                      children: [
                                        Icon(
                                          Icons.local_taxi,
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Text(
                                            "${driverProvider.chosenDriver!.car!.carModel}")
                                      ],
                                    ),
                                    Visibility(
                                      visible: false,
                                      child: Row(
                                        children: [
                                          Icon(
                                            Icons.star,
                                            color: Theme.of(context)
                                                .colorScheme
                                                .secondary,
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          Text(driverProvider.driverRating[
                                                  driverProvider
                                                      .chosenDriver!.driverId]
                                              .toString()),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),

                            // CircularProgressIndicator(),
                          ]
                        : driverProvider.orderStat == '3'
                            ? [
                                const Text(
                                  "نتأسف منكم",
                                  style: TextStyle(color: Colors.red),
                                ),
                                const Text(
                                  "الكابتن غير قادر على استلام",
                                  style: TextStyle(color: Colors.red),
                                ),
                                const Text(
                                  "الطلب",
                                  style: TextStyle(color: Colors.red),
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                                Column(
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        Text(driverProvider
                                            .chosenDriver!.driverName),
                                        Row(
                                          children: [
                                            Icon(
                                              Icons.star,
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .secondary,
                                            ),
                                            SizedBox(
                                              width: 5,
                                            ),
                                            Text(driverProvider.driverRating[
                                                    driverProvider
                                                        .chosenDriver!.driverId]
                                                .toString()),
                                          ],
                                        ),
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                      children: [
                                        Row(
                                          children: [
                                            Icon(
                                              Icons.local_taxi,
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                            ),
                                            SizedBox(
                                              width: 5,
                                            ),
                                            Text(
                                                "${driverProvider.chosenDriver!.car!.carModel}")
                                          ],
                                        ),
                                        Visibility(
                                          visible: false,
                                          child: Row(
                                            children: [
                                              Icon(
                                                Icons.star,
                                                color: Theme.of(context)
                                                    .colorScheme
                                                    .secondary,
                                              ),
                                              SizedBox(
                                                width: 5,
                                              ),
                                              Text(driverProvider.driverRating[
                                                      driverProvider
                                                          .chosenDriver!
                                                          .driverId]
                                                  .toString()),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                // Consumer<MapProvider>(
                                //   builder: (context, mapProvider, _) =>
                                //       ElevatedButton(
                                //     onPressed: () async {
                                //       Navigator.pushAndRemoveUntil(
                                //         context,
                                //         MaterialPageRoute(
                                //             builder: (context) =>
                                //                 const HomePage()),
                                //         (route) => false,
                                //       );
                                //       await mapProvider.initState();
                                //     },
                                //     child: Text("إعادة المحاولة"),
                                //   ),
                                // ),
                              ]
                            : driverProvider.orderStat == "1"
                                ? [
                                    Text(
                                        "ستصل السيارة خلال ${driverProvider.driverArrivingTime[driverProvider.chosenDriver!.driverId]!.split(' ').first}  دقيقة"),
                                    const Text("يمكنك تتبع الكابتن على "),
                                    const Text("الخريطة"),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    // Consumer<MapProvider>(
                                    //   builder: (context, mapProvider, _) =>
                                    //       ElevatedButton(
                                    //     onPressed: () async {
                                    //       await driverProvider
                                    //           .driverTracker()
                                    //           .then((value) {
                                    //         mapProvider.addDriverMarker(
                                    //           value!,
                                    //         );

                                    //         driverProvider.timerForTracer =
                                    //             Timer.periodic(
                                    //                 Duration(seconds: 10),
                                    //                 (timer) async {
                                    //           print(
                                    //               "from timer function ${mapProvider.myMarker!.length}");
                                    //           await driverProvider
                                    //               .driverTracker()
                                    //               .then((value) {
                                    //             mapProvider.addDriverMarker(
                                    //               value!,
                                    //             );
                                    //           });
                                    //         });
                                    //       });

                                    //       mapProvider.changeIndex(5);
                                    //     },
                                    //     child: Text("بدا الرحلة "),
                                    //   ),
                                    // ),
                                    Column(
                                      children: [
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          children: [
                                            Text(driverProvider
                                                .chosenDriver!.driverName),
                                            Row(
                                              children: [
                                                Icon(
                                                  Icons.star,
                                                  color: Theme.of(context)
                                                      .colorScheme
                                                      .secondary,
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                Text(driverProvider
                                                    .driverRating[driverProvider
                                                        .chosenDriver!.driverId]
                                                    .toString()),
                                              ],
                                            ),
                                          ],
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceAround,
                                          children: [
                                            Row(
                                              children: [
                                                Icon(
                                                  Icons.local_taxi,
                                                  color: Theme.of(context)
                                                      .colorScheme
                                                      .primary,
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                Text(
                                                    "${driverProvider.chosenDriver!.car!.carModel}")
                                              ],
                                            ),
                                            Visibility(
                                              visible: false,
                                              child: Row(
                                                children: [
                                                  Icon(
                                                    Icons.star,
                                                    color: Theme.of(context)
                                                        .colorScheme
                                                        .secondary,
                                                  ),
                                                  SizedBox(
                                                    width: 5,
                                                  ),
                                                  Text(driverProvider
                                                      .driverRating[
                                                          driverProvider
                                                              .chosenDriver!
                                                              .driverId]
                                                      .toString()),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ]
                                : driverProvider.orderStat == '2'
                                    ? [
                                        Text(
                                          "الكابتن بإنتظارك",
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                        SizedBox(
                                          height: 40,
                                        ),
                                        Column(
                                          children: [
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              children: [
                                                Text(driverProvider
                                                    .chosenDriver!.driverName),
                                                Row(
                                                  children: [
                                                    Icon(
                                                      Icons.star,
                                                      color: Theme.of(context)
                                                          .colorScheme
                                                          .secondary,
                                                    ),
                                                    SizedBox(
                                                      width: 5,
                                                    ),
                                                    Text(driverProvider
                                                            .driverRating[
                                                        driverProvider
                                                            .chosenDriver!
                                                            .driverId]!),
                                                  ],
                                                ),
                                              ],
                                            ),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceAround,
                                              children: [
                                                Row(
                                                  children: [
                                                    Icon(
                                                      Icons.local_taxi,
                                                      color: Theme.of(context)
                                                          .colorScheme
                                                          .primary,
                                                    ),
                                                    SizedBox(
                                                      width: 5,
                                                    ),
                                                    Text(
                                                        "${driverProvider.chosenDriver!.car!.carModel}")
                                                  ],
                                                ),
                                                Visibility(
                                                  visible: false,
                                                  child: Row(
                                                    children: [
                                                      Icon(
                                                        Icons.star,
                                                        color: Theme.of(context)
                                                            .colorScheme
                                                            .secondary,
                                                      ),
                                                      SizedBox(
                                                        width: 5,
                                                      ),
                                                      Text(driverProvider
                                                          .driverRating[
                                                              driverProvider
                                                                  .chosenDriver!
                                                                  .driverId]
                                                          .toString()),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ]
                                    : []),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            driverProvider.orderStat == null || driverProvider.orderStat == "1"
                ? Consumer<MapProvider>(
                    builder: (context, mapProvider, _) => ElevatedButton(
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all(
                                Theme.of(context).colorScheme.secondary),
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            )),
                            elevation: MaterialStateProperty.all(7),
                            padding: MaterialStateProperty.all(
                              EdgeInsets.symmetric(
                                  horizontal:
                                      MediaQuery.of(context).size.width * .22,
                                  vertical: 15),
                            ),
                          ),
                          child: Text(
                            "الغاء الرحلة ",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                            ),
                          ),
                          onPressed: () async {
                            if (await driverProvider.cancelOrder(
                                orderId: driverProvider.orderModel!.orderId
                                    .toString())) {
                              driverProvider.desDetails.clear();
                              driverProvider.sourceDetails.clear();
                              mapProvider.startMarker = null;
                              Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => const HomePage(),
                                ),
                              );
                              await mapProvider.initState();
                            }
                          },
                        ))
                : driverProvider.orderStat == '3'
                    ? Consumer<MapProvider>(
    builder: (context, mapProvider, _) => ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(
                              Theme.of(context).colorScheme.primary),
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          )),
                          elevation: MaterialStateProperty.all(7),
                          padding: MaterialStateProperty.all(
                            EdgeInsets.symmetric(
                                horizontal:
                                    MediaQuery.of(context).size.width * .22,
                                vertical: 15),
                          ),
                        ),
                        child: Text(
                          "اختر كابتن اخر",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                        onPressed: () async {
                          driverProvider.timer!.cancel();
                          driverProvider.driverId = 0;
                          // driverProvider.chosenDriver = null;
                          await driverProvider.getAllDrivers(mapProvider.source!,);
                          driverProvider.changeIndex(index! - 1);
                        },
                      ))
                    : ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(
                              Theme.of(context).colorScheme.primary),
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          )),
                          elevation: MaterialStateProperty.all(7),
                          padding: MaterialStateProperty.all(
                            EdgeInsets.symmetric(
                                horizontal:
                                    MediaQuery.of(context).size.width * .22,
                                vertical: 15),
                          ),
                        ),
                        child: Text(
                          "بدء الرحلة ",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                        onPressed: () async {
                          AwesomeDialog(
                              isDense: true,
                              width: MediaQuery.of(context).size.width * .88,
                              padding: EdgeInsets.all(0),
                              borderSide: null,
                              dialogType: DialogType.NO_HEADER,
                              context: context,
                              body: SingleChildScrollView(
                                child: Column(
                                  children: [
                                    Icon(
                                      Icons.report_problem_rounded,
                                      color: Theme.of(context)
                                          .colorScheme
                                          .secondary,
                                      size: 100,
                                    ),
                                    SizedBox(
                                      height: 25,
                                    ),
                                    Text("هل بدأ الرحلة ؟"),
                                    SizedBox(
                                      height: 15,
                                    ),
                                    Container(
                                      width: double.infinity,
                                      padding:
                                          EdgeInsets.symmetric(horizontal: 52),
                                      child: Text(
                                        "تم اعلامنا من قبل الكابتن ببدأ الرحلة نتمنا لكم رحلة أمنة",
                                        style: TextStyle(
                                          color: Color(0xffE2E2E2),
                                          fontWeight: FontWeight.w600,
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    Row(
                                      children: [
                                        Expanded(
                                          flex: 2,
                                          child: Padding(
                                            padding:
                                                const EdgeInsets.only(right: 5),
                                            child: ElevatedButton(
                                                onPressed: () {},
                                                style: ButtonStyle(
                                                  backgroundColor:
                                                      MaterialStateProperty.all(
                                                          Theme.of(context)
                                                              .colorScheme
                                                              .primary),
                                                  shape: MaterialStateProperty
                                                      .all<RoundedRectangleBorder>(
                                                          RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10.0),
                                                  )),
                                                  elevation:
                                                      MaterialStateProperty.all(
                                                          7),
                                                ),
                                                child: Text(
                                                  "ليس بعد",
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.white,
                                                  ),
                                                )),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 3,
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                                left: 5.0, right: 5.0),
                                            child: ElevatedButton(
                                                onPressed: () {
                                                  Navigator.pop(context);
                                                  driverProvider
                                                      .trackingDriverTimer!
                                                      .cancel();
                                                  driverProvider.changeIndex(5);
                                                },
                                                style: ButtonStyle(
                                                  backgroundColor:
                                                      MaterialStateProperty.all(
                                                          Theme.of(context)
                                                              .colorScheme
                                                              .primary),
                                                  shape: MaterialStateProperty
                                                      .all<RoundedRectangleBorder>(
                                                          RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10.0),
                                                  )),
                                                  elevation:
                                                      MaterialStateProperty.all(
                                                          7),
                                                ),
                                                child: Text(
                                                  "نعم",
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.white,
                                                  ),
                                                )),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              )).show();
                        },
                      ),
            SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
    );
  }
}
