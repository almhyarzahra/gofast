import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../theme/colors.dart';

void showErrorSnakBar(context, text) {
  var snackBar = SnackBar(
      backgroundColor: Colors.redAccent,
      elevation: 2,
      duration: const Duration(seconds: 1),
      content: Text(
        text,
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.bold,
        ),
      ));
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}

Widget getFloatingButton(
    VoidCallback? onPressed, String? inputedText, String tag) {
  return SizedBox(
    width: 50.w,
    height: 46.h,
    child: FloatingActionButton(
      heroTag: tag,
      elevation: 3,
      backgroundColor: Colors.white,
      child: const Icon(
        Icons.arrow_forward,
        color: Color(0xFFD6D6D6),
      ),
      onPressed: inputedText == null ? null : onPressed,
    ),
  );
}

Widget buildFloatingButton(IconData icon, VoidCallback onPressed, String tag) {
  return SizedBox(
    width: 50.w,
    height: 46.h,
    child: FloatingActionButton(
      heroTag: tag,
      backgroundColor: Colors.white,
      onPressed: onPressed,
      child: Icon(
        icon,
        color: kTeal100,
        size: 29,
      ),
    ),
  );
}

Widget buildLocationTypes() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceAround,
    children: [
      _locationType(Icons.location_on_outlined, 'موقعك'),
      // SizedBox(width: 70.w),
      _locationType(Icons.favorite_border, 'المفضلة'),
      // SizedBox(width: 70.w),
      _locationType(Icons.home_outlined, 'المنزل'),
      // SizedBox(width: 70.w),
      _locationType(Icons.work_outline, 'العمل'),
      // SizedBox(width: 70.w),
    ],
  );
}

Widget _locationType(IconData icon, String title) {
  return Column(
    children: [
      Icon(
        icon,
        size: 24.sp,
        color: Color(0xFF707070),
      ),
      SizedBox(
        height: 3.3.h,
      ),
      Text(
        title,
        style: TextStyle(fontSize: 12.sp, color: Color(0xFF707070)),
      )
    ],
  );
}

// void showLoadingDialog(BuildContext context) {
//   showDialog(
//     context: context,
//     barrierDismissible: false,
//     builder: (BuildContext context) {
//       return Text("sss");
//     },
//   );
// }

Future<void>? showAwesomeDilog(
    {required BuildContext context,
    required String title,
    required String desc,
    required DialogType dialogType,
    required String btnOkText,
    String? btnCancelText,
    required VoidCallback onOkPress,
    VoidCallback? onCancelPress}) {
  return AwesomeDialog(
    context: context,
    dialogType: dialogType,
    animType: AnimType.BOTTOMSLIDE,
    title: title,
    desc: desc,
    btnOkText: btnOkText,
    btnCancelText: btnCancelText == null ? null : btnCancelText,
    btnCancelOnPress: onCancelPress,
    btnOkOnPress: onOkPress,
    dismissOnTouchOutside: false,
    dismissOnBackKeyPress: false,
  ).show();
}

const String API_KEY = 'AIzaSyDCSt4ABayMg8O3n9Hvxb_vrs_1oUfWXuA';
